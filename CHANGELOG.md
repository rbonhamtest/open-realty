# Change Log

All notable changes to this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).
 
## [Unreleased] - ####-##-###
 
Here we write upgrading notes for open-realty. It's a team effort to make them as
straightforward as possible.
 
### Added
- [#1](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/1) First issue title goes here.
 
### Changed
 
### Fixed

## [3.4.0-beta.2] - 2021-02-11

### Fixed
- [#1](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/1) Addon and Files folder were missing from install package
- [#2](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/2) common.dist.php file was missing
- Vendor Path was set incorrectly in common.php durring install.
- Fix api offset null error error
- Fix $display not set error in listing display.

## [3.4.0-beta.1] - 2021-02-08
  
 
### Added
- PHP 7.4 support
- We now use composer to install php dependencies.
- We now use yarn to install javascript dependencies. (80% Migrated)
- We use https://www.mortgagecalculator.org/ for our mortgage calculator
- Large code cleanup effort, much more still needed. 
 
### Changed
  
- Project is now released under MIT license.
- Project is housed on Gitlab
- Update ADODB
- Update reCaptcha
- Updated Securimage 
- Updated ckeditor
- Updated phpMailer
- Update DataTables
- Update Twitter Auth
- Update Jquery for Admin
- Update Jquery UI
- Updated twitteroauth
- Updated colorbox
- Updated jquery.cookie

 
### Fixed
- Fixed a bug in the user API update method that unnecessarily updated the Activity log when an unauthenticated user attempted to use  it.
- Fixed a bug that prevented the listing ID from being passed to the Listing Detail page's printer friendly link, email to a friend link, and the add favorites link
- When the user functions were refactored for v3.3 we inadvertently broke the ability for the admin to complete a lost password reset operation
- The jQuery form validation would not allow an empty field type: Date if the field was not set to be required.
- OR v3.3 upgrade functions would not upgrade the DB when upgrading from v3.2.7
- The slideshow template's block tags were not being properly cleaned-up if a listing had no photos.
- get_featured_raw template tag was inadvertently disabled due to recent refactoring
- Agent signup was not always sending signup notifications to the Admin and to the person signing up when account moderation was active this was a side-effect of recent PDO refactoring.
- Improved error_reporting for the Field API and PHPmail function.
- Improved the lost password reset email validation check
- Removed deprecated agent_login_link tag from agent_activation_email.html. replaced with baseurl/admin
- Fixed the listing hit counter. This was broken for unauthenticated site visitors due to code refactoring in OR v3.3
- Update Media handling to deal with Protocol-relative URLs \
- Fixed session handling for Securimage
- Fixed extra db connection used by Adodb sessions
- Fixed Agent hit counter that was broken due to prior refactoring
- Fixed email validation that did not handle some email addresses correctly.
- Removed deprecated calculator and javascript library
