ARG VARIANT="7.4"

FROM php:${VARIANT}-apache

RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN apt-get update -y && apt-get install -y libwebp-dev libjpeg62-turbo-dev libpng-dev libxpm-dev \
    libfreetype6-dev wget curl openssl libonig-dev
RUN apt-get update && \
    apt-get install -y \
    zlib1g-dev 

RUN docker-php-ext-install mbstring

RUN apt-get install -y libzip-dev
RUN docker-php-ext-install zip

RUN docker-php-ext-configure gd --enable-gd --with-webp --with-jpeg \
    --with-xpm --with-freetype

RUN docker-php-ext-install gd

EXPOSE 80