# Open-Realty

**Description**:  Open-Realty® is a web-based real estate listing management and lead generation content management system (CMS) that is designed to be very reliable and flexible as a framework for creating and managing a real estate website.


**Screenshot**:

![](https://gitlab.com/appsbytherealryanbonham/open-realty/-/raw/main/screenshot.png)


## Dependencies

* PHP v7.4
* MySQL 5.5 - 5.7
* PHP GD Support or Imagemagik
* PHP Multibyte String Support
* PHP CURL support
* PHP OpenSSL support
* PHP ZIP support
* PHP Short Tag support disabled
* PHP Safe Mode disabled
* PHP magic_quotes set to OFF (in all program folders)
* PHP session.auto_start disabled (session.auto_start = "0")
* PHP SuExec/SuPHP (recommended for automatic upgrades)
* APACHE mod_rewrite (required for SEO Friendly URLs)
* APACHE mod_expires (recommended)
* APACHE mod_headers (recommended)


## Installation

[Installation Guide](https://docs.open-realty.org/nav.guide/01.installation/)

## Getting help

For general help, try our [discord server](https://discord.gg/uU7EYnxW). 
If you have feature request or bug reports, etc, please file an issue in this repository's Issue Tracker.


## Getting involved

See our [CONTRIBUTING](CONTRIBUTING.md) guide.

----

## Open source licensing info

[MIT LICENSE](LICENSE)
