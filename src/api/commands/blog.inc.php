<?php

/**
 * This is the Blog  API, it contains all api calls for creating and retrieving Blog data.
 *
 * @package Open-Realty
 * @subpackage API
 **/
class blog_api
{

    //password field removed from list.
    protected $OR_INT_FIELDS = ['blogmain_id','userdb_id','blogmain_title','blog_seotitle','blogmain_date','blogmain_full','blogmain_description','blogmain_keywords','blogmain_published'];

    /*
        protected $read_filter = array(
            'fields' =>array(
                ‘filter’ => FILTER_SANITIZE_STRING
            )
        );

        protected $read_filter = array(
            'blogmain_id' => FILTER_VALIDATE_INT,
            'userdb_id' => FILTER_VALIDATE_INT,
            'blogmain_title' => FILTER_SANITIZE_STRING,
            'blog_seotitle' => FILTER_SANITIZE_STRING,
            'blogmain_date' => FILTER_VALIDATE_INT,
            'blogmain_full' => FILTER_SANITIZE_STRING,
            'blogmain_description' => FILTER_SANITIZE_STRING,
            'blogmain_keywords' => FILTER_SANITIZE_STRING,
            'blogmain_published' => FILTER_VALIDATE_INT
        );

    */

    /**
     * This API Command searches the users
     * @param array $data $data expects an array containing the following array keys.
     *  <ul>
     *      <li>$data['parameters'] - This is a REQUIRED array of the fields and the values we are searching for.</li>
     *      <li>$data['sortby'] - This is an optional array of fields to sort by.</li>
     **     <li>$data['sorttype'] - This is an optional array of sort types (ASC/DESC) to sort the sortby fields by.</li>
     *      <li>$data['offset'] - This is an optional integer of the number of users to offset the search by. To use offset you must set a limit.</li>
     *      <li>$data['limit'] - This is an optional integer of the number of users to limit the search by. 0 or unset will return all users.</li>
     *      <li>$data['count_only'] - This is an optional integer flag 1/0, where 1 returns a record count only, defaults to 0 if not set. Usefull if doing limit/offset search for pagenation to get the inital full record count..</li>
     *  </ul>
     * @return array  - Array retruned will contain the following paramaters.
     *  [error] = true/FASLE
     *  [user_count] = Number of records found, if using a limit this is only the number of records that match your current limit/offset results.
     *  [users] = Array of user_ids.
     *  [info] = The info array contains benchmark information on the search, including process_time, query_time, and total_time
     *  [sortby] = Contains an array of fields that were used to sort the search results. Note if you are doing a CountONly search the sort is not actually used as this would just slow down the query.
     *  [sorttype] = Contains an array of the sorttype (ASC/DESC) used on the sortby fields.
    *   ['limit'] - INT - The numeric limit being imposed on the results.
    *   ['resource'] - TEXT - The resource the search was made against, 'agent' or 'member'
     **/
    public function search($data)
    {
        $DEBUG_SQL = false;
        global $conn, $lapi, $config, $misc, $lang, $db_type;

        $start_time = $this->getmicrotime();
        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();

        extract($data, EXTR_SKIP || EXTR_REFS, '');
        //Check that required settings were passed
        if (!isset($parameters) || !is_array($parameters)) {
            return ['error' => true, 'error_msg' => 'parameters: correct_parameter_not_passed'];
        }
        if (isset($sortby)&& !is_array($sortby)) {
            return ['error' => true, 'error_msg' => 'sortby: correct_parameter_not_passed'];
        }
        if (isset($sorttype)&& !is_array($sorttype)) {
            return ['error' => true, 'error_msg' => 'sorttype: correct_parameter_not_passed'];
        }
        if (isset($offset) && !is_numeric($offset)) {
            return ['error' => true, 'error_msg' => 'offset: correct_parameter_not_passed'];
        }

        if (isset($limit) && !is_numeric($limit)) {
            return ['error' => true, 'error_msg' => 'limit: correct_parameter_not_passed'];
        }

        if (isset($count_only)&& $count_only ==1) {
            $count_only=true;
        } else {
            $count_only=false;
        }
        $searchresultSQL = '';
        // Set Default Search Options
        $imageonly = false;
        $tablelist = [];
        $tablelist_fullname = [];
        $string_where_clause='';
        $string_where_clause_nosort='';
        $login_status = $login->verify_priv('can_access_blog_manager');

        if ($login_status !== true || !isset($parameters['blogmain_published'])) {
            //If we are not an agent only show active agents, or if user did not specify show only actives by default.
            $parameters['blogmain_published']=1;
        }

        //check to see teh publishing status of this user
        $pub_status = $this->get_publisher_status($_SESSION['userID']);

        if ($pub_status === false || $pub_status === true && $_SESSION['userID'] !=1) {
            $admin_not_agent = true;
        } else {
            $admin_not_agent = false;
        }

        //Loop through search paramaters
        foreach ($parameters as $k => $v) {
            if (is_null($v) || $v == '') {
                unset($parameters[$k]);
                continue;
            }

            //Search blog by blogmain_id
            if ($k == 'blogmain_id') {
                $blogmain_id = explode(',', $v);
                $i = 0;
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
                foreach ($blogmain_id as $id) {
                    $id = intval($id);
                    if ($i == 0) {
                        $searchresultSQL .= '((' . $config['table_prefix'] . 'blogmain.blogmain_id = ' . $id . ')';
                    } else {
                        $searchresultSQL .= ' OR (' . $config['table_prefix'] . 'blogmain.blogmain_id = ' . $id . ')';
                    }
                    $i++;
                }
                $searchresultSQL .= ')';
            } elseif ($k == 'userdb_id') {
                if ($v != '' && $v != 'any') {
                    if (is_array($v)) {
                        $sstring = '';
                        foreach ($v as $u) {
                            $u = intval($u);
                            if (empty($sstring)) {
                                $sstring .=  $config['table_prefix'] . 'blogmain.userdb_id = '.$u;
                            } else {
                                $sstring .=  ' OR ' . $config['table_prefix'] . 'blogmain.userdb_id = '.$u;
                            }
                        }
                        if ($searchresultSQL != '') {
                            $searchresultSQL .= ' AND ';
                        }
                        $searchresultSQL .=  '(' . $sstring. ')';
                    } else {
                        $sql_v = intval($v);
                        if ($searchresultSQL != '') {
                            $searchresultSQL .= ' AND ';
                        }
                        $searchresultSQL .= '(' . $config['table_prefix'] . 'blogmain.userdb_id = ' . $sql_v . ')';
                    }
                }
            } elseif ($k == 'blogmain_title') {
                $safe_v = '%'.addslashes($v).'%';
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                $string_where_clause .= '(' . $config['table_prefix'] . 'blogmain.blogmain_title LIKE \''.$safe_v.'\')';
                $string_where_clause_nosort .= '(' . $config['table_prefix'] . 'blogmain.blogmain_title LIKE \''.$safe_v.'\')';
            } elseif ($k == 'blogmain_full') {
                $safe_v = strip_tags($v);
                $safe_v = '%'.addslashes($v).'%';
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                $string_where_clause .= '(' . $config['table_prefix'] . 'blogmain.blogmain_full LIKE \''.$safe_v.'\')';
                $string_where_clause_nosort .= '(' . $config['table_prefix'] . 'blogmain.blogmain_full LIKE \''.$safe_v.'\')';
            } elseif ($k == 'blogmain_description') {
                $safe_v = '%'.addslashes($v).'%';
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                $string_where_clause .= '(' . $config['table_prefix'] . 'blogmain.blogmain_description LIKE \''.$safe_v.'\')';
                $string_where_clause_nosort .= '(' . $config['table_prefix'] . 'blogmain.blogmain_description LIKE \''.$safe_v.'\')';
            } elseif ($k == 'blogmain_keywords') {
                $safe_v = strip_tags($safe_v);
                $safe_v = '%'.addslashes($v).'%';
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                $string_where_clause .= '(' . $config['table_prefix'] . 'blogmain.blogmain_keywords LIKE \''.$safe_v.'\')';
                $string_where_clause_nosort .= '(' . $config['table_prefix'] . 'blogmain.blogmain_keywords LIKE \''.$safe_v.'\')';
            } elseif ($k == 'blogmain_published') {
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                // Get any blog regardless of publish status
                if ($v == 'any') {
                    $string_where_clause .= '(' . $config['table_prefix'] . 'blogmain.blogmain_published = \'0\' 
												OR ' . $config['table_prefix'] . 'blogmain.blogmain_published = \'1\' 
												OR ' . $config['table_prefix'] . 'blogmain.blogmain_published = \'2\'
											)';
                    $string_where_clause_nosort .= '(' . $config['table_prefix'] . 'blogmain.blogmain_published = \'0\' 
														OR ' . $config['table_prefix'] . 'blogmain.blogmain_published = \'1\' 
														OR ' . $config['table_prefix'] . 'blogmain.blogmain_published = \'2\'
													)';
                }
                //Draft
                elseif ($v == 0) {
                    $string_where_clause .= '(' . $config['table_prefix'] . 'blogmain.blogmain_published = \'0\')';
                    $string_where_clause_nosort .= '(' . $config['table_prefix'] . 'blogmain.blogmain_published = \'0\')';
                }
                //Live
                elseif ($v == 1) {
                    $string_where_clause .= '(' . $config['table_prefix'] . 'blogmain.blogmain_published = \'1\')';
                    $string_where_clause_nosort .= '(' . $config['table_prefix'] . 'blogmain.blogmain_published = \'1\')';
                }
                // Pending Review
                elseif ($v == 2) {
                    $string_where_clause .= '(' . $config['table_prefix'] . 'blogmain.blogmain_published = \'2\')';
                    $string_where_clause_nosort .= '(' . $config['table_prefix'] . 'blogmain.blogmain_published = \'2\')';
                } else {
                    $string_where_clause .= '(' . $config['table_prefix'] . 'blogmain.blogmain_published = \'1\')';
                    $string_where_clause_nosort  .= '(' . $config['table_prefix'] . 'blogmain.blogmain_published = \'1\')';
                }
            }

            // creation date related searches
            //  date_equals is already handled via 'blogmain_date' => xxx
            elseif ($k == 'blog_creation_date_greater') {
                $safe_v = intval($v);
                if ($safe_v>0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    //$safe_v = $conn->DBDate($v);
                    $searchresultSQL .= ' blogmain_date > '.$safe_v;
                }
            } elseif ($k == 'blog_creation_date_less') {
                $safe_v = intval($v);
                if ($safe_v>0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    //$safe_v =$conn->DBDate($v);
                    $searchresultSQL .= ' blogmain_date < '.$safe_v;
                }
            } elseif ($k == 'blog_creation_date_equal_days') {
                $safe_v = intval($v);
                if ($safe_v>0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    //$time = mktime(0, 0, 0, date("m")  , date("d")-intval($v), date("Y"));
                    //$safe_v = $conn->DBTimeStamp($time);
                    $searchresultSQL .= ' blogmain_date = '.$safe_v;
                }
            } elseif ($k == 'blog_creation_date_greater_days') {
                $safe_v = intval($v);
                if ($v>0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    //$time = mktime(0, 0, 0, date("m")  , date("d")-intval($v), date("Y"));
                    //$safe_v = $conn->DBTimeStamp($time);;
                    $searchresultSQL .= ' blogmain_date > '.$safe_v;
                }
                //userdb_last_modified
            } elseif ($k == 'blog_creation_date_less_days') {
                $safe_v = intval($v);
                if ($v>0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    //$time = mktime(0, 0, 0, date("m")  , date("d")-intval($v), date("Y"));
                    //$safe_v = $conn->DBTimeStamp($time);
                    $searchresultSQL .= ' blogmain_date < '.$safe_v;
                }
                //userdb_last_modified
            } elseif ($k == 'blog_categories') {
                $use = false;
                $comma_separated = implode(' ', $v);
                if (trim($comma_separated) != '') {
                    $use = true;
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                }
                if ($use === true) {
                    $safe_k = 'category_id';
                    $searchresultSQL .= ' (';
                    $vitem_count = 0;
                    foreach ($v as $vitem) {
                        $safe_vitem = addslashes($vitem);
                        if ($vitem != '') {
                            if ($vitem_count != 0) {
                                $searchresultSQL .= " OR `$safe_k` = '$safe_vitem'";
                            } else {
                                $searchresultSQL .= " `$safe_k` = '$safe_vitem'";
                            }
                            $vitem_count++;
                        }
                    }
                    $searchresultSQL .= ')';
                    $tablelist[] = $config['table_prefix_no_lang'] . 'blogcategory_relation';
                }
            } elseif ($k == 'blog_post_tags') {
                $use = false;
                $comma_separated = implode(' ', $v);
                if (trim($comma_separated) != '') {
                    $use = true;
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                }
                if ($use === true) {
                    $safe_k = 'tag_id';
                    $searchresultSQL .= ' (';
                    $vitem_count = 0;
                    foreach ($v as $vitem) {
                        $safe_vitem = addslashes($vitem);
                        if ($vitem != '') {
                            if ($vitem_count != 0) {
                                $searchresultSQL .= " OR `$safe_k` = '$safe_vitem'";
                            } else {
                                $searchresultSQL .= " `$safe_k` = '$safe_vitem'";
                            }
                            $vitem_count++;
                        }
                    }
                    $searchresultSQL .= ')';
                    $tablelist[] = $config['table_prefix_no_lang'] . 'blogtag_relation';
                }
            }

            //this one is non-funtional. I need to work out the joins if it is even possible.
            elseif ($k == 'blog_comment_count_equal') {
                $safe_v = intval($v);
                if ($safe_v >= 0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $safe_k = 'blogcomments_moderated';

                    //$safe_v = $conn->DBDate($v);
                    $searchresultSQL .= " `$safe_k` = '$safe_v'";
                    $tablelist[] = $config['table_prefix'] . 'blogcomments';
                }
            }

            //Anything left must not be one of these.
            elseif ($v != '' && $k != 'cur_page' && $k != 'action' && $k != 'PHPSESSID' && $k != 'sortby' && $k != 'sorttype'
                        && $k != 'printer_friendly' && $k !='template' && $k != 'popup' && $k != 'blogmain_id' && $k != 'userdb_id'
                        && $k != 'blogmain_title' && $k != 'blogmain_title' && $k != 'blog_creation_date_equal' && $k != 'blog_creation_date_greater'
                        && $k != 'blog_creation_date_less' && $k != 'blog_creation_date_equal_days' && $k != 'blog_creation_date_greater_days'
                        && $k != 'blog_creation_date_less_days' && $k != 'blogmain_full' && $k != 'blogmain_description' && $k != 'blogmain_keywords'
                        && $k != 'blogmain_published'
                ) {
            }
        }

        // Handle Sorting
        // sort the users
        // this is the main SQL that grabs the users
        // basic sort by title..
        $group_order_text = '';
        $sortby_array=[];
        $sorttype_array=[];
        //Set array
        if (isset($sortby)&& !empty($sortby)) {
            $sortby_array= $sortby;
        }
        if (isset($sorttype)&& !empty($sorttype)) {
            $sorttype_array= $sorttype;
        }
        $sql_sort_type = '';
        $sort_text = '';
        $order_text = '';
        $group_order_text = '';
        $tablelist_nosort = $tablelist;
        $sort_count = count($sortby_array);
        for ($x = 0; $x < $sort_count; $x++) {
            //make sure user input is sanitized before adding to query string
            $sortby_array[$x]=addslashes($sortby_array[$x]);
            if (!isset($sorttype_array[$x])) {
                $sorttype_array[$x]='';
            } elseif ($sorttype_array[$x] != 'ASC' && $sorttype_array[$x] != 'DESC') {
                $sorttype_array[$x]='';
            }

            if ($sortby_array[$x] == 'blogmain_id') {
                if ($x == 0) {
                    $order_text .= 'ORDER BY blogmain_id ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',blogmain_id ' . $sorttype_array[$x];
                }
            } elseif ($sortby_array[$x] == 'userdb_id') {
                if ($x == 0) {
                    $order_text .= 'ORDER BY userdb_id ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',userdb_id ' . $sorttype_array[$x];
                }
            } elseif ($sortby_array[$x] == 'blogmain_title') {
                if ($x == 0) {
                    $order_text .= 'ORDER BY blogmain_title ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',blogmain_title ' . $sorttype_array[$x];
                }
            } elseif ($sortby_array[$x] == 'blogmain_date') {
                if ($x == 0) {
                    $order_text .= 'ORDER BY blogmain_date ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',blogmain_date ' . $sorttype_array[$x];
                }
            } elseif ($sortby_array[$x] == 'blogmain_published') {
                if ($x == 0) {
                    $order_text .= 'ORDER BY blogmain_published ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',blogmain_published ' . $sorttype_array[$x];
                }
            } elseif ($sortby_array[$x] == 'random') {
                if ($x == 0) {
                    $order_text .= 'ORDER BY rand() ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',rand() ' . $sorttype_array[$x];
                }
            }
        }
        $group_order_text = $group_order_text . ' '.$order_text;

        if ($DEBUG_SQL) {
            echo '<strong>Sort Type SQL:</strong> ' . $sql_sort_type . '<br />';
            echo '<strong>Sort Text:</strong> ' . $sort_text . '<br />';
            echo '<strong>Order Text:</strong> ' . $order_text . '<br />';
        }

        //$guidestring_with_sort = $guidestring_with_sort . $guidestring;
        // End of Sort
        $arrayLength = count($tablelist);
        if ($DEBUG_SQL) {
            echo '<strong>Table List Array Length:</strong> ' . $arrayLength . '<br />';
        }
        $string_table_list = '';
        for ($i = 0; $i < $arrayLength; $i++) {
            $string_table_list .= ' ,'  . $tablelist[$i].'';
        }
        $arrayLength = count($tablelist_nosort);
        $string_table_list_no_sort = '';
        for ($i = 0; $i < $arrayLength; $i++) {
            $string_table_list_no_sort .= ' ,'  . $tablelist[$i].'';
        }

        $arrayLength = count($tablelist_fullname);
        if ($DEBUG_SQL) {
            echo '<strong>Table List Array Length:</strong> ' . $arrayLength . '<br />';
        }
        for ($i = 0; $i < $arrayLength; $i++) {
            $string_table_list .= ' ,' . $tablelist_fullname[$i];
            $string_table_list_no_sort .= ' ,' . $tablelist_fullname[$i];
        }

        if ($DEBUG_SQL) {
            echo '<strong>Table List String:</strong> ' . $string_table_list . '<br />';
        }
        $arrayLength = count($tablelist);
        for ($i = 0; $i < $arrayLength; $i++) {
            if ($string_where_clause != '') {
                $string_where_clause .= ' AND ';
            }
            $string_where_clause .= ' (' . $config['table_prefix'] . 'blogmain.blogmain_id = ' . $tablelist[$i] . '.blogmain_id)';
        }
        $arrayLength = count($tablelist_nosort);
        for ($i = 0; $i < $arrayLength; $i++) {
            if ($string_where_clause_nosort != '') {
                $string_where_clause_nosort .= ' AND ';
            }
            $string_where_clause_nosort .= ' (' . $config['table_prefix'] . 'blogmain.blogmain_id = ' . $tablelist[$i] . '.blogmain_id)';
        }

        $searchSQL = 'SELECT distinct(' . $config['table_prefix'] . 'blogmain.blogmain_id)
					FROM ' . $config['table_prefix'] . 'blogmain ' . $string_table_list . '
					WHERE ' . $string_where_clause;

        $searchSQLCount = 'SELECT COUNT(distinct(' . $config['table_prefix'] . 'blogmain.blogmain_id)) as total_blogs
					FROM ' . $config['table_prefix'] . 'blogmain ' . $string_table_list_no_sort . '
					WHERE ' . $string_where_clause_nosort;

        if ($searchresultSQL != '') {
            $searchSQL .= ' AND ' . $searchresultSQL;
            $searchSQLCount .= ' AND ' . $searchresultSQL;
        }

        $sql = $searchSQL.' '.$sort_text.' '.$order_text;
        if ($count_only) {
            $sql = $searchSQLCount;
        }
        //$searchSQLCount = $searchSQLCount;
        // We now have a complete SQL Query. Now grab the results
        $process_time = $this->getmicrotime();
        //echo 'Limit: '.$limit .'<br>';
        //echo 'Offset: '.$offset;

        if ($limit > 0) {
            $recordSet = $conn->SelectLimit($sql, $limit, $offset);
        //$recordSet = $conn->Execute($sql . ' LIMIT '.$limit. ' OFFSET ' .$offset);
        } else {
            $recordSet = $conn->Execute($sql);
        }

        $query_time = $this->getmicrotime();
        $query_time = $query_time - $process_time;
        $process_time = $process_time - $start_time;
        if ($DEBUG_SQL) {
            echo '<strong>Search Query:</strong> ' . $sql . '<br />';
        }
        if (!$recordSet) {
            $error = $conn->ErrorMsg();
            $lapi->load_local_api('log__log_create_entry', ['log_type'=>'CRIT','log_api_command'=>'api->blog->search','log_message'=>'DB Error: '.$error. ' Full SQL: '.$sql]);
            return ['error' => true,'error_msg'=>'DB Error: '.$error."\r\n".'SQL: '.$sql];
        }
        $blogs_found=[];

        if ($count_only) {
            $blog_count = $recordSet->fields['total_bloga'];
        } else {
            $blog_count = $recordSet->RecordCount();
        }
        if (!$count_only) {
            while (!$recordSet->EOF) {
                $blogs_found[]= $recordSet->fields['blogmain_id'];
                $recordSet->MoveNext();
            }
        }
        $total_time = $this->getmicrotime();
        $total_time = $total_time - $start_time;
        $info['process_time']=sprintf('%.3f', $process_time);
        $info['query_time']=sprintf('%.3f', $query_time);
        $info['total_time']=sprintf('%.3f', $total_time);
        return [
            'error' => false,
            'blog_count' => $blog_count,
            'blogs'=>$blogs_found,
            'info'=>$info,
            'sortby'=>$sortby_array,
            'sorttype'=>$sorttype_array,
            'limit'=>$limit,
            'offset'=>$offset, ];
    }



    /**
     * This API Command creates users.
     * @param array $data $data expects an array containing the following array keys.
     *  <ul>
     *      <li>$data['class_id'] - This should be an integer of the class_id that this listing is assigned to.</li>
     *      <li>$data['listing_details'] - This should be an array containg the following three settings.</li>
     *      <li>$data['listing_details']['title'] - Required. This is the title for the listing.</li>
     *      <li>$data['listing_details']['seotitle'] - Optional. If not set the API will create a seo friendly title based on the title supplied. The API will ensure teh seotitle is unique, so the seotitle you supply will be modified if needed..</li>
     *      <li>$data['listing_details']['notes'] - Options - notes about this listing, only visible to admin and agents.</li>
     *      <li>$data['listing_details']['featured'] - Required Boolean - Is this a featured listings. true/false</li>
     *      <li>$data['listing_details']['active'] - - Required Boolean - Is this a active listings. true/false</li>
     *      <li>$data['listing_agents'] - This should be an array of up to agent ids. This sets the listing agent ID, the primary listing agent ID must be key 0.
     *      <code>$data['listing_agents'][0]=5; //This lising belongs to agent 5. All other keys are currently ignored.</code></li>
     *      <li>$data['user_fields'] - This should be an array of the actual listing data. The array keys should be the field name and the array values should be the field values. Only valid fields will be used, other data will be dropped.
     *      <code>$data['user_fields'] =array('mls_id' => 126,'address'=>'126 E Buttler Ave');  // This example defines a field value of 126 for a field called mls_id and a value of "126 E Buttler Ave" for the address field.</code></li>
     *      <li>$data['listing_media'] - Currently not used and MUST be an empty array.</li>
     *  </ul>
     * @return array
     *
     */
    /*
        public function create($data){
            global $conn,$lapi,$config,$lang, $misc;
            require_once $config['basepath'] . '/include/login.inc.php';
            $login = new login();
            $login_status = $login->verify_priv('edit_all_users');
            if ($login_status !== true && $config["demo_mode"] != 1 ) {
                return array('error' => true, 'error_msg' => 'Login Failure or Demo mode');
            }
            extract($data, EXTR_SKIP || EXTR_REFS, '');
            //Check that required settings were passed

            if(!isset($user_details) || !is_array($user_details)){
                return array('error' => true, 'error_msg' => 'user_details: correct_parameter_not_passed');
            }

            if(!isset($user_details['user_name']) || empty($user_details['user_name'])){
                return array('error' => true, 'error_msg' => '$user_details[user_name]: correct_parameter_not_passed');
            }
            if(!isset($user_details['user_first_name']) || empty($user_details['user_first_name'])){
                return array('error' => true, 'error_msg' => '$user_details[user_first_name]: correct_parameter_not_passed');
            }
            if(!isset($user_details['user_last_name']) || empty($user_details['user_last_name'])){
                return array('error' => true, 'error_msg' => '$user_details[user_last_name]: correct_parameter_not_passed');
            }
            if(!isset($user_details['emailaddress']) || empty($user_details['emailaddress'])){
                return array('error' => true, 'error_msg' => '$user_details[emailaddress]: correct_parameter_not_passed');
            }
            if(!isset($user_details['user_password']) || empty($user_details['user_password'])){
                return array('error' => true, 'error_msg' => '$user_details[user_password]: correct_parameter_not_passed');
            }
            if(!isset($user_details['active']) || $user_details['active'] !='yes' && $user_details['active'] !='no'){
                $user_details['active'] = 'no';
            }
            if(!isset($user_details['is_admin']) || $user_details['is_admin'] !='yes' && $user_details['is_admin'] !='no'){
                $user_details['is_admin'] = 'no';
            }
            if(!isset($user_details['is_agent']) || $user_details['is_agent'] !='yes' && $user_details['is_agent'] !='no'){
                $user_details['is_agent'] = 'no';
                $resource = 'member';
            }
            if(isset($user_fields) && !is_array($user_fields)){
                return array('error' => true, 'error_msg' => 'user_fields: correct_parameter_not_passed');
            }
            if (is_array($user_fields) || $user_details['is_agent'] =='yes') {
                    $resource = 'agent';
            }

            //get a list of agent or member user fields based on $resource
            $result = $lapi->load_local_api('fields__metadata',array(
                'resource'=>$resource
            ));

            if($result['error']){
            //If an error occurs die and show the error msg;
                die($result['error_msg']);
            }
            // if there are no user fields in the request
            // check the $result array to see if any are required.
            if(!isset($user_fields) || empty($user_fields) ){
                $column_names = array_column($result['fields'], 'required');
                if (in_array('Yes', $column_names))    {
                    return array('error' => true, 'error_msg' => 'user_fields: Fields set as required are missing');
                }
            }

            //Ok we this far now we bulid the user.
            $user_details['creation_date'] = time();
            $user_details['last_modified'] = time();
            $errors ='';
            $this_shall_pass = 'Yes';

            // first, make sure the user name isn't in use
            $result = $lapi->load_local_api('user__search',array(
                'parameters'=>array(
                    'userdb_user_name' => $user_details['user_name'],
                    'userdb_active' =>'any'
                ),
                'resource' =>'agent',
                'count_only'=>0
            ));
            if ($result['error']) {
                return array('error' => true, 'error_msg' => $result['error_msg']);
            }
            $num = $result['user_count'];

            // second, make sure the user email isn't in use
            $result = $lapi->load_local_api('user__search',array(
                'parameters'=>array(
                    'userdb_emailaddress' => $user_details['emailaddress'],
                    'userdb_active' =>'any'
                ),
                'resource' =>'agent',
                'count_only'=>1
            ));
            if ($result['error']) {
                return array('error' => true, 'error_msg' => $result['error_msg']);
            }
            $num2 = $result['user_count'];

            if ($num >= 1) {
                $this_shall_pass = 'No';
                $errors .= $lang['user_creation_username_taken'].'<br />';
            } // end if
            elseif ($num2 >= 1) {
                $this_shall_pass = 'No';
                $errors .= $lang['email_address_already_registered'].'<br />';
            } // end if
            if (!empty($errors)) {
                return array('error' => true, 'error_msg' =>$errors );
            }

            if ($this_shall_pass == 'Yes') {

                $sql_user_name = $misc->make_db_safe($user_details['user_name']);
                $sql_user_first_name = $misc->make_db_safe($user_details['user_first_name']);
                $sql_user_last_name = $misc->make_db_safe($user_details['user_last_name']);
                $sql_user_email = $misc->make_db_safe($user_details['emailaddress']);
                $md5_user_pass = md5($user_details['user_password']);
                $md5_user_pass = $misc->make_db_safe($md5_user_pass);
                $sql_active = strtolower($misc->make_db_safe($user_details['active']));
                $sql_isAgent = strtolower($misc->make_db_safe($user_details['is_agent']));
                $sql_isAdmin = strtolower($misc->make_db_safe($user_details['is_admin']));

                if(strtolower($user_details['is_admin']) == 'yes'){
                    $resource = 'agent';
                    //set this again because we don;t want an admin who is not an also Agent.
                    $sql_isAgent =$misc->make_db_safe("yes");

                    $sql_limitFeaturedListings = $misc->make_db_safe('-1');
                    if(isset($user_details['rank'])){
                        $sql_rank = intval($user_details['rank']);
                    }else{
                        $sql_rank = 1;
                    }
                    $sql_limitListings = $misc->make_db_safe('-1');
                    $sql_canEditSiteConfig = $misc->make_db_safe("no");
                    $sql_canEditMemberTemplate = $misc->make_db_safe("no");
                    $sql_canEditAgentTemplate = $misc->make_db_safe("no");
                    $sql_canEditListingTemplate = $misc->make_db_safe("no");
                    $sql_canFeatureListings = $misc->make_db_safe("no");
                    $sql_canViewLogs = $misc->make_db_safe("no");
                    $sql_canModerate = $misc->make_db_safe("no");
                    $sql_canPages = $misc->make_db_safe("no");
                    $sql_canVtour = $misc->make_db_safe("no");
                    $sql_canFiles = $misc->make_db_safe("no");
                    $sql_canUserFiles = $misc->make_db_safe("no");
                    $sql_canExportListings = $misc->make_db_safe("no");
                    $sql_canEditListingExpiration = $misc->make_db_safe("no");
                    $sql_canEditAllListings = $misc->make_db_safe("no");
                    $sql_canEditAllUsers = $misc->make_db_safe("no");
                    $sql_canEditPropertyClasses = $misc->make_db_safe("no");
                    $sql_canManageAddons = $misc->make_db_safe("no");
                    $sql_blogUserType = $misc->make_db_safe("4");

                }else if (strtolower($user_details['is_agent']) == 'yes') {
                    $resource = 'agent';
                    if($config["agent_default_edit_site_config"]==1){
                        $sql_canEditSiteConfig = $misc->make_db_safe('yes');
                    }else{
                        $sql_canEditSiteConfig = $misc->make_db_safe('no');
                    }
                    if($config["agent_default_edit_member_template"]==1){
                        $sql_canEditMemberTemplate = $misc->make_db_safe('yes');
                    }else{
                        $sql_canEditMemberTemplate = $misc->make_db_safe('no');
                    }
                    if($config["agent_default_edit_agent_template"]==1){
                        $sql_canEditAgentTemplate = $misc->make_db_safe('yes');
                    }else{
                        $sql_canEditAgentTemplate = $misc->make_db_safe('no');
                    }

                    if($config["agent_default_edit_listing_template"]==1){
                        $sql_canEditListingTemplate = $misc->make_db_safe('yes');
                    }else{
                        $sql_canEditListingTemplate = $misc->make_db_safe('no');
                    }
                    if($config["agent_default_feature"]==1){
                        $sql_canFeatureListings = $misc->make_db_safe('yes');
                    }else{
                        $sql_canFeatureListings = $misc->make_db_safe('no');
                    }
                    if($config["agent_default_logview"]==1){
                        $sql_canViewLogs = $misc->make_db_safe('yes');
                    }else{
                        $sql_canViewLogs = $misc->make_db_safe('no');
                    }
                    if($config["agent_default_moderate"]==1){
                        $sql_canModerate = $misc->make_db_safe('yes');
                    }else{
                        $sql_canModerate = $misc->make_db_safe('no');
                    }
                    if($config["agent_default_editpages"]==1){
                        $sql_canPages = $misc->make_db_safe('yes');
                    }else{
                        $sql_canPages = $misc->make_db_safe('no');
                    }
                    if($config["agent_default_havevtours"]==1){
                        $sql_canVtour = $misc->make_db_safe('yes');
                    }else{
                        $sql_canVtour = $misc->make_db_safe('no');
                    }
                    if($config["agent_default_havefiles"]==1){
                        $sql_canFiles = $misc->make_db_safe('yes');
                    }else{
                        $sql_canFiles = $misc->make_db_safe('no');
                    }
                    if($config["agent_default_haveuserfiles"]==1){
                        $sql_canUserFiles = $misc->make_db_safe('yes');
                    }else{
                        $sql_canUserFiles = $misc->make_db_safe('no');
                    }
                    $sql_limitListings = $misc->make_db_safe($config["agent_default_num_listings"]);
                    $sql_limitFeaturedListings = $misc->make_db_safe($config["agent_default_num_featuredlistings"]);

                    if($config["agent_default_can_export_listings"]==1){
                        $sql_canExportListings = $misc->make_db_safe('yes');
                    }else{
                        $sql_canExportListings = $misc->make_db_safe('no');
                    }
                    if($config["agent_default_canChangeExpirations"]==1){
                        $sql_canEditListingExpiration = $misc->make_db_safe('yes');
                    }else{
                        $sql_canEditListingExpiration = $misc->make_db_safe('no');
                    }
                    if($config["agent_default_edit_all_listings"]==1){
                        $sql_canEditAllListings = $misc->make_db_safe('yes');
                    }else{
                        $sql_canEditAllListings = $misc->make_db_safe('no');
                    }
                    if($config["agent_default_edit_all_users"]==1){
                        $sql_canEditAllUsers = $misc->make_db_safe('yes');
                    }else{
                        $sql_canEditAllUsers = $misc->make_db_safe('no');
                    }

                    if($config["agent_default_edit_property_classes"]==1){
                        $sql_canEditPropertyClasses = $misc->make_db_safe('yes');
                    }else{
                        $sql_canEditPropertyClasses = $misc->make_db_safe('no');
                    }

                    if($config["agent_default_canManageAddons"]==1){
                        $sql_canManageAddons = $misc->make_db_safe('yes');
                    }else{
                        $sql_canManageAddons = $misc->make_db_safe('no');
                    }
                    if(isset($user_details['rank'])){
                        $sql_rank = intval($user_details['rank']);
                    }else{
                        //could improve this to set to last rank position.
                        $sql_rank = 5;
                    }
                    $sql_blogUserType = $misc->make_db_safe($config["agent_default_blogUserType"]);


                }
                else {
                    $resource = 'member';
                    $sql_canEditSiteConfig = $misc->make_db_safe("no");
                    $sql_canEditMemberTemplate = $misc->make_db_safe("no");
                    $sql_canEditAgentTemplate = $misc->make_db_safe("no");
                    $sql_canEditListingTemplate = $misc->make_db_safe("no");
                    $sql_canFeatureListings = $misc->make_db_safe("no");
                    $sql_canViewLogs = $misc->make_db_safe("no");
                    $sql_canModerate = $misc->make_db_safe("no");
                    $sql_canPages = $misc->make_db_safe("no");
                    $sql_canVtour = $misc->make_db_safe("no");
                    $sql_canFiles = $misc->make_db_safe("no");
                    $sql_canUserFiles = $misc->make_db_safe("no");
                    $sql_canExportListings = $misc->make_db_safe("no");
                    $sql_canEditListingExpiration = $misc->make_db_safe("no");
                    $sql_canEditAllListings = $misc->make_db_safe("no");
                    $sql_canEditAllUsers = $misc->make_db_safe("no");
                    $sql_limitListings = 0;
                    $sql_limitFeaturedListings = 0;
                    $sql_rank =0;
                    $sql_canEditPropertyClasses = $misc->make_db_safe("no");
                    $sql_canManageAddons = $misc->make_db_safe("no");
                    $sql_blogUserType = $misc->make_db_safe("1");
                }
                // create the account
                $sql = 'INSERT INTO ' . $config['table_prefix'] . 'userdb (
                            userdb_user_name,
                            userdb_user_password,
                            userdb_user_first_name,
                            userdb_user_last_name,
                            userdb_emailAddress,
                            userdb_creation_date,
                            userdb_last_modified,
                            userdb_active,
                            userdb_is_agent,
                            userdb_is_admin,
                            userdb_can_edit_member_template,
                            userdb_can_edit_agent_template,
                            userdb_can_edit_listing_template,
                            userdb_can_feature_listings,
                            userdb_can_view_logs,
                            userdb_can_moderate,
                            userdb_can_edit_pages,
                            userdb_can_have_vtours,
                            userdb_can_have_files,
                            userdb_can_have_user_files,
                            userdb_limit_listings,
                            userdb_comments,
                            userdb_hit_count,
                            userdb_can_edit_expiration,
                            userdb_can_export_listings,
                            userdb_can_edit_all_users,
                            userdb_can_edit_all_listings,
                            userdb_can_edit_site_config,
                            userdb_can_edit_property_classes,
                            userdb_can_manage_addons,
                            userdb_rank,
                            userdb_featuredlistinglimit,
                            userdb_email_verified,
                            userdb_blog_user_type
                        )
                        VALUES(
                            ' . $sql_user_name . ',
                            ' . $md5_user_pass . ',
                            ' . $sql_user_first_name . ',
                            ' . $sql_user_last_name . ',
                            ' . $sql_user_email . ',
                            ' . $conn->DBDate(time()) . ',
                            ' . $conn->DBTimeStamp(time()) . ',
                            ' . $sql_active . ',
                            ' . $sql_isAgent . ',
                            ' . $sql_isAdmin . ',
                            ' . $sql_canEditMemberTemplate . ',
                            ' . $sql_canEditAgentTemplate . ',
                            ' . $sql_canEditListingTemplate . ',
                            ' . $sql_canFeatureListings . ',
                            ' . $sql_canViewLogs . ',
                            ' . $sql_canModerate . ',
                            ' . $sql_canPages . ',
                            ' . $sql_canVtour . ',
                            ' . $sql_canFiles . ',
                            ' . $sql_canUserFiles . ',
                            ' . $sql_limitListings . ',
                            \'\',
                            0,
                            ' . $sql_canEditListingExpiration . ',
                            ' . $sql_canExportListings . ',
                            ' . $sql_canEditAllUsers . ',
                            ' . $sql_canEditAllListings . ',
                            ' . $sql_canEditSiteConfig . ',
                            ' . $sql_canEditPropertyClasses . ',
                            ' . $sql_canManageAddons . ',
                            ' . $sql_rank . ',
                            ' . $sql_limitFeaturedListings . ',
                            \'yes\',
                            '.$sql_blogUserType.'
                        )';
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $new_user_id = $conn->Insert_ID(); // this is the new user's ID number

                // Insert custom user fields
                if(is_array($user_fields)){
                    $sql = 'SELECT '.$resource.'formelements_default_text,
                                    '.$resource.'formelements_field_name,
                                    '.$resource.'formelements_id,
                                    '.$resource.'formelements_field_type,
                                    '.$resource.'formelements_field_elements,
                                    '.$resource.'formelements_required
                            FROM  ' . $config['table_prefix'] . $resource.'formelements';
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $error = $conn->ErrorMsg();
                        $lapi->load_local_api('log__log_create_entry',array('log_type'=>'CRIT','log_api_command'=>'api->user->create','log_message'=>'DB Error: '.$error));
                        return array('error' => true,'error_msg'=>'DB Error: '.$error."\r\n".'SQL: '.$sql);
                    }
                    //Verify user fields passed via API exist
                    while(!$recordSet->EOF{
                        $name = $recordSet->fields[$resource.'formelements_field_name'];
                        $id = $recordSet->fields[$resource.'formelements_id'];
                        $data_type = $recordSet->fields[$resource.'formelements_field_type'];
                        $data_elements = $recordSet->fields[$resource.'formelements_field_elements'];
                        $default_text = $recordSet->fields[$resource.'formelements_default_text'];
                        if(array_key_exists($name, $user_fields)){

                            switch ($data_type){
                                case 'number':
                                    if($user_fields[$name] === ""){
                                        $insert_user_fields[$name]=null;
                                    }else{
                                        if(is_numeric($user_fields[$name])){
                                            $insert_user_fields[$name] = $user_fields[$name];
                                        }else{
                                            $price = str_replace(',','',$user_fields[$name]);
                                            $insert_user_fields[$name] = intval($price);
                                        }
                                    }
                                    break;
                                case 'decimal':
                                case 'price':
                                    if($user_fields[$name] === ""){
                                        $insert_user_fields[$name]=null;
                                    }else{
                                        $price = str_replace(',','',$user_fields[$name]);
                                        $insert_user_fields[$name] = (float)$price;
                                    }
                                    break;
                                case 'date':
                                        if($user_fields[$name] === ""){
                                            $insert_user_fields[$name]=null;
                                        }else{
                                            $insert_user_fields[$name] = $this->convert_date($user_fields[$name],$or_date_format);
                                        }
                                        break;
                                case 'select':
                                case 'select-multiple':
                                case 'option':
                                case 'checkbox':
                                    //This is a lookup field. Make sure values passed are allowed by the system.
                                    //Get Array of allowed data elements
                                    $data_elements_array=explode('||',$data_elements);
                                    //Get array of passed data eleements
                                    if(!is_array($user_fields[$name])){
                                        $t_value = $user_fields[$name];
                                        unset($user_fields[$name]);
                                        $user_fields[$name][]=$t_value;
                                    }
                                    $good_elements=array();
                                    foreach($user_fields[$name] as $fvalue){
                                        if(in_array($fvalue,$data_elements_array) && !in_array($fvalue,$good_elements)){
                                            $good_elements[]=$fvalue;
                                        }
                                    }
                                    $insert_user_fields[$name]=$good_elements;
                                    break;
                                default:
                                    $insert_user_fields[$name] = $user_fields[$name];
                                    break;
                            }
                        }else{
                            if($default_text!=''){
                                $insert_user_fields[$name]=$default_text;
                            }else{
                                $insert_user_fields[$name]='';
                            }
                        }

                        $recordSet->Movenext();
                    }
                    $sql = "INSERT INTO " . $config['table_prefix'] . "userdbelements (userdbelements_field_name, userdbelements_field_value, userdb_id) VALUES ";
                    $sql2=array();
                    foreach ($insert_user_fields as $name => $value){
                        $sql_name = addslashes($name);
                        if(is_array($value)){
                            $sql_value = addslashes(implode('||',$value));
                        }else{
                            $sql_value = addslashes($value);
                        }
                        $sql2[] = "('$sql_name', '$sql_value', $new_user_id)";

                    }
                    if(count($sql2)>0){
                        $sql .= implode(',',$sql2);
                        $recordSet = $conn->Execute($sql);
                        if (!$recordSet) {
                            $error = $conn->ErrorMsg();
                            $lapi->load_local_api('log__log_create_entry',array('log_type'=>'CRIT','log_api_command'=>'api->user->create','log_message'=>'DB Error: '.$error));
                            return array('error' => true,'error_msg'=>'DB Error: '.$error."\r\n".'SQL: '.$sql);
                        }
                    }
                }

                //call the new user hook
                require_once($config['basepath'] . '/include/hooks.inc.php');
                $hooks = new hooks();
                $hooks->load('after_user_signup',$new_user_id);

                return array('error' => false,'user_id'=>$new_user_id);
            }
        }

    */


    /**
     * This API Command reads user info
     * @param array $data $data expects an array containing the following array keys.
     *  <ul>
     *      <li>$data['userdb_id'] - This is the Listing ID that we are updating.</li>
     *      <li>$data['resource'] - This is the resource you want to get fields for. Allowed Options are: 'agent' or 'member'</li>
     *      <li>$data['fields'] - This is an optional array of fields to retrieve, if left empty or not passed all fields will be retrieved.</li>
     *  </ul>
     * @return array
     **/
    public function read($data)
    {
        global $conn, $lapi, $config, $lang, $misc, $page;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('can_access_blog_manager');

        extract($data, EXTR_SKIP || EXTR_REFS, '');

        //sanitize $data input
        //$fields = (filter_var_array($fields,  FILTER_SANITIZE_STRING));
        //print_r ($fields);

        //Check that required settings were passed
        if (!isset($blog_id) || !is_numeric($blog_id)) {
            return ['error' => true, 'error_msg' => 'blog_id: correct_parameter_not_passed'];
        }
        if (isset($fields) && !is_array($fields)) {
            return ['error' => true, 'error_msg' => 'fields: correct_parameter_not_passed'];
        }
        //If no fields were passed make an empty array to save checking for if !isset later
        if (!isset($fields)) {
            $fields=[];
        }

        //This array will hold our blog data
        $blog_data=[];

        if (!$security) {
            $suffix_published = ' AND blogmain_published = \'1\'';
            $suffix_moderated = ' AND blogcomments_moderated = \'1\'';
        } else {
            $suffix_published ='';
            $suffix_moderated = '';
        }

        //Get Base user information
        if (empty($fields)) {
            $sql = 'SELECT '.implode(',', $this->OR_INT_FIELDS).' 
					FROM '.$config['table_prefix'].'blogmain 
					WHERE blogmain_id = '.$blog_id .' 
					'.$suffix_published;

            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $error = $conn->ErrorMsg();
                $lapi->load_local_api('log__log_create_entry', ['log_type'=>'CRIT','log_api_command'=>'api->blog->read','log_message'=>'DB Error: '.$error]);
                return ['error' => true,'error_msg'=>'DB Error: '.$error."\r\n".'SQL: '.$sql];
            }

            if ($recordSet->RecordCount() == 1) {
                foreach ($this->OR_INT_FIELDS as $field) {
                    $blog_data[$field]=$recordSet->fields[$field];
                }
            }

            //Get Name of Blog Author
            $result = $lapi->load_local_api('user__read', [
                'user_id' => $blog_data['userdb_id'],
                'resource' => 'agent',
                'fields' => [
                    'userdb_user_first_name',
                    'userdb_user_last_name',
                ],
            ]);

            $blog_data['blog_author_firstname'] = $result['user']['userdb_user_first_name'];
            $blog_data['blog_author_lastname'] = $result['user']['userdb_user_last_name'];
            $blog_data['blog_categories'] = $this->get_blog_categories_assignment_names($blog_id);
            $blog_data['blog_post_tags'] = $this->get_blog_tag_assignment($blog_id);

            //get comment count and comments
            $sql = 'SELECT userdb_id,blogcomments_timestamp, blogcomments_text, blogcomments_id, blogcomments_moderated 
					FROM ' . $config['table_prefix'] . 'blogcomments 
					WHERE blogmain_id = '.$blog_id. ' '.
                    $suffix_moderated .'
					ORDER BY blogcomments_timestamp ASC;';
            $recordSet = $conn->Execute($sql);
            if ($recordSet === false) {
                $error = $conn->ErrorMsg();
                $lapi->load_local_api('log__log_create_entry', ['log_type'=>'CRIT','log_api_command'=>'api->blog->read','log_message'=>'DB Error: '.$error]);
                return ['error' => true,'error_msg'=>'DB Error: '.$error."\r\n".'SQL: '.$sql];
            }

            $blog_data['blog_url'] = $page->magicURIGenerator('blog', $blog_id, true);
            $blog_data['blog_comment_count'] = $recordSet->RecordCount();

            while (!$recordSet->EOF) {
                $blog_data['blog_comments'][$recordSet->fields['blogcomments_id']]['userdb_id'] = $recordSet->fields['userdb_id'];
                $blog_data['blog_comments'][$recordSet->fields['blogcomments_id']]['blogcomments_id'] = $recordSet->fields['blogcomments_id'];
                $blog_data['blog_comments'][$recordSet->fields['blogcomments_id']]['blogcomments_timestamp'] = $recordSet->fields['blogcomments_timestamp'];
                $blog_data['blog_comments'][$recordSet->fields['blogcomments_id']]['blogcomments_text'] = $recordSet->fields['blogcomments_text'];
                $blog_data['blog_comments'][$recordSet->fields['blogcomments_id']]['blogcomments_moderated'] = $recordSet->fields['blogcomments_moderated'];
                $recordSet->MoveNext();
            }
        } else {
            $core_fields = array_intersect($this->OR_INT_FIELDS, $fields);
            $noncore_fields = array_diff($fields, $this->OR_INT_FIELDS);

            if (!empty($core_fields)) {
                $sql = 'SELECT '.implode(',', $core_fields).' 
						FROM '.$config['table_prefix'].'blogmain 
						WHERE blogmain_id = '.$blog_id .' '.$suffix;

                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $error = $conn->ErrorMsg();
                    $lapi->load_local_api('log__log_create_entry', ['log_type'=>'CRIT','log_api_command'=>'api->blog->read','log_message'=>'DB Error: '.$error]);
                    return ['error' => true,'error_msg'=>'DB Error: '.$error."\r\n".'SQL: '.$sql];
                }

                if ($recordSet->RecordCount() == 1) {
                    foreach ($core_fields as $field) {
                        $blog_data[$field]=$recordSet->fields[$field];
                    }
                }
            }
            if (!empty($noncore_fields)) {
                if (in_array('blog_url', $noncore_fields)) {
                    $blog_data['blog_url'] = $page->magicURIGenerator('blog', $blog_id, true);
                }
                // if either of these files is set, do stuff.
                if (in_array('blog_comment_count', $noncore_fields) || in_array('blog_comments', $noncore_fields)) {
                    //if (isset ($noncore_fields['blog_comment_count']) || isset ($noncore_fields['comments']) ) {

                    //get comment count and comments
                    $sql = 'SELECT userdb_id,blogcomments_timestamp, blogcomments_text, blogcomments_id, blogcomments_moderated 
							FROM ' . $config['table_prefix'] . 'blogcomments 
							WHERE blogmain_id = '.$blog_id.' 
							AND blogcomments_moderated = 1 
							ORDER BY blogcomments_timestamp ASC;';
                    $recordSet = $conn->Execute($sql);

                    if ($recordSet === false) {
                        $error = $conn->ErrorMsg();
                        $lapi->load_local_api('log__log_create_entry', ['log_type'=>'CRIT','log_api_command'=>'api->blog->read','log_message'=>'DB Error: '.$error]);
                        return ['error' => true,'error_msg'=>'DB Error: '.$error."\r\n".'SQL: '.$sql];
                    }

                    $blog_data['blog_comment_count'] = $recordSet->RecordCount();

                    // if blog_comments is set generate that stuff.
                    if (in_array('blog_comments', $noncore_fields)) {
                        while (!$recordSet->EOF) {
                            $blog_data['blog_comments'][$recordSet->fields['blogcomments_id']]['userdb_id'] = $recordSet->fields['userdb_id'];
                            $blog_data['blog_comments'][$recordSet->fields['blogcomments_id']]['blogcomments_id'] = $recordSet->fields['blogcomments_id'];
                            $blog_data['blog_comments'][$recordSet->fields['blogcomments_id']]['blogcomments_timestamp'] = $recordSet->fields['blogcomments_timestamp'];
                            $blog_data['blog_comments'][$recordSet->fields['blogcomments_id']]['blogcomments_text'] = $recordSet->fields['blogcomments_text'];
                            $blog_data['blog_comments'][$recordSet->fields['blogcomments_id']]['blogcomments_moderated'] = $recordSet->fields['blogcomments_moderated'];
                            $recordSet->MoveNext();
                        }
                    }
                }
                if (in_array('blog_author_firstname', $noncore_fields) || in_array('blog_author_lastname', $noncore_fields)) {
                    //Get Name of Blog Author
                    $result = $lapi->load_local_api('user__read', [
                        'user_id' => $blog_data['userdb_id'],
                        'resource' => 'agent',
                        'fields' => [
                            'userdb_user_first_name',
                            'userdb_user_last_name',
                        ],
                    ]);

                    if (in_array('blog_author_firstname', $noncore_fields)) {
                        $blog_data['blog_author_firstname'] = $result['user']['userdb_user_first_name'];
                    }
                    if (in_array('blog_author_lastname', $noncore_fields)) {
                        $blog_data['blog_author_lastname'] = $result['user']['userdb_user_last_name'];
                    }
                }
                if (in_array('blog_categories', $noncore_fields)) {
                    $blog_data['blog_categories'] = $this->get_blog_categories_assignment_names($blog_id);
                }
                if (in_array('blog_post_tags', $noncore_fields)) {
                    $blog_data['blog_post_tags'] = $this->get_blog_tag_assignment($blog_id);
                }
            }
        }
        return ['error' => false,'blog'=>$blog_data];
    }



    /**
     * This API Command updates blogs.
     * @param array $data $data expects an array containing the following array keys.
     *  <ul>
     *      <li>$data['blogmain_id'] - This is the Listing ID that we are updating.</li>
     *      <li>$data['user_details'] - This should be an array containg the following three settings.</li>
     *      <li>$data['user_details']['active'] -Set if this a active listings, only set if you need to change. true/false</li>
     *      <li>$data['user_details'] - This should be an array of the actual listing data. The array keys should be the field name and the array values should be the field values. Only valid fields will be used, other data will be dropped.
     *      <code>$data['user_fields'] =array('phone' => '555-555-1212','fax'=>'555-555-1313');  </code></li>
     *  </ul>
     * @return array
     *
     */
    /*
        public function update($data){
            global $conn, $config, $lang, $lapi;

            require_once $config['basepath'] . '/include/login.inc.php';
            $login = new login();
            $login_status = false;
            $has_permission=true;

            extract($data, EXTR_SKIP || EXTR_REFS, '');

            //setup array that will contain our SQL fields for update
            $sql_fields=array();

            //Check that required settings were passed
            if(!isset($blog_id)||!is_numeric($blog_id)){
                return array('error' => true, 'error_msg' => 'blog_id: correct_parameter_not_passed');
            }
            else{
                $userdb_id = intval($blog_id);
                //get user status (resource) of user
                $is_admin = $misc->get_admin_status($userdb_id);
                $is_agent = $misc->get_agent_status($userdb_id);

                if ($is_agent === true || $is_admin === true) {
                    $resource = 'agent';
                }
                else {
                    $resource = 'member';
                }

                //does this user exist?
                $resultc = $lapi->load_local_api('user__search', array(
                    'parameters'=>array(
                        'userdb_id' =>$userdb_id
                       ),
                    'resource' =>$resource
                ));
                if ($resultc['user_count'] ==0) {
                    return array('error' => true, 'error_msg' => 'blog_id: '.$userdb_id.' not present');
                }

            }

            //check permissions.
            if($_SESSION['userID'] != $user_id){
                $login_status = $login->verify_priv('edit_all_users');
                if (!$login_status) {
                    $has_permission=false;
                }
            }

            //Check that required settings were passed
            if(!isset($userdb_id)||!is_numeric($userdb_id)){
                return array('error' => true, 'error_msg' => 'blog_id: correct_parameter_not_passed');
            }
            if(isset($user_details) && !is_array($user_details)){
                return array('error' => true, 'error_msg' => 'user_details: correct_parameter_not_passed');
            }
            if(isset($user_fields) && !is_array($user_fields)){
                return array('error' => true, 'error_msg' => 'user_fields: correct_parameter_not_passed');
            }

            //Non-editable fields.
            //can't touch this
            if(isset($user_details['id']) && !empty($user_details['id']) ) {
                return array('error' => true, 'error_msg' => 'user_details[id]: User ID# (userdb_id) cannot be changed.');
            }
            //can't touch this
            if(isset($user_details['user_name']) && !empty($user_details['user_name']) ) {
                return array('error' => true, 'error_msg' => 'user_details[user_name]: Username cannot be changed.');
            }
            //can't touch this
            if(isset($user_details['is_admin']) && !empty($user_details['is_admin'])){
                return array('error' => true, 'error_msg' => 'user_details[is_admin]: Status cannot be changed');
            }
            //can't touch this
            if(isset($user_details['is_agent']) && !empty($user_details['is_agent'])){
                return array('error' => true, 'error_msg' => 'user_details[is_agent]: Status cannot be changed');
            }
            //can't touch this
            if(isset($user_details['creation_date']) && empty($user_details['creation_date']) ) {
                return array('error' => true, 'error_msg' => 'user_details[creation_date]: cannot be modified');
            }
            //can't touch this
            if(isset($user_details['last_modified']) && empty($user_details['last_modified']) ) {
                return array('error' => true, 'error_msg' => 'user_details[last_modified]: cannot be modified');
            }


            //these can't be set empty, and have lowest restrictions
            if(isset($user_details['user_first_name']) && empty($user_details['user_first_name']) ) {
                return array('error' => true, 'error_msg' => 'user_details[user_first_name]: cannot be empty');
            }
            else if(isset($user_details['user_first_name']) ){
                $sql_fields['userdb_user_first_name'] = $user_details['user_first_name'];
            }

            if(isset($user_details['user_last_name']) && empty($user_details['user_last_name']) ) {
                return array('error' => true, 'error_msg' => 'user_details[user_last_name]: cannot be empty');
            }
            else if(isset($user_details['user_last_name']) ) {
                $sql_fields['userdb_user_last_name'] = $user_details['user_last_name'];
            }
            if( isset($user_details['emailaddress']) && !filter_var($user_details['emailaddress'], FILTER_VALIDATE_EMAIL) ){
                return array('error' => true, 'error_msg' => 'user_details[emailaddress]: not a valid address');
            }
            else if( isset($user_details['emailaddress']) ){
                //make sure this address does not already exist
                $result = $lapi->load_local_api('user__search',array(
                    'parameters'=>array(
                        'userdb_emailaddress' => $user_details['emailaddress'],
                        'userdb_active' =>'any'
                    ),
                    'resource' =>$resource,
                    'count_only'=>1
                ));
                if ($result['error']) {
                    return array('error' => true, 'error_msg' => $result['error_msg']);
                }
                $num = $result['user_count'];
                if ($num >= 1){
                    return array('error' => true, 'error_msg' => 'user_details[emailaddress]: address already exists');
                }
                else {
                    $sql_fields['userdb_emailaddress'] = $user_details['emailaddress'];
                }

            }

            if(isset($user_details['user_password']) && empty($user_details['user_password']) ) {
                return array('error' => true, 'error_msg' => 'user_details[user_password]: cannot be empty');
            }
            else if(isset($user_details['user_password']) ) {
                    $sql_fields['userdb_user_password'] = md5($user_details['user_password']);
            }

            //Admin or edit_all_users permissions required
            if (($login_status===true || $_SESSION['admin_privs'] == 'yes')) {

                if(isset($user_details['active']) && !is_bool($user_details['active'])){
                    return array('error' => true, 'error_msg' => 'user_details[active]: correct_parameter_not_passed');
                }
                else if(isset($user_details['active'])){
                    if($user_details['active'] ==true){
                        $sql_fields['userdb_active'] = 'yes';
                    }
                    else{
                        $sql_fields['userdb_active'] = 'no';
                    }
                }
                //See if the user is currently active.
                $api_result = $lapi->load_local_api('user__read',array('blog_id'=>$blog_id,'fields'=>array('userdb_active')));
                if(!$api_result['error']){
                    $oldstatus = $api_result['user']['userdb_active'];
                }

                if(isset($user_details['hit_count']) && !is_int($user_details['hit_count']) ) {
                    return array('error' => true, 'error_msg' => 'user_details[hit_count]: correct_parameter_not_passed');
                }
                else if(isset($user_details['hit_count']) ) {
                    $sql_fields['userdb_hit_count'] = intval($user_details['hit_count']);
                }

                //check and set values only if an Agent account.
                if ($is_agent ===true) {
                    //
                    //boolean permission fields. These can only be set by an Admin or an Agent with edit_all_users permissions
                    if(isset($user_details['can_edit_site_config']) && !is_bool($user_details['can_edit_site_config']) ){
                        return array('error' => true, 'error_msg' => 'user_details[can_edit_site_config]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_edit_site_config']) ) {
                        if($listing_details['can_edit_site_config']){
                            $sql_fields['userdb_can_edit_site_config'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_edit_site_config'] = 'no';
                        }
                    }

                    if(isset($user_details['can_edit_member_template']) && !is_bool($user_details['can_edit_member_template']) ){
                        return array('error' => true, 'error_msg' => 'user_details[can_edit_member_template]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_edit_member_template']) ) {
                        if($listing_details['can_edit_member_template']){
                            $sql_fields['userdb_can_edit_member_template'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_edit_member_template'] = 'no';
                        }
                    }

                    if(isset($user_details['can_edit_agent_template']) && !is_bool($user_details['can_edit_agent_template']) ){
                        return array('error' => true, 'error_msg' => 'user_details[can_edit_agent_template]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_edit_agent_template']) ) {
                        if($listing_details['can_edit_agent_template']){
                            $sql_fields['userdb_can_edit_agent_template'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_edit_agent_template'] = 'no';
                        }
                    }

                    if(isset($user_details['can_edit_listing_template']) && !is_bool($user_details['can_edit_listing_template'])){
                        return array('error' => true, 'error_msg' => 'user_details[can_edit_listing_template]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_edit_listing_template']) ) {
                        if($listing_details['can_edit_listing_template']){
                            $sql_fields['userdb_can_edit_listing_template'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_edit_listing_template'] = 'no';
                        }
                    }

                    if(isset($user_details['can_feature_listings']) && !is_bool($user_details['can_feature_listings'])){
                        return array('error' => true, 'error_msg' => 'user_details[can_feature_listings]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_feature_listings']) ) {
                        if($listing_details['can_feature_listings']){
                            $sql_fields['userdb_can_feature_listings'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_feature_listings'] = 'no';
                        }
                    }

                    if(isset($user_details['can_view_logs']) && !is_bool($user_details['can_view_logs'])){
                        return array('error' => true, 'error_msg' => 'user_details[can_view_logs]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_view_logs']) ) {
                        if($listing_details['can_view_logs']){
                            $sql_fields['userdb_can_view_logs'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_view_logs'] = 'no';
                        }
                    }

                    if(isset($user_details['can_moderate']) && !is_bool($user_details['can_moderate'])){
                        return array('error' => true, 'error_msg' => 'user_details[can_moderate]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_moderate']) ) {
                        if($listing_details['can_edit_site_config']){
                            $sql_fields['userdb_can_edit_site_config'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_edit_site_config'] = 'no';
                        }
                    }

                    if(isset($user_details['can_edit_pages']) && !is_bool($user_details['can_edit_pages'])){
                        return array('error' => true, 'error_msg' => 'user_details[can_edit_pages]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_edit_pages']) ) {
                        if($listing_details['can_edit_pages']){
                            $sql_fields['userdb_can_edit_pages'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_edit_pages'] = 'no';
                        }
                    }

                    if(isset($user_details['can_have_vtours']) && !is_bool($user_details['can_have_vtours'])){
                        return array('error' => true, 'error_msg' => 'user_details[can_have_vtours]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_have_vtours']) ) {
                        if($listing_details['can_have_vtours']){
                            $sql_fields['userdb_can_have_vtours'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_have_vtours'] = 'no';
                        }
                    }

                    if(isset($user_details['can_edit_expiration']) && !is_bool($user_details['can_edit_expiration'])){
                        return array('error' => true, 'error_msg' => 'user_details[can_edit_expiration]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_edit_expiration']) ) {
                        if($listing_details['can_edit_expiration']){
                            $sql_fields['userdb_can_edit_expiration'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_edit_expiration'] = 'no';
                        }
                    }

                    if(isset($user_details['can_export_listings']) && !is_bool($user_details['can_export_listings'])){
                        return array('error' => true, 'error_msg' => 'user_details[can_export_listings]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_export_listings']) ) {
                        if($listing_details['can_export_listings']){
                            $sql_fields['userdb_can_export_listings'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_export_listings'] = 'no';
                        }
                    }

                    if(isset($user_details['can_edit_all_users']) && !is_bool($user_details['can_edit_all_users'])){
                        return array('error' => true, 'error_msg' => 'user_details[can_edit_all_users]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_edit_all_users']) ) {
                        if($listing_details['can_edit_all_users']){
                            $sql_fields['userdb_can_edit_all_users'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_edit_all_users'] = 'no';
                        }
                    }

                    if(isset($user_details['can_edit_all_listings']) && !is_bool($user_details['can_edit_all_listings'])){
                        return array('error' => true, 'error_msg' => 'user_details[can_edit_all_listings]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_edit_all_listings']) ) {
                        if($listing_details['can_edit_all_listings']){
                            $sql_fields['userdb_can_edit_all_listings'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_edit_all_listings'] = 'no';
                        }
                    }

                    if(isset($user_details['can_edit_property_classes']) && !is_bool($user_details['can_edit_property_classes'])){
                        return array('error' => true, 'error_msg' => 'user_details[can_edit_property_classes]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_edit_property_classes']) ) {
                        if($listing_details['can_edit_property_classes']){
                            $sql_fields['userdb_can_edit_property_classes'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_edit_property_classes'] = 'no';
                        }
                    }

                    if(isset($user_details['can_have_files']) && !is_bool($user_details['can_have_files'])){
                        return array('error' => true, 'error_msg' => 'user_details[can_have_files]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_have_files']) ) {
                        if($listing_details['can_have_files']){
                            $sql_fields['userdb_can_have_files'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_have_files'] = 'no';
                        }
                    }

                    if(isset($user_details['can_have_user_files']) && !is_bool($user_details['can_have_user_files'])){
                        return array('error' => true, 'error_msg' => 'user_details[can_have_user_files]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_have_user_files']) ) {
                        if($listing_details['can_have_user_files']){
                            $sql_fields['userdb_can_have_user_files'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_have_user_files'] = 'no';
                        }
                    }

                    if(isset($user_details['can_manage_addons']) && !is_bool($user_details['can_manage_addons'])){
                        return array('error' => true, 'error_msg' => 'user_details[can_manage_addons]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_manage_addons']) ) {
                        if($listing_details['can_manage_addons']){
                            $sql_fields['userdb_can_manage_addons'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_manage_addons'] = 'no';
                        }
                    }

                    if(isset($user_details['can_edit_all_leads']) && !is_bool($user_details['can_edit_all_leads'])){
                        return array('error' => true, 'error_msg' => 'user_details[can_edit_all_leads]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_edit_all_leads']) ) {
                        if($listing_details['can_edit_all_leads']){
                            $sql_fields['userdb_can_edit_all_leads'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_edit_all_leads'] = 'no';
                        }
                    }

                    if(isset($user_details['can_edit_lead_template']) && !is_bool($user_details['can_edit_lead_template'])){
                        return array('error' => true, 'error_msg' => 'user_details[can_edit_lead_template]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['can_edit_lead_template']) ) {
                        if($listing_details['can_edit_lead_template']){
                            $sql_fields['userdb_can_edit_lead_template'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_can_edit_lead_template'] = 'no';
                        }
                    }

                    //
                    //misc settings. These can also only be set by an Admin or an Agent with edit_all_users permissions
                    if(isset($user_details['comments']) && empty($user_details['comments']) ) {
                        return array('error' => true, 'error_msg' => 'user_details[comments]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['comments']) ) {
                        $sql_fields['userdb_comments'] = addslashes($user_details['comments']);
                    }

                    if(isset($user_details['limit_listings']) && !is_int($user_details['limit_listings']) ) {
                        return array('error' => true, 'error_msg' => 'user_details[limit_listings]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['limit_listings']) ) {
                        $sql_fields['userdb_limit_listings'] = intval($user_details['limit_listings']);
                    }

                    if(isset($user_details['blog_user_type']) && !is_int($user_details['blog_user_type']) ) {
                        return array('error' => true, 'error_msg' => 'user_details[blog_user_type]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['blog_user_type']) ) {
                        $sql_fields['userdb_blog_user_type'] = intval($user_details['blog_user_type']);
                    }

                    if(isset($user_details['rank']) && !is_int($user_details['rank']) ) {
                        return array('error' => true, 'error_msg' => 'user_details[rank]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['rank']) ) {
                        $sql_fields['userdb_rank'] = intval($user_details['rank']);
                    }

                    if(isset($user_details['featuredlistinglimit']) && !is_int($user_details['featuredlistinglimit']) ) {
                        return array('error' => true, 'error_msg' => 'user_details[featuredlistinglimit]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['featuredlistinglimit']) ) {
                        $sql_fields['userdb_featuredlistinglimit'] = intval($user_details['featuredlistinglimit']);
                    }

                    if(isset($user_details['email_verified']) && !is_bool($user_details['email_verified'])){
                        return array('error' => true, 'error_msg' => 'user_details[email_verified]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['email_verified']) ) {
                        if($listing_details['email_verified']){
                            $sql_fields['userdb_email_verified'] = 'yes';
                        }
                        else{
                            $sql_fields['userdb_email_verified'] = 'no';
                        }
                    }

                    if(isset($user_details['send_notifications_to_floor']) && !is_bool($user_details['send_notifications_to_floor']) ) {
                        return array('error' => true, 'error_msg' => 'user_details[send_notifications_to_floor]: correct_parameter_not_passed');
                    }
                    else if(isset($user_details['send_notifications_to_floor']) ) {
                        if($listing_details['send_notifications_to_floor'] ===true){
                            $sql_fields['userdb_send_notifications_to_floor'] = '1';
                        }
                        else{
                            $sql_fields['userdb_send_notifications_to_floor'] = '0';
                        }
                    }

                }

            }

            //echo '<pre>User Field Array: '.print_r($user_details,true).'</pre>';

            //update the last modified timestamp
            $user_details['last_modified'] = time();

            $sql_a=array();
            foreach($sql_fields as $field => $value){
                if(!is_numeric($value)){
                    $value ="'$value'";
                }
                $sql_a[] = $field.' = '.$value;
            }

            $sql_a[]  = 'userdb_last_modified = '.$conn->DBTimeStamp($user_details['last_modified']);

            $sql = 'UPDATE '.$config['table_prefix'].'userdb
                    SET '.implode(',',$sql_a).'
                    WHERE userdb_id = '.$userdb_id;
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $error = $conn->ErrorMsg();
                $lapi->load_local_api('log__log_create_entry',array('log_type'=>'CRIT','log_api_command'=>'api->user->update','log_message'=>'DB Error: '.$error));
                return array('error' => true,'error_msg'=>'DB Error: '.$error."\r\n".'SQL: '.$sql);
            }

            //Deal with user fields
            if(isset($user_fields)){
                //Get List of user fields
                $sql = 'SELECT '.$resource.'formelements_field_name, '.$resource.'formelements_id,'.$resource.'formelements_field_type, '.$resource.'formelements_field_elements
                            FROM  ' . $config['table_prefix'] . $resource.'formelements';

                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $error = $conn->ErrorMsg();
                    $lapi->load_local_api('log__log_create_entry',array('log_type'=>'CRIT','log_api_command'=>'api->user->create','log_message'=>'DB Error: '.$error));
                    return array('error' => true,'error_msg'=>'DB Error: '.$error."\r\n".'SQL: '.$sql);
                }

                //Verify user fields passed via API exist in the applicable formelements table.
                $insert_user_fields=array();
                //print_r($user_fields);
                while(!$recordSet->EOF){
                    $name = $recordSet->fields[$resource.'formelements_field_name'];
                    if(array_key_exists($name,$user_fields)){
                        $id = $recordSet->fields[$resource.'formelements_id'];
                        $data_type = $recordSet->fields[$resource.'formelements_field_type'];
                        $data_elements = $recordSet->fields[$resource.'formelements_field_elements'];
                        switch ($data_type){
                            case 'number':
                                if($user_fields[$name] === ""){
                                    $insert_user_fields[$name]=null;
                                }else{
                                    if(is_numeric($user_fields[$name])){
                                        $insert_user_fields[$name] = $user_fields[$name];
                                    }else{
                                        $price = str_replace(',','',$user_fields[$name]);
                                        $insert_user_fields[$name] = intval($price);
                                    }
                                }
                                break;
                            case 'decimal':
                            case 'price':
                                if($user_fields[$name] === ""){
                                    $insert_user_fields[$name]=null;
                                }else{
                                    $price = str_replace(',','',$user_fields[$name]);
                                    $insert_user_fields[$name] = (float)$price;
                                }
                                break;
                            case 'date':
                                    if($user_fields[$name] === ""){
                                        $insert_user_fields[$name]=null;
                                    }else{
                                        $insert_user_fields[$name] = $this->convert_date($user_fields[$name],$or_date_format);
                                    }
                                    break;
                            case 'select':
                            case 'select-multiple':
                            case 'option':
                            case 'checkbox':
                                //This is a lookup field. Make sure values passed are allowed by the system.
                                //Get Array of allowed data elements
                                $data_elements_array=explode('||',$data_elements);
                                //echo '<pre> Data Elements: '.print_r($data_elements_array,true).'</pre>';
                                //Get array of passed data eleements
                                if(!is_array($user_fields[$name])){

                                    $t_value = $user_fields[$name];
                                    unset($user_fields[$name]);
                                    $user_fields[$name][]=$t_value;
                                }
                                //echo '<pre> Field Elements: '.print_r($user_fields[$name],true).'</pre>';
                                $good_elements=array();
                                foreach($user_fields[$name] as $fvalue){
                                    if(in_array($fvalue,$data_elements_array) && !in_array($fvalue,$good_elements)){
                                        $good_elements[]=$fvalue;
                                    }
                                }
                                //echo '<pre> Good Elements: '.print_r($good_elements,true).'</pre>';
                                $insert_user_fields[$name]=$good_elements;
                                break;
                            default:
                                $insert_user_fields[$name] = $user_fields[$name];
                                break;
                        }

                    }
                    $recordSet->Movenext();
                }
                //print_r($insert_user_fields);
                foreach ($insert_user_fields as $name => $value){
                    $sql_name = addslashes($name);
                    if(is_array($value)){
                        $sql_value = addslashes(implode('||',$value));
                    }else{
                        $sql_value = addslashes($value);
                    }
                    $sql = "UPDATE " . $config['table_prefix'] . "userdbelements
                            SET userdbelements_field_value = '$sql_value'
                            WHERE userdbelements_field_name = '$sql_name'
                            AND userdb_id = '$userdb_id'";
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $error = $conn->ErrorMsg();
                        $lapi->load_local_api('log__log_create_entry',array('log_type'=>'CRIT','log_api_command'=>'api->user->update','log_message'=>'DB Error: '.$error));
                        return array('error' => true,'error_msg'=>'DB Error: '.$error."\r\n".'SQL: '.$sql);
                    }
                }
            }
            //Deal with Hooks and social sites
            if(isset($user_details['active']) && $user_details['active'] && $oldstatus == 'no'){
                require_once($config['basepath'] . '/include/hooks.inc.php');
                $hooks = new hooks();
                $hooks->load('after_activated_user',$userdb_id);
            }

            // ta da! we're done...
            $admin_status = $login->verify_priv('Admin');
            if($admin_status == false || !isset($or_int_disable_log) || $or_int_disable_log == false){
                $lapi->load_local_api('log__log_create_entry',array('
                    log_type'=>'CRIT',
                    'log_api_command'=>'api->user->update',
                    'log_message'=>$lang['log_updated_user'] . ' ' . $userdb_id.' by '.$_SESSION['username']
                ));
            }
            //call the changed user change hook
            require_once($config['basepath'] . '/include/hooks.inc.php');
            $hooks = new hooks();
            $hooks->load('after_user_change',$userdb_id);
            return array('error' => false,'userdb_id' => $userdb_id);
        }

    */
    /**
     * This API Command deletes blog articles.
     * @param array $data expects an array containing the following array keys.
     *  <ul>
     *      <li>$data['blog_id'] - Number - blogmain_id# to delete</li>
     *  <ul>
     */
    public function delete($data)
    {
        global $conn, $config, $lang, $lapi, $misc;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $login_status = $login->verify_priv('can_access_blog_manager');
        // We may need to work out permissions here, the login_status
        // check above presently allows an Admin, Author, Contributor and Editor to do this.
        if ($login_status !== true) {
            return ['error' => true, 'error_msg' => 'Login Failure'];
        }

        extract($data, EXTR_SKIP || EXTR_REFS, '');
        //Check that required settings were passed
        if (!isset($blog_id)||!is_numeric($blog_id)) {
            return ['error' => true, 'error_msg' => $blog_id.' Invalid Blog ID'];
        }

        // Set Variable to hold errors
        $errors = '';

        if ($config['demo_mode'] == 1 && $_SESSION['admin_privs'] != 'yes') {
            return ['error' => true, 'error_msg' => $lang['demo_mode'] . ' - ' . 'Permission Denied'];
        }

        if ($login_status === true) {
            $blog_id = intval($blog_id);

            $sql = 'SELECT blogmain_id 
					FROM ' . $config['table_prefix'] . 'blogmain  
					WHERE blogmain_id = ' . $blog_id . '';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            if ($recordSet->RecordCount() == 0) {
                return ['error' => true,'error_msg'=>'Blog Article does not exist'];
            }

            $sql = 'DELETE FROM ' . $config['table_prefix'] . 'blogmain  
					WHERE blogmain_id = ' . $blog_id . '';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $sql = 'DELETE FROM ' . $config['table_prefix_no_lang'] . 'blogtag_relation  
					WHERE blogmain_id = ' . $blog_id . '';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $sql = 'DELETE FROM ' . $config['table_prefix_no_lang'] . 'blogcategory_relation  
					WHERE blogmain_id = ' . $blog_id . '';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }

            //delete the Blog Media Folder
            $dir = $config['basepath'].'/images/blog_uploads/'.$blog_id;
            if ($dir == $config['basepath']) {
                return json_encode(['error' => '1','blog_id' => $blog_id .'No folder present']);
            } else {
                try {
                    $files = array_diff(scandir($dir), ['.','..']);
                    foreach ($files as $file) {
                        (is_dir("$dir/$file")) ? recurseRmdir("$dir/$file") : unlink("$dir/$file");
                    }
                    rmdir($dir);
                } catch (Exception $e) {
                    $lapi->load_local_api('log__log_create_entry', ['log_type'=>'CRIT','log_api_command'=>'api->blog->delete','log_message'=>'missing folder: '.$target . ' '.$e ]);
                }
            }
        }

        // ta da! we're done...
        $lapi->load_local_api('log__log_create_entry', [
            'log_type'=>'CRIT',
            'log_api_command'=>'api->blog->delete',
            'log_message'=>'Deleted ' . $blog_id.' by '.$_SESSION['username'],
        ]);

        include_once $config['basepath'] . '/include/hooks.inc.php';
        $hooks = new hooks();
        $hooks->load('after_blog_delete', $delete_id);

        return ['error' => false,'blog_id' => $blog_id];
    }

    /*
    *
    *   PRIVATE FUNCTIONS
    */

    private function getmicrotime()
    {
        [$usec, $sec] = explode(' ', microtime());
        return ((float)$usec + (float)$sec);
    }
    private function get_publisher_status($user_id)
    {
        global $lapi;

        $result = $lapi->load_local_api('user__read', [
            'user_id'=>$user_id,
            'resource' => 'agent',
            'fields'=>['userdb_blog_user_type'],
        ]);

        $pub_status = $result['user']['userdb_blog_user_type'];

        if (is_numeric($pub_status)) {
            return $pub_status;
        } else {
            return 0;
        }
    }

    private function get_blog_categories_assignment_names($blog_id)
    {
        global $conn, $config, $misc;

        $sql = 'SELECT ' . $config['table_prefix'] . 'blogcategory.category_id, category_name 
				FROM ' . $config['table_prefix_no_lang'] . 'blogcategory_relation, ' . $config['table_prefix'] . 'blogcategory 
				WHERE ' . $config['table_prefix_no_lang'] . 'blogcategory_relation.category_id =  ' . $config['table_prefix'] . 'blogcategory.category_id 
				AND blogmain_id = '.intval($blog_id);
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
        $assigned=[];
        while (!$recordSet->EOF) {
            $assigned[$recordSet->fields['category_id']]= $recordSet->fields['category_name'];
            $recordSet->MoveNext();
        }
        return $assigned;
    }

    private function get_blog_tag_assignment($blog_id)
    {
        global $conn, $config, $misc;

        include_once $config['basepath'] . '/include/core.inc.php';
        $page = new page_user();
        //Get Min/Max population.
        $sql = 'SELECT count(tag_id) as population 
				FROM ' . $config['table_prefix_no_lang'] . 'blogtag_relation 
				GROUP BY tag_id 
				ORDER BY population';
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
        $min_count = $recordSet->fields['population'];
        $recordSet->MoveLast();
        $max_count = $recordSet->fields['population'];

        $sql = 'SELECT ' . $config['table_prefix_no_lang'] . 'blogtag_relation.tag_id, tag_name, tag_seoname, tag_description 
				FROM ' . $config['table_prefix_no_lang'] . 'blogtag_relation 
				LEFT JOIN ' . $config['table_prefix'] . 'blogtags 
				ON ' . $config['table_prefix_no_lang'] . 'blogtag_relation.tag_id = ' . $config['table_prefix'] . 'blogtags.tag_id 
				WHERE blogmain_id = '.intval($blog_id);
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }

        $assigned=[];
        while (!$recordSet->EOF) {
            $tag_id = $recordSet->fields['tag_id'];
            $tag_name = $recordSet->fields['tag_name'];
            $tag_seoname = $recordSet->fields['tag_seoname'];
            $tag_description = $recordSet->fields['tag_description'];
            //Get Tag LInk
            $tag_link=$page->magicURIGenerator('blog_tag', $tag_id, true);
            //Get Tag Population
            $assigned[$tag_id]= ['tag_name' => $tag_name,'tag_seoname' => $tag_seoname, 'tag_description' =>  $tag_description, 'tag_link'=>$tag_link];
            $recordSet->MoveNext();
        }
        return $assigned;
    }
}
