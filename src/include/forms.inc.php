<?php


class forms
{

    /**
     * ****************************************************************************\
     * renderFormElement($field_type, $field_name, $field_value,              *
     *                  $field_caption, $default_text, $required, $field_elements)*
     * \****************************************************************************
     */
    public function renderFormElement($field_type, $field_name, $field_value, $field_caption, $default_text, $required, $field_elements, $field_length = '', $tool_tip = '')
    {
        // handles the rendering of already filled in user forms
        global $lang, $config, $jscript;
        $field_value_raw=$field_value;
        $field_value_array=[];
        if (is_array($field_value)) {
            foreach ($field_value as $x => $v) {
                $field_value_array[$v]=htmlentities($v, ENT_COMPAT, $config['charset']);
            }
            $field_value=$field_value_array;
        } else {
            $field_value=htmlentities($field_value, ENT_COMPAT, $config['charset']);
        }
        $display = '';
        $validate=[];

        switch ($field_type) {
            case 'lat':
            case 'long':
            case 'text': // handles text input boxes
                $display .= '	<div class="form_div">
									<div class="field_caption">
										'.$field_caption.'';
                if ($required == 'Yes') {
                    $markup = '	<span class="form_required_field"></span>';
                    $validate[]='required';
                }
                $display .= '		</div>
				
									<div class="field_element">';
                $display .= '			<input type="text" name="'.$field_name.'" id="'.$field_name.'" value="'.$field_value.'" ';
                if ($field_length != '' && $field_length != 0) {
                    $display .= 'maxlength="'.$field_length.'" ';
                    //$validate[]='maxlength:'.$field_length;
                    //$validate[]='maxlength = "'.$field_length.'"';
                }
                if (!empty($validate)) {
                    //$display .= ' class="validate '.implode(',',$validate).'" ';
                    $display .= ' '.implode(' ', $validate).'';
                }
                $display .= ' />' .$markup;

                if ($tool_tip != '') {
                    $display .= '		<a href="#" class="tooltip"><span class="ui-icon ui-icon-help"></span>
											<span>'.$tool_tip.'</span>
										</a>';
                }
                $display .= '	</div>
							</div>
							<div class="clear"></div>'.BR;
                break;

            case 'date':
                switch ($config['date_format']) {
                    case 1:
                        $format='m/d/Y';
                        break;
                    case 2:
                        $format='Y/d/m';
                        break;
                    case 3:
                        $format='d/m/Y';
                        break;
                    default:
                        $format='m/d/Y';
                }

                if ($field_value != '') {
                    $field_value=date($format, intval($field_value));
                }

                $display .= '	<div class="form_div">	
									<div class="field_caption">'.$field_caption.'';

                if ($required == 'Yes') {
                    $markup = '	<span class="form_required_field"></span>';
                    $validate[]='required';
                    $validate[]='ordate';
                }

                $display .= '		</div>
									<div class="field_element">
										<input type="text" name="'.$field_name.'" id="'.$field_name.'" value="'.$field_value.'" ';
                if (!empty($validate)) {
                    //$display .= ' class="{validate:{'.implode(',',$validate).'}}" ';
                    $display .= ' class="'.implode(' ', $validate).'" ';
                }
                $display .= '/>'. $markup;

                $display .= '	(' . $config['date_format_long'] . ')';
                if ($tool_tip != '') {
                    $display .= '		<a href="#" class="tooltip"><span class="ui-icon ui-icon-help"></span><span>'.$tool_tip.'</span></a>';
                }
                $display .= '			<script>
											$( function() {
											    $( "#'.$field_name.'" ).datepicker({
											    	onClose: function() {
												        $( "#'.$field_name.'" ).validate();
												    }
											    });
											});
										  </script>
									</div>
								</div>
								<div class="clear"></div>'.BR;
                break;

            case 'textarea': // handles textarea input
                if ($_GET['action'] === 'edit_listing' && !empty($_GET['edit'])) {
                    /*
                    $inline_vars = 'id="'.$field_name.'"';
                    $init_inline ='<script>
                                        CKEDITOR.replace("'.$field_name.'", {
                                            toolbar : "Textarea",
                                            customConfig : CKEDITOR_CFGFILE,
                                            language: "'. $config['lang'].'"
                                        });
                                    </script>';
                    */
                }

                $display .= '	<div class="form_div">
									<div class="field_caption">'.$field_caption;

                //Had to set class="required" for this element. Just adding 'required'
                // to the <input> tag as usual does not seem to work.
                if ($required == 'Yes') {
                    $markup= 'class="required"';
                    $markup2 = '<span class="form_required_field"></span>';
                }
                if ($field_length != '' && $field_length != 0) {
                    $validate[]='maxlength = '.$field_length;
                }
                $display .= '		</div>
				
									<div class="field_element">
										<textarea name="'.$field_name.'" id="'.$field_name.'" '.$markup.'';
                if (!empty($validate)) {
                    //$display .= ' class="{validate:{'.implode(',',$validate).'}}" ';
                    $display .= ' '.implode(' ', $validate).'';
                }
                $display .= '>'.$field_value.'</textarea> '.$markup2;
                if ($tool_tip != '') {
                    $display .= '		<a href="#" class="tooltip"><span class="ui-icon ui-icon-help"></span>
											<span>'.$tool_tip.'</span>
										</a>';
                }
                $display .= '		</div>
								</div>
								<div class="clear"></div>'.BR;
                break;

            case 'select': // handles single item select boxes
                $display .= '	<div class="form_div">';
                $display .= '		<div class="field_caption">'.$field_caption;
                if ($required == 'Yes') {
                    $markup = '			<span class="form_required_field"></span>';
                    $validate[]='required';
                }
                $display .= '		</div>
									<div class="field_element">
										<select name="'.$field_name.'" id="'.$field_name.'" size="1"';
                if (!empty($validate)) {
                    //$display .= ' class="{validate:{'.implode(',',$validate).'}}" ';
                    $display .= ' '.implode(' ', $validate).'';
                }
                $display .= '>';
                if (!is_array($field_elements)) {
                    $field_elements = explode('||', $field_elements);
                }
                sort($field_elements);
                foreach ($field_elements as $list_item) {
                    $html_list_item = htmlentities($list_item, ENT_COMPAT, $config['charset']);
                    $display .= '			<option value="'.$html_list_item.'"';
                    if ($list_item == $field_value_raw || $list_item == "{lang_$field_value_raw}") {
                        $display .= ' selected="selected"';
                    }
                    $display .= '>'.$list_item.'
											</option>';
                }
                $display .= '			</select>'.$markup;
                if ($tool_tip != '') {
                    $display .= ' <a href="#" class="tooltip"><span class="ui-icon ui-icon-help"></span><span>'.$tool_tip.'</span></a>';
                }
                $display .= '		</div>';
                $display .= '	</div>';
                $display .= '	<div class="clear"></div>'.BR;
                break;

            case 'select-multiple': // handles multiple item select boxes
                $display .= '	<div class="form_div">
									<div class="field_caption">'.$field_caption;
                if ($required == 'Yes') {
                    $markup = '			<span class="form_required_field"></span>';
                    $validate[]='required';
                }
                $display .= '		</div>
									<div class="field_element">
										<select name="'.$field_name.'[]" id="'.$field_name.'" multiple="multiple"';
                if (!empty($validate)) {
                    //$display .= ' class="{validate:{'.implode(',',$validate).'}}" ';
                    $display .= ' '.implode(' ', $validate).'';
                }
                $display .= '>';
                if (!is_array($field_elements)) {
                    $field_elements = explode('||', $field_elements);
                }
                sort($field_elements);
                foreach ($field_elements as $feature_list_Value => $feature_list_item) {
                    $html_feature_list_item = htmlentities($feature_list_item, ENT_COMPAT, $config['charset']);
                    $display .= "			<option value=\"$html_feature_list_item\" ";
                    // now, compare it against the list of currently selected feature items
                    if (!is_array($field_value_raw)) {
                        $field_value_raw = explode('||', $field_value_raw);
                    }
                    sort($field_value_raw);
                    foreach ($field_value_raw as $field_value_list_item) {
                        if ($field_value_list_item == $feature_list_item || $field_value_list_item == "{lang_$feature_list_item}") {
                            $display .= 'selected="selected"';
                        }
                    }
                    $display .= " >$feature_list_item
											</option>";
                }
                $display .= '			</select>'.$markup;
                if ($tool_tip != '') {
                    $display .= '		<a href="#" class="tooltip"><span class="ui-icon ui-icon-help"></span>
											<span>'.$tool_tip.'</span>
										</a>';
                }
                $display .= '		</div>';
                $display .= '	</div>';
                $display .= '	<div class="clear"></div>'.BR;
                break;

            case 'divider': // handles dividers in forms
                $display .= '	<div class="form_div">
									<div class="field_caption"></div>';
                $display .= $field_caption;
                $display .= '		<div class="field_element"></div>
								</div>';
                $display .= '	<div class="clear"></div>'.BR;
                break;

            case 'price': // handles price input
                $display .= '	<div class="form_div">
									<div class="field_caption">
										'.$field_caption.' '.$config['money_sign'];
                if ($required == 'Yes') {
                    $markup = '	<span class="form_required_field"></span>';
                    $validate[]='required';
                }
                $validate[]='number';
                //$validate[] = 'pattern="\d+(\.\d{2})?"';
                $display .= '		</div>
									<div class="field_element">
										<input type="text" name="'.$field_name.'" id="'.$field_name.'" value="'.$field_value.'" ';
                if ($field_length != '' && $field_length != 0) {
                    $display .= 'maxlength="'.$field_length.'" ';
                    //$validate[]='maxlength = "'.$field_length.'"';
                }
                if (!empty($validate)) {
                    //$display .= ' class="{validate:{'.implode(',',$validate).'}}" ';
                    //$display .= ' '.implode(' ',$validate).'';
                    $display .= ' class="'.implode(' ', $validate).'" ';
                }
                $display .= '			/>'.$markup;
                if ($tool_tip != '') {
                    $display .= '		<a href="#" class="tooltip"><span class="ui-icon ui-icon-help"></span>
											<span>'.$tool_tip.'</span>
										</a>';
                }
                $display .= '		</div>';
                $display .= '	</div>';
                $display .= '	<div class="clear"></div>'.BR;
                break;

            case 'url': // handles url input fields
                $display .= '	<div class="form_div">
									<div class="field_caption">'.$field_caption.' ';
                if ($required == 'Yes') {
                    $markup = '	<span class="form_required_field"></span>';
                    $validate[]='required';
                }
                $validate[]='url';
                $display .= '			<br />
										('.$lang['dont_forget_http'].')
									</div>';
                $display .= '		<div class="field_element">
										<input type="text" name="'.$field_name.'" id="'.$field_name.'" value="'.$field_value.'" ';
                if ($field_length != '' && $field_length != 0) {
                    $display .= 'maxlength="'.$field_length.'" ';
                    //$validate[]='maxlength:'.$field_length;
                }
                if (!empty($validate)) {
                    //$display .= ' class="{validate:{'.implode(',',$validate).'}}" ';
                    $display .= ' class="'.implode(' ', $validate).'" ';
                }
                $display .= '			/>'.$markup;
                if ($tool_tip != '') {
                    $display .= '		<a href="#" class="tooltip"><span class="ui-icon ui-icon-help"></span><span>'.$tool_tip.'</span></a>';
                }
                $display .= '		</div>
								</div>
								<div class="clear"></div>'.BR;
                break;

            case 'email': // handles email input
                $display .= '	<div class="form_div">
									<div class="field_caption">'.$field_caption.' ';
                if ($required == 'Yes') {
                    $markup = '	<span class="form_required_field"></span>';
                    $validate[]='required';
                }
                $validate[]='email';
                $display .= '			<br />
										('.$lang['email_example'].')
									</div>';
                $display .= '		<div class="field_element">
										<input type="text" name="'.$field_name.'" id="'.$field_name.'" value="'.$field_value.'" ';
                if ($field_length != '' && $field_length != 0) {
                    $display .= 'maxlength="'.$field_length.'" ';
                    //$validate[]='maxlength:'.$field_length;
                }
                if (!empty($validate)) {
                    //$display .= ' class="{validate:{'.implode(',',$validate).'}}" ';
                    $display .= ' class="'.implode(' ', $validate).'" ';
                }
                $display .= '			/>'.$markup;
                if ($tool_tip != '') {
                    $display .= '		<a href="#" class="tooltip"><span class="ui-icon ui-icon-help"></span>
											<span>'.$tool_tip.'</span>
										</a>';
                }
                $display .= '		</div>
								</div>
								<div class="clear"></div>'.BR;
                break;

            case 'checkbox': // handles checkboxes
                $display .= '	<div class="form_div">
									<div class="field_caption">'.$field_caption.' ';
                if ($required == 'Yes') {
                    $display .= '			<span class="form_required_field"></span>';
                    $validate[]='required';
                }
                if ($tool_tip != '') {
                    $display .= '		<a href="#" class="tooltip">
											<span class="ui-icon ui-icon-help"></span>
											<span>'.$tool_tip.'</span>
										</a>';
                }
                $display .= '		</div>
									<div class="field_element">';
                $count = 1;
                $numcols = 2;
                if (!is_array($field_elements)) {
                    $field_elements = explode('||', $field_elements);
                }
                sort($field_elements);
                if ($required != 'Yes') {
                    $display .= '		<input type="hidden" value="" name="'.$field_name.'[]" />';
                }
                $item_count = 0;
                foreach ($field_elements as $feature_list_Value => $feature_list_item) {
                    $html_feature_list_item = htmlentities($feature_list_item, ENT_COMPAT, $config['charset']);
                    // need to figure out how to place an id="" on these
                    $display .= '		<div class="field_element_column">
											<input type="checkbox" value="'.$html_feature_list_item.'" name="'.$field_name.'[]"';
                    // now, compare it against the list of currently selected feature items
                    if (!is_array($field_value_raw)) {
                        $field_value_raw = explode('||', $field_value_raw);
                    }
                    sort($field_value_raw);
                    foreach ($field_value_raw as $field_value_list_item) {
                        if ($field_value_list_item == $feature_list_item || $field_value_list_item == "{lang_$feature_list_item}") {
                            $display .= 'checked="checked" ';
                        } // end if
                    } // end while
                    if (!empty($validate) && $item_count==0) {
                        //$display .= ' class="{validate:{'.implode(',',$validate).'}}" ';
                        $display .= ' class="'.implode(' ', $validate).'" ';
                    }
                    $display .= '			/>'.$feature_list_item.'
										</div>';
                    if ($count % $numcols == 0) {
                        $display .= '	<div style="clear:left;"></div>';
                        ;
                        $count=1;
                    } else {
                        $count++;
                    }
                    $item_count++;
                } // end while
                $display .= '			<div style="clear:left;"></div>
									</div>
								</div>';
                $display .= '	<div class="clear"></div>'.BR;
                break;

            case 'option': // handles options
                $display .= '	<div class="form_div">
									<div class="field_caption">'.$field_caption.' ';
                if ($required == 'Yes') {
                    $display .= '		<span class="form_required_field"></span>';
                    $validate[]='required';
                }
                if ($tool_tip != '') {
                    $display .= '		<a href="#" class="tooltip"><span class="ui-icon ui-icon-help"></span>
											<span>'.$tool_tip.'</span>
										</a>';
                }
                $count = 1;
                $numcols = 2;
                $display .= '		</div>
									<div class="field_element">';
                if (!is_array($field_elements)) {
                    $field_elements = explode('||', $field_elements);
                }
                sort($field_elements);
                foreach ($field_elements as $feature_list_Value => $feature_list_item) {
                    $html_feature_list_item = htmlentities($feature_list_item, ENT_COMPAT, $config['charset']);

                    $display .= '		<div class="field_element_column">
											<input type="radio" value="'.$html_feature_list_item.'" id="'.$field_name.'" name="'.$field_name.'" ';
                    // now, compare it against the list of currently selected feature items
                    if ($feature_list_item == $field_value_raw || $feature_list_item == "{lang_$field_value_raw}") {
                        $display .= 'checked="checked" ';
                    } // end if
                    if (!empty($validate)) {
                        //$display .= ' class="{validate:{'.implode(',',$validate).'}}" ';
                        $display .= ' class="'.implode(' ', $validate).'" ';
                    }
                    $display .= "			/>$feature_list_item 
										</div>";
                    if ($count % $numcols == 0) {
                        $display .= '	<div style="clear:left;"></div>';
                    }
                    $count++;
                } // end while
                $display .= '			<div style="clear:left;"></div>
									</div>
								</div>';
                $display .= '	<div class="clear"></div>'.BR;
                break;

            case 'number':
            case 'decimal':
                $display .= '	<div class="form_div">
									<div class="field_caption">'.$field_caption;
                if ($required == 'Yes') {
                    $markup = '	<span class="form_required_field"></span>';
                    $validate[]='required';
                }
                $validate[]='number';
                $display .= '		</div>';
                $display .= '		<div class="field_element">
										<input type="text" name="'.$field_name.'" id="'.$field_name.'" value="'.$field_value.'" ';
                if ($field_length != '' && $field_length != 0) {
                    $display .= 'maxlength="'.$field_length.'" ';
                    //$validate[]='maxlength:'.$field_length;
                }
                if (!empty($validate)) {
                    // $display .= ' class="{validate:{'.implode(',',$validate).'}}" ';
                    $display .= ' class="'.implode(' ', $validate).'" ';
                }
                $display .= '			/>'.$markup;
                if ($tool_tip != '') {
                    $display .= '		<a href="#" class="tooltip"><span class="ui-icon ui-icon-help"></span>
											<span>'.$tool_tip.'</span>
										</a>';
                }
                $display .= '		</div>
								</div>
								<div class="clear"></div>'.BR;
                break;

            case 'submit': // handles submit buttons
                $display .= '<div class=\"field_element\">
								<input type="submit" value="'.$field_value.'" />
							</div>'.BR;
                break;

            default: // the catch all... mostly for errors and whatnot
                $display .= '<div>no handler yet</div>'.BR;
        } // end switch statement
        return $display;
    } // end renderExistingUserFormElement function

    public function validateForm($db_to_validate, $pclass = [])
    {
        // Validates the info being put into the system
        global $conn, $lang, $config, $misc;

        $pass_the_form = 'Yes';
        // this stuff is input that's already been dealt with
        // check to if the form should be passed
        $sql = 'SELECT ' . $db_to_validate . '_required, ' . $db_to_validate . '_field_type, ' . $db_to_validate . '_field_name 
				FROM ' . $config['table_prefix'] . $db_to_validate;
        if (count($pclass) > 0 && $db_to_validate == 'listingsformelements') {
            $sql .= ' WHERE listingsformelements_id IN (
							SELECT listingsformelements_id 
							FROM '.$config['table_prefix_no_lang'].'classformelements 
							WHERE class_id IN ('.implode(',', $pclass).')
						)';
        }
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
        while (!$recordSet->EOF) {
            $required = $recordSet->fields[$db_to_validate . '_required'];
            $field_type = $recordSet->fields[$db_to_validate . '_field_type'];
            $field_name = $recordSet->fields[$db_to_validate . '_field_name'];
            if ($required == 'Yes') {
                if (!isset($_POST[$field_name]) || (is_array($_POST[$field_name]) && count($_POST[$field_name])==0) || (!is_array($_POST[$field_name]) && trim($_POST[$field_name]) =='')) {
                    $pass_the_form = 'No';
                    $error[$field_name] = 'REQUIRED';
                }
            } // end if
            if ($field_type == 'number' && isset($_POST[$field_name]) && !is_numeric($_POST[$field_name]) && $_POST[$field_name] != '') {
                $pass_the_form = 'No';
                $error[$field_name] = 'TYPE';
            }
            $recordSet->MoveNext();
        }
        if ($pass_the_form == 'Yes') {
            return $pass_the_form;
        } else {
            return $error;
        }
    } // end function validateForm
}
