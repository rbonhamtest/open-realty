<?php

class lead_manager
{
    public function show_add_lead()
    {
        global $config,$jscript;

        include_once $config['basepath'] . '/include/core.inc.php';
        $page = new page_admin();
        include_once $config['basepath'] . '/include/lead_functions.inc.php';
        $lead_functions = new lead_functions();
        $jscript.='{load_css_add_lead}';
        $page->load_page($config['admin_template_path'].'/add_lead.html');
        $lead_fields = $lead_functions->get_feedback_formelements();
        $page->replace_tag('feedback_formelements', $lead_fields);
        return $page->return_page();
    }

    /*
     *  feedbackview() View and edit leads
     */
    public function feedbackview($all_lead_view = false)
    {
        global $config, $lang;

        include_once $config['basepath'].'/include/login.inc.php';
        $login = new login();

        if (isset($_GET['feedback_id'])) {
            global $conn, $jscript, $misc;

            $feedback_id = intval($_GET['feedback_id']);
            if (isset($_GET['active']) && $_GET['active'] ==1) {
                $active = 1;
            } else {
                $active = 0;
            }
            $sql = 'SELECT feedbackdb_creation_date, feedbackdb_notes, listingdb_id, feedbackdb_last_modified, userdb_id, feedbackdb_member_userdb_id
					FROM ' . $config['table_prefix'] . "feedbackdb
					WHERE feedbackdb_id = '".$feedback_id."'";
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }

            include_once $config['basepath'].'/include/lead_functions.inc.php';
            $lead_functions = new lead_functions();
            include_once $config['basepath'] . '/include/listing.inc.php';
            $listing = new listing_pages();
            include_once $config['basepath'].'/include/user.inc.php';
            $user = new user();

            $feedback_last_modified = $recordSet->UserTimeStamp($recordSet->fields['feedbackdb_last_modified'], 'd m, Y g:ia');
            $feedbackdb_creation_date = $recordSet->UserTimeStamp($recordSet->fields['feedbackdb_creation_date'], 'd m, Y g:ia');
            $creation_day = $recordSet->UserTimeStamp($recordSet->fields['feedbackdb_creation_date'], 'd');
            $creation_month = $recordSet->UserTimeStamp($recordSet->fields['feedbackdb_creation_date'], 'm');
            $creation_year = $recordSet->UserTimeStamp($recordSet->fields['feedbackdb_creation_date'], 'Y');
            $creation_hour = $recordSet->UserTimeStamp($recordSet->fields['feedbackdb_creation_date'], 'G');
            $creation_minute = $recordSet->UserTimeStamp($recordSet->fields['feedbackdb_creation_date'], 'i');
            //Format Y-m-d per ISO8601 for fullcalendar
            $calendar_creation_date = $recordSet->UserTimeStamp($recordSet->fields['feedbackdb_creation_date'], 'Y-m-d');
            $user_id = $recordSet->fields['userdb_id'];
            $listingsdb_id = $recordSet->fields['listingdb_id'];

            $listing_title = $listing->get_listing_single_value('listingsdb_title', $listingsdb_id);

            $url_listing_title = rawurlencode($listing_title);
            $member_id = $recordSet->fields['feedbackdb_member_userdb_id'];
            $user_email = $user->get_user_single_item('userdb_emailaddress', $member_id);
            $notes = htmlentities($recordSet->fields['feedbackdb_notes'], ENT_COMPAT, $config['charset']);
            //Make sure we have perission.
            $permission=false;
            if ($_SESSION['userID'] !== $user_id) {
                if ($all_lead_view==true) {
                    $permission = $login->verify_priv('edit_all_leads');
                }
            } else {
                $permission=true;
            }
            if (!$permission) {
                return $lang['listing_editor_permission_denied'];
            }

            $jscript .= '<script type="text/javascript" src="'.$config['baseurl'].'/node_modules/moment/min/moment.min.js"></script>';
            $jscript .= '<script type="text/javascript" src="'.$config['baseurl'].'/node_modules/fullcalendar/dist/fullcalendar.js"></script>';

            $jscript .= '<script type="text/javascript">
			$(document).ready(function() {

				$( "#tabs" ).uitabs();

				var orig_user_arr = $("select[name=user]");
				var orig_user_id = orig_user_arr.val();

				var d = '.$creation_day.';
				var y = '.$creation_year.';
				var m = '.($creation_month-1).';

				var leaddate = $.fullCalendar.moment("'.$calendar_creation_date.'");
				
				// var calendar2El = document.getElementById("calendar2");
				// let calendar2 = new FullCalendar.Calendar(calendar2El, {
				// 	initialView: "dayGridMonth",
				// 	headerToolbar: {
				// 	  left: "",
				// 	  center: "",
				// 	  right: ""
				// 	}
				// });
				// calendar2.render();
				$("#calendar2").fullCalendar({
					defaultDate: leaddate,
					editable: false,
					defaultView: "basicWeek",
					header: {
						left: "",
						center: "",
						right: ""
					},

					columnFormat: {
						day: "dddd, MMM d, yyyy"
					},

					events: [{
						id: 1,
						title: "Contact",
						start: new Date(y, m, d, '.$creation_hour.', '.$creation_minute.'),
						url: "mailto:'.$user_email.'?subject=RE:'.$url_listing_title.'",
						allDay: false
					}]
				});

				$("#calendar").fullCalendar({
					defaultDate: leaddate,
					editable: false,
					weekMode: "liquid",
					header: {
						left: "title",
						center: "",
						right: "month,basicDay"
					},

					events: [{
						id: 1,
						title: "Contact",
						start: new Date(y, m, d, '.$creation_hour.', '.$creation_minute.'),
						url: "mailto:'.$user_email.'?subject=RE:'.$url_listing_title.'",
						allDay: false
					}]
				});

				$("select#user").change( function()
            	{
  	             	confirmAgentChange()
    	        });

        	    $("#status").change( function()
            	{
	               	confirmStatus()
    	        });

        	    $("#save_note").click( function()
	            {
  	            	save_note();
  	         	});
				$("#priority").change( function(){
					var loadUrl = "ajax.php?action=ajax_change_lead_priority";
					var staff_notes = $("textarea[name=staff_notes]");

					$.post(loadUrl, {priority: $("#priority").val(), feedback_id: '.$feedback_id.'},
						function(data) {
							if(data.error == "1")
							{
								status_error(data.error_msg);
							}
							else
							{
								status_msg("Priority '.$lang['generic_saved_text'].' ");
							}
						},"json");

					return;
				});
				function save_note() {
					var origtext = $("#legend_text").text();

					$("#legend_text").text("Saving...");
					$("#legend_text").css("color", "blue");

					var loadUrl = "ajax.php?action=ajax_change_lead_notes";
					var staff_notes = $("textarea[name=staff_notes]");

					$.post(loadUrl, {notes: staff_notes.val(), feedback_id: '.$feedback_id.'},
						function(data) {
							if(data.error == "1") {
								status_error(data.error_msg);
							}
							else {
								status_msg("Notes '.$lang['generic_saved_text'].' ");
							}
						},"json");

					return;
				}

	           function confirmAgentChange() {
					$("#assigned_to").text("Changing... ");
					$("#assigned_to").css("color", "blue");

					var loadUrl = "ajax.php?action=ajax_change_lead_agent";
					var user_id = $("select[name=user]");

					var agree = confirm("Are you sure you want to re-assign this Lead?");

					if (agree) {
						$.post(loadUrl, {user: user_id.val(), feedback_id: '.$feedback_id.'},
							function(data) {
								if(data.error == "1") {
									status_error(data.error_msg);
									$("#assigned_to").text("Assigned To:");
									$("#assigned_to").css("color", "black");
								}
								else {
									status_msg("'.$lang['lead_reassigned'].'");
									$("#assigned_to").text("Assigned To:");
									$("#assigned_to").css("color", "black");
								}
							},"json");
						orig_user_id = user_id.val();
						return;
					}
					else
					{
						$("#assigned_to").text("Assigned To:");
						$("#assigned_to").css("color", "black");
						$("#user").val(orig_user_id);
						return;
					}
				}
				function confirmStatus() {
					var loadUrl = "ajax.php?action=ajax_change_lead_status";
					var current_status = $("checkbox[name=active]");

					if ($("#status").is(":checked"))
					{
						$("#active_status").text("Changing...");

						var agree = confirm("Change the status to ACTIVE?");
						if (agree)
						{
							$.post(loadUrl, {status: "1", feedback_id: '.$feedback_id.'},
							function(data) {
								if(data.error == "1")
								{
									status_error(data.error_msg);
								}
								else
								{
									status_msg("'.$lang['leadmanager_status_active'].'");
									$("#active_status").text("'.$lang['leadmanager_status_active'].'");
									$("#active_status").css("color", "blue");

									$("a[href*=\'active=\']").each(function () {
   										var href = $(this).attr("href");
    									if ($(this).attr("href").match(/=[0-9]+&active/))
    									{
    										$(this).attr("href", href.replace(/active=0/, "active=1"));
    									}
									});
								}},"json");
							orig_status = current_status.val();
							return false;
						}
						else
						{
							$("#status").attr("checked", false);
							$("#active_status").text("'.$lang['leadmanager_status_inactive'].'");
							$("#active_status").css("color", "red");
						}
					}
					else
					{
						$("#active_status").text("Changing...");
						$("#active_status").css("color", "red");

						var agree = confirm("Change the status to INACTIVE?");
						if (agree)
						{
							$.post(loadUrl, {change_status: "change_status", status: "0", feedback_id: '.$feedback_id.'},
							function(data) {
							if(data.error == "1")
								{
									status_error(data.error_msg);
								}
								else
								{
									status_msg("'.$lang['leadmanager_status_inactive'].'");
									$("#active_status").text("'.$lang['leadmanager_status_inactive'].'");
									$("#active_status").css("color", "red");


									$("a[href*=\'&active=\']").each(function () {
	   									var href = $(this).attr("href");
	    								if ($(this).attr("href").match(/=[0-9]+&active/))
	    								{
	    									$(this).attr("href", href.replace(/active=1/, "active=0"));
	    								}
									});
								}
							},"json");
							return false;
						}
						else
						{
							$("#status").attr("checked", true);
							$("#active_status").text("'.$lang['leadmanager_status_active'].'");
							$("#active_status").css("color", "blue");
						}
					}
				}

			});
			</script>';

            $jscript .= '{load_css_view_leads}';
            $jscript .= '<link href="'.$config['baseurl'].'/node_modules/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" type="text/css" />';

            include_once $config['basepath'] . '/include/core.inc.php';
            $page = new page_admin();
            $page->load_page($config['admin_template_path'].'/view_leads.html');

            $page->replace_tag('leadmanager_pg_First', $this->leadmanager_pagination('First', $all_lead_view));
            $page->replace_tag('leadmanager_pg_Previous', $this->leadmanager_pagination('Previous', $all_lead_view));
            $page->replace_tag('leadmanager_pg_Last', $this->leadmanager_pagination('Last', $all_lead_view));
            $page->replace_tag('leadmanager_pg_Next', $this->leadmanager_pagination('Next', $all_lead_view));

            $page->replace_tag('leadmanager_status', $this->leadmanager_status());

            $page->replace_tag('leadmanager_priority', $this->leadmanager_priority());

            $field = $lead_functions->renderTemplateArea('headline', $feedback_id);
            $page->page = str_replace('{headline}', $field, $page->page);

            $field = $lead_functions->renderTemplateArea('top_left', $feedback_id);
            $page->page = str_replace('{top_left}', $field, $page->page);
            $field = $lead_functions->renderTemplateArea('top_right', $feedback_id);
            $page->page = str_replace('{top_right}', $field, $page->page);

            $field = $lead_functions->renderTemplateArea('center', $feedback_id);
            $page->page = str_replace('{center}', $field, $page->page);

            $field = $lead_functions->renderTemplateArea('bottom_left', $feedback_id);
            $page->page = str_replace('{bottom_left}', $field, $page->page);
            $field = $lead_functions->renderTemplateArea('bottom_right', $feedback_id);
            $page->page = str_replace('{bottom_right}', $field, $page->page);
            //End of Template Areas

            $page->page = str_replace('{notes}', $notes, $page->page);
            $feedback_last_modified = $recordSet->UserTimeStamp($recordSet->fields['feedbackdb_last_modified'], 'm/d/Y g:ia');
            $feedbackdb_creation_date = $recordSet->UserTimeStamp($recordSet->fields['feedbackdb_creation_date'], 'm/d/Y g:ia');
            $modifiedby = $lead_functions->getFeedbackModData($feedback_id, 'modifiedby');
            $page->replace_tag('modifiedby', $modifiedby);

            $leadmanager_agents = $lead_functions->contactAgentEmailDropdown($user_id);
            $page->replace_tag('leadmanager_agent_select', $leadmanager_agents);

            $page->replace_tag('lastmodified', $feedback_last_modified);
            $page->replace_tag('creationdate', $feedbackdb_creation_date);
            $page->replace_tag('feedback_id', $feedback_id);
            $page->replace_user_field_tags($member_id);
            $edit_listing_link = $config['baseurl'].'/admin/index.php?action=edit_listing&amp;edit='.$listingsdb_id;
            $page->replace_tag('edit_listing_link', $edit_listing_link);
            $page->replace_listing_field_tags($listingsdb_id);
            $page->replace_permission_tags();
            $page->replace_urls();
            //$page->replace_meta_template_tags();
            $page->auto_replace_tags();
            //$page->replace_tags(array('load_js', 'load_ORjs', 'load_js_last'));
            $page->replace_lang_template_tags();
            $page->replace_css_template_tags();

            $output = $page->return_page();
        } else {
            $output = '<a href="index.php">Not a valid request</a>';
        }
        return $output;
    }

    public function ajax_leadmanager_datatable($show_all_leads = false)
    {
        global $config, $conn,$lang, $misc;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();

        $aColumns = [ 'feedbackdb_id', 'feedbackdb_member_userdb_id', 'feedbackdb_creation_date', 'feedbackdb_priority', 'feedbackdb_status', 'userdb_id'];
        $where = ' WHERE userdb_id = '.intval($_SESSION['userID']).' ';
        if ($show_all_leads==true) {
            $login_status = $login->verify_priv('edit_all_leads');
            if ($login_status===true) {
                $where='';
            } else {
                return $lang['permission_denied'];
            }
        }

        //Do Search to get total record count, no need pass in soring information
        $limit = 0;
        $offset = 0;
        $sql = 'SELECT count(*) as mycount 
				FROM ' . $config['table_prefix'] . "feedbackdb $where";
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
        $iTotal = $recordSet->fields['mycount'];
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $limit = intval($_GET['iDisplayLength']);
            $offset = intval($_GET['iDisplayStart']);
            //$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".mysql_real_escape_string( $_GET['iDisplayLength'] );
        }
        $sortby=[];
        $sorttype=[];
        //Deal with sorting
        if (isset($_GET['iSortCol_0'])) {
            for ($i=0; $i<intval($_GET['iSortingCols']); $i++) {
                if ($_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == 'true') {
                    $sortby[$i]=$aColumns[ intval($_GET['iSortCol_'.$i]) ];
                    $sorttype[$i]= strtoupper($_GET['sSortDir_'.$i]);
                }
            }
        }
        $sWhere='';
        if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
            $sql_sSearch = $conn->qstr('%'.$_GET['sSearch'].'%');

            if ($where=='') {
                $sWhere = 'WHERE (';
            } else {
                $sWhere = 'AND (';
            }
            for ($i=0; $i<count($aColumns); $i++) {
                if ($aColumns[$i] == 'userdb_id' || $aColumns[$i] == 'feedbackdb_member_userdb_id') {
                    $sWhere .= '`'.$aColumns[$i].'` IN (SELECT userdb_id FROM '.$config['table_prefix']."userdb 
																		WHERE CONCAT(userdb_user_last_name,', ',userdb_user_first_name) 
																		LIKE ".$sql_sSearch.') OR ';
                } else {
                    $sWhere .= '`'.$aColumns[$i].'` LIKE '.$sql_sSearch.' OR ';
                }
            }
            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }
        //Deal with column filters
        for ($i=0; $i<count($aColumns); $i++) {
            if ($_GET['bSearchable_'.$i] == 'true' && $_GET['sSearch_'.$i] != '') {
                if ($aColumns[$i] == 'feedbackdb_status') {
                    $ARGS[$aColumns[$i]]=$_GET['sSearch_'.$i];
                } else {
                    $ARGS[$aColumns[$i]]=$_GET['sSearch_'.$i];
                }
            }
        }
        if (!empty($ARGS)) {
            foreach ($ARGS as $f => $k) {
                if ($sWhere == '') {
                    if ($where=='') {
                        $where .= ' WHERE ';
                    } else {
                        $where .= ' AND ';
                    }
                } else {
                    $where .= 'AND ';
                }

                if ($f == 'feedbackdb_status') {
                    $where .= addslashes($f).' = '.intval($k);
                } elseif ($f == 'feedbackdb_member_userdb_id') {
                    $where .= '`'.addslashes($f).'` IN (SELECT userdb_id 
													FROM '.$config['table_prefix']."userdb 
													WHERE CONCAT(userdb_user_last_name,', ',userdb_user_first_name) 
													LIKE '%".addslashes($k)."%') ";
                } else {
                    $where .= addslashes($f).' LIKE \'%'.addslashes($k).'%\'';
                }
            }
        }
        $sort = '';
        if (count($sortby)>0) {
            $sort .= ' ORDER BY ';
            foreach ($sortby as $x => $f) {
                if ($sort != ' ORDER BY ') {
                    $sort .= ', ';
                }
                $sort .= addslashes($f).' '.addslashes($sorttype[$x]);
            }
        }
        $limitstr = '';
        if ($limit>0) {
            $limitstr .= 'LIMIT '.$offset.', '.$limit;
        }
        $sql = 'SELECT count(*) as filteredcount FROM ' . $config['table_prefix'] . "feedbackdb $sWhere $where";
        $filtersql = $sql;
        //echo $sql;
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
        $iFilteredTotal = $recordSet->fields['filteredcount'];
        $sql = 'SELECT * FROM ' . $config['table_prefix'] . "feedbackdb $sWhere $where $sort $limitstr ";
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }

        $output = [
                'sEcho' => intval($_GET['sEcho']),
                'iTotalRecords' => $iTotal,
                'filterSQL'=>$filtersql,
                'iTotalDisplayRecords' => $iFilteredTotal,
                'aaData' => [],
        ];
        $status_array[0]=$lang['leadmanager_status_inactive'];
        $status_array[1]=$lang['leadmanager_status_active'];
        if ($show_all_leads==true) {
            $lead_edit_action = 'leadmanager_feedback_edit';
            $lead_view_action = 'leadmanager_viewfeedback';
        } else {
            $lead_edit_action = 'leadmanager_my_feedback_edit';
            $lead_view_action = 'leadmanager_my_viewfeedback';
        }
        while (!$recordSet->EOF) {
            $row=[];
            //feedbackdb_id, feedbackdb_member_userdb_id, userdb_id, feedbackdb_creation_date, feedbackdb_priority, feedbackdb_status, ,
            $lead_id = $recordSet->fields['feedbackdb_id'];
            $row[]=$recordSet->fields['feedbackdb_id'];
            $sqlMember = 'SELECT userdb_user_first_name, userdb_user_last_name
			FROM '.$config['table_prefix'].'userdb
			WHERE userdb_id ='.$recordSet->fields['feedbackdb_member_userdb_id'];

            $recordSet2 = $conn->execute($sqlMember);
            if ($recordSet2 === false) {
                $misc->log_error($sqlMember);
            }
            $first_name = $recordSet2->fields['userdb_user_first_name'];
            $last_name = $recordSet2->fields['userdb_user_last_name'];
            $row[]=$last_name.', '.$first_name;

            $row[]=$recordSet->UserTimeStamp($recordSet->fields['feedbackdb_creation_date'], 'D M j G:i:s T Y');
            $row[]=$recordSet->fields['feedbackdb_priority'];
            $row[]=$status_array[$recordSet->fields['feedbackdb_status']];
            $sqlMember = 'SELECT userdb_user_first_name, userdb_user_last_name
			FROM '.$config['table_prefix'].'userdb
			WHERE userdb_id ='.$recordSet->fields['userdb_id'];

            $recordSet2 = $conn->execute($sqlMember);
            if ($recordSet2 === false) {
                $misc->log_error($sqlMember);
            }
            $first_name = $recordSet2->fields['userdb_user_first_name'];
            $last_name = $recordSet2->fields['userdb_user_last_name'];
            $row[]=$last_name.', '.$first_name;
            $row[]='<div id="actionlinks"><a onclick="gotoLead(\''.$config['baseurl'].'/admin/index.php?action='.$lead_edit_action.'&feedback_id='.$lead_id.'\');" href="#" title="'.$lang['edit'].'">

					<img class="addon_manager_action_icon" src="'.$config['admin_template_url'].'/images/generic_icon_edit.png" title="'.$lang['edit'].'" alt="'.$lang['edit'].'" />
				</a>

				<a onclick="gotoLead(\''.$config['baseurl'].'/admin/index.php?action='.$lead_view_action.'&feedback_id='.$lead_id.'\');" href="#" title="'.$lang['view'].'">
					<img class="addon_manager_action_icon" src="'.$config['admin_template_url'].'/images/generic_icon_go.png" title="'.$lang['view'].'" alt="'.$lang['view'].'" />
				</a>

				<a href="#"	onclick="deleteLead('.$lead_id.');" title="'.$lang['delete'].'">
					<img class="addon_manager_action_icon" src="'.$config['admin_template_url'].'/images/generic_icon_delete.png" title="'.$lang['delete'].'"	alt="'.$lang['delete'].'" />
				</a> 
				</div>';
            $output['aaData'][] = $row;
            $recordSet->MoveNext();
        }
        return json_encode($output);
    }

    /*
     * show_leads() Displays a list of available leads
     */
    public function show_leads($show_all_leads = false)
    {
        global $config,$conn,$lang, $jscript, $misc;

        include_once $config['basepath'] . '/include/core.inc.php';
        $page = new page_admin();

        include_once $config['basepath'].'/include/user.inc.php';
        $user = new user();
        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $display='';

        if ($show_all_leads) {
            $assigned_agent_visible='{ "sWidth": "120px" }';
        } else {
            $assigned_agent_visible='{ "sWidth": "120px", "bVisible": false }';
        }
        //Load JScript
        $jscript .= '
		{load_css_datatable}
		{load_css_view_leads}
		<script type="text/javascript" src="'.$config['baseurl'].'/node_modules/datatables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript">
		var leadFilterStatus = 1;
		$(document).ready(function() {

			//Load Datatable
			var oTable;
			var asInitVals = new Array();
			oTable = $("#showleads").dataTable( {
				"sPaginationType": "full_numbers",
				"bAutoWidth": false,
				"sAjaxSource": "ajax.php?action=ajax_leadmanager_datatable&show_all_leads='.intval($show_all_leads).'",
				"bProcessing": true,
				"bServerSide": true,
				"iDisplayLength": 25,
				"sDom": \'<"top" <il><"clear"><"space_10"> fp <"clear">> rt <"bottom"ip <"clear">>\',
				/* "bFilter":false, */
				"aoColumns": [
					{ "bVisible": true },
					{ "sWidth": "120px" },
					{ "sType": "date", "iDataSort": 2, "sWidth": "180px" },
					{ "bVisible": false },
					{ "sWidth": "80px" },
					'.$assigned_agent_visible.',
					null
				],
				"aaSorting": [[0,"desc"]]
			});
			
			oTable.fnFilter( "1",4,true );

			//Allow Searching Of Each Column
			$("tfoot input").keyup( function () {
				/* Filter on the column (the index) of this element */
				oTable.fnFilter( this.value, $("tfoot input").index(this) );
			} );

			/*
			 * Support functions to provide a little bit of "user friendlyness" to the textboxes in
			 * the footer
			 */
			$("tfoot input").each( function (i) {
				asInitVals[i] = this.value;
			} );

			$("tfoot input").focus( function () {
				if ( this.className == "search_init" )
				{
					this.className = "";
					this.value = "";
				}
			} );

			$("tfoot input").blur( function (i) {
				if ( this.value == "" )
				{
					this.className = "search_init";
					this.value = asInitVals[$("tfoot input").index(this)];
				}
			} );

			$("#lead_filter_inactive").click(function(){
				$("#lead_filter_inactive").css({"font-weight": "bold"});
				$("#lead_filter_active").css({"font-weight": "normal"});
				oTable.fnFilter( "0",4,true );
				leadFilterStatus=0;
				return false;
			});
			$("#lead_filter_active").click(function(){
				$("#lead_filter_active").css({"font-weight": "bold"});
				$("#lead_filter_inactive").css({"font-weight": "normal"});
				oTable.fnFilter( "1",4,true );
				leadFilterStatus =1;
				return false;
			});


		});
		function deleteLead(leadid){
			//Delete Listings

			if(confirmDelete()){
				$.getJSON("ajax.php?action=ajax_delete_lead", {"lead_id": leadid }, function(json){
					if(json.error == 0){
						var oTable = $("#showleads").dataTable();
						oTable.fnDraw();
						status_msg("Lead "+json.lead_id+" deleted");
					}else{
				    	status_error(json.error_msg);
					}
			    });
			}
			return false;
		}
		function gotoLead(url){
			window.location = url+"&active="+leadFilterStatus;
		}
		</script>
		';
        //Load Template File
        $page->load_page($config['admin_template_path'] . '/leadmanager.html');

        if ($show_all_leads==true) {
            $login_status = $login->verify_priv('edit_all_leads');
            if ($login_status!==true) {
                return $lang['permission_denied'];
            }
        }
        //Get Counts
        if ($show_all_leads==true) {
            $sql2 = 'SELECT count(feedbackdb_id) as count 
					FROM ' .$config['table_prefix'].'feedbackdb 
					WHERE feedbackdb_status = \'1\'';
        } else {
            //
            $sql2 = 'SELECT count(feedbackdb_id) as count 
					FROM ' .$config['table_prefix'].'feedbackdb 
					WHERE userdb_id = '.intval($_SESSION['userID']).' 
					AND feedbackdb_status = \'1\'';
        }
        $recordSet2 = $conn->Execute($sql2);
        if (!$recordSet2) {
            $misc->log_error($sql2);
        }
        $active_lead_count = $recordSet2->fields['count'];
        if ($show_all_leads==true) {
            $sql2 = 'SELECT count(feedbackdb_id) as count 
					FROM ' .$config['table_prefix'].'feedbackdb 
					WHERE feedbackdb_status = \'0\'';
        } else {
            // WHERE userdb_id = '.intval($_SESSION['userID']).'
            $sql2 = 'SELECT count(feedbackdb_id) as count 
					FROM ' .$config['table_prefix'].'feedbackdb 
					WHERE userdb_id = '.intval($_SESSION['userID']).' 
					AND feedbackdb_status = \'0\'';
        }
        $recordSet2 = $conn->Execute($sql2);
        if (!$recordSet2) {
            $misc->log_error($sql2);
        }
        $inactive_lead_count = $recordSet2->fields['count'];

        //lead_inactive_count
        //lead_active_count
        $page->replace_tag('lead_inactive_count', $inactive_lead_count);
        $page->replace_tag('lead_active_count', $active_lead_count);
        //Feed Back Status Array

        $page->replace_permission_tags();
        $page->auto_replace_tags('', true);
        $display .= $page->return_page();
        return $display;
    }

    public function ajax_change_lead_notes()
    {
        global $config, $lang;

        if (isset($_POST['feedback_id']) && is_numeric($_POST['feedback_id']) && isset($_POST['notes'])) {
            include_once $config['basepath'].'/include/lead_functions.inc.php';
            $lead_functions = new lead_functions();
            include_once $config['basepath'].'/include/login.inc.php';
            $login = new login();
            $feedback_id = intval($_POST['feedback_id']);
            $notes = $_POST['notes'];
            $user_id = intval($_SESSION['userID']);
            $current_owner = $lead_functions->get_feedback_owner($feedback_id);
            //Make sure we have permissions.
            $permission=false;
            if ($_SESSION['userID'] !== $current_owner) {
                $permission = $login->verify_priv('edit_all_leads');
            } else {
                $permission=true;
            }
            if (!$permission) {
                header('Content-type: application/json');
                return json_encode(['error' => '1','error_msg' => $lang['listing_editor_permission_denied']]);
            }
            $lead_functions->set_notes($feedback_id, $notes);
            $lead_functions->set_feedback_mods($feedback_id, $user_id);
            header('Content-type: application/json');
            return json_encode(['error' => '0','lead_id' => $feedback_id]);
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1','error_msg' => $lang['listing_editor_permission_denied']]);
        }
    }

    public function ajax_change_lead_priority()
    {
        global $lang;

        if (isset($_POST['feedback_id']) && is_numeric($_POST['feedback_id']) && isset($_POST['priority']) && in_array($_POST['priority'], ['Low','Normal','Urgent'])) {
            global $conn, $config, $misc;

            include_once $config['basepath'].'/include/lead_functions.inc.php';
            $lead_functions = new lead_functions();
            include_once $config['basepath'].'/include/login.inc.php';
            $login = new login();
            $feedback_id = intval($_POST['feedback_id']);
            $priority = $_POST['priority'];
            $user_id = $_SESSION['userID'];
            $current_owner = $lead_functions->get_feedback_owner($feedback_id);
            //Make sure we have permissions.
            $permission=false;
            if ($_SESSION['userID'] !== $current_owner) {
                $permission = $login->verify_priv('edit_all_leads');
            } else {
                $permission=true;
            }
            if (!$permission) {
                header('Content-type: application/json');
                return json_encode(['error' => '1','error_msg' => $lang['listing_editor_permission_denied']]);
            }
            $lead_functions->set_feedback_priority($feedback_id, $priority);
            $lead_functions->set_feedback_mods($feedback_id, $user_id);
            header('Content-type: application/json');
            return json_encode(['error' => '0','lead_id' => $feedback_id]);
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1','error_msg' => $lang['listing_editor_permission_denied']]);
        }
    }

    public function ajax_change_lead_status()
    {
        global $config, $lang;

        if (isset($_POST['feedback_id']) && is_numeric($_POST['feedback_id']) && isset($_POST['status']) && is_numeric($_POST['status'])) {
            include_once $config['basepath'].'/include/lead_functions.inc.php';
            $lead_functions = new lead_functions();
            include_once $config['basepath'].'/include/login.inc.php';
            $login = new login();
            $feedback_id = intval($_POST['feedback_id']);
            $status = intval($_POST['status']);
            $user_id = intval($_SESSION['userID']);
            $current_owner = $lead_functions->get_feedback_owner($feedback_id);
            //Make sure we have permissions.
            $permission=false;
            if ($_SESSION['userID'] !== $current_owner) {
                $permission = $login->verify_priv('edit_all_leads');
            } else {
                $permission=true;
            }
            if (!$permission) {
                header('Content-type: application/json');
                return json_encode(['error' => '1','error_msg' => $lang['listing_editor_permission_denied']]);
            }
            $lead_functions->set_feedback_status($feedback_id, $status);
            $lead_functions->set_feedback_mods($feedback_id, $user_id);
            header('Content-type: application/json');
            return json_encode(['error' => '0','lead_id' => $feedback_id]);
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1','error_msg' => $lang['listing_editor_permission_denied']]);
        }
    }

    public function ajax_change_lead_agent()
    {
        global $lang;

        if (isset($_POST['feedback_id']) && is_numeric($_POST['feedback_id']) && isset($_POST['user']) && is_numeric($_POST['user'])) {
            global $conn, $config, $misc;

            include_once $config['basepath'].'/include/lead_functions.inc.php';
            $lead_functions = new lead_functions();
            include_once $config['basepath'].'/include/login.inc.php';
            $login = new login();

            $feedback_id = intval($_POST['feedback_id']);
            $new_user_id = intval($_POST['user']);
            $user_id = intval($_SESSION['userID']);
            $current_owner = $lead_functions->get_feedback_owner($feedback_id);
            //Make sure we have permissions.
            $permission=false;
            if ($_SESSION['userID'] !== $current_owner) {
                $permission = $login->verify_priv('edit_all_leads');
            } else {
                $permission=true;
            }
            if (!$permission) {
                header('Content-type: application/json');
                return json_encode(['error' => '1','error_msg' => $lang['listing_editor_permission_denied']]);
            }
            $sql = 'UPDATE ' .  $config['table_prefix'] . 'feedbackdb
           		SET userdb_id = '.$new_user_id.'
           		WHERE (feedbackdb_id = '.$feedback_id.')';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $sql = 'UPDATE ' .  $config['table_prefix'] . 'feedbackdbelements
           		SET userdb_id = '.$new_user_id.'
           		WHERE (feedbackdb_id = '.$feedback_id.')';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $lead_functions->set_feedback_mods($feedback_id, $user_id);
            $lead_functions->send_agent_lead_assigned_notice($feedback_id, $user_id);
            header('Content-type: application/json');
            return json_encode(['error' => '0','lead_id' => $feedback_id]);
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1','error_msg' => $lang['listing_editor_permission_denied']]);
        }
    }

    /*
     *  show_feedback_edit() Edit a specific lead
     */
    public function show_feedback_edit($edit, $all_lead_view = false)
    {
        global $conn, $config, $misc, $jscript, $lang;

        include_once $config['basepath'].'/include/login.inc.php';
        $login = new login();
        include_once $config['basepath'].'/include/forms.inc.php';
        $forms = new forms();
        include_once $config['basepath'].'/include/lead_functions.inc.php';
        $lead_functions = new lead_functions();
        include_once $config['basepath'] . '/include/listing.inc.php';
        $listing = new listing_pages();
        //include_once $config['basepath'].'/include/user.inc.php';
        include_once $config['basepath'] . '/include/core.inc.php';
        $page = new page_admin();
        $display ='';

        $sql_edit = intval($edit);
        // first, grab the feedback's main info
        $sql = 'SELECT feedbackdb_id, feedbackdb_notes, userdb_id, feedbackdb_creation_date, feedbackdb_last_modified,
				listingdb_id, feedbackdb_member_userdb_id
        		FROM ' .  $config['table_prefix'] . 'feedbackdb
        		WHERE (feedbackdb_id = '.$sql_edit.')';
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
        $num_records = $recordSet->RecordCount();
        if ($num_records == 0) {
            die('Failed to access Lead DB');
        }
        while (!$recordSet->EOF) {
            // collect up the main DB's various fields
            $feedback_id = $recordSet->fields['feedbackdb_id'];
            $listingsdb_id = $recordSet->fields['listingdb_id'];
            $edit_notes = $recordSet->fields['feedbackdb_notes'];
            $creation_date = $recordSet->UserTimeStamp($recordSet->fields['feedbackdb_creation_date'], 'm-d-Y g:ia');
            $user_id = $recordSet->fields['userdb_id'];
            $member_id = $recordSet->fields['feedbackdb_member_userdb_id'];
            $last_modified = $recordSet->UserTimeStamp($recordSet->fields['feedbackdb_last_modified'], 'm-d-Y g:ia');
            $recordSet->MoveNext();
        } // end while
        //Make sure we have perission.
        $permission=false;
        if (intval($_SESSION['userID']) !== $user_id) {
            if ($all_lead_view==true) {
                $permission = $login->verify_priv('edit_all_leads');
            }
        } else {
            $permission=true;
        }
        if (!$permission) {
            return $lang['listing_editor_permission_denied'];
        }
        // find the name of the agent listed as ID in $user_id
        $sql='SELECT userdb_user_first_name, userdb_user_last_name
                		FROM ' .  $config['table_prefix'] . 'userdb
                		WHERE (userdb_id = \''.$user_id.'\')';
        $recordSet = $conn->Execute($sql);

        if (!$recordSet) {
            $misc->log_error($sql);
        }
        $agent_html='';
        $agentfname = $recordSet->fields['userdb_user_first_name'];
        $agentlname = $recordSet->fields['userdb_user_last_name'];
        $edit_owner_name =  $agentfname .' '.$agentlname;

        $agent_html .= '<option value="'.$user_id.'">'.$edit_owner_name.'</option>';

        // fill list with names of all agents
        $sql='SELECT userdb_id, userdb_user_first_name, userdb_user_last_name
                		FROM ' .  $config['table_prefix'] . 'userdb
                		WHERE userdb_is_agent = \'yes\'
                		ORDER BY userdb_user_name';
        $recordSet = $conn->Execute($sql);

        if (!$recordSet) {
            $misc->log_error($sql);
        }

        while (!$recordSet->EOF) {
            $agentfname = $recordSet->fields['userdb_user_first_name'];
            $agentlname = $recordSet->fields['userdb_user_last_name'];
            $agent_name =  $agentfname .' '.$agentlname;
            $agent_ID = $recordSet->fields['userdb_id'];
            $agent_html .= '<option value="'.$agent_ID.'">'.$agent_name.'</option>';
            $recordSet->MoveNext();
        }
        $headline='';
        $top_left='';
        $top_right='';
        $center='';
        $bottom_left='';
        $bottom_right='';
        $misc_hold='';

        $sql = 'SELECT feedbackformelements_field_name, feedbackdbelements_field_value, feedbackformelements_location,
						feedbackformelements_field_type, feedbackformelements_field_caption, feedbackformelements_default_text,
						feedbackformelements_field_elements, feedbackformelements_required, feedbackformelements_tool_tip
        		FROM ' .  $config['table_prefix'] . 'feedbackformelements
        		LEFT JOIN ' .  $config['table_prefix'] . 'feedbackdbelements
        		ON feedbackdbelements_field_name = feedbackformelements_field_name and feedbackdb_id = '.$edit.'
        		ORDER BY feedbackformelements_rank';
        $recordSet = $conn->Execute($sql);

        if (!$recordSet) {
            $misc->log_error($sql);
        }

        $field ='';
        while (!$recordSet->EOF) {
            $field_type = $recordSet->fields['feedbackformelements_field_type'];
            $field_name = $recordSet->fields['feedbackformelements_field_name'];
            $field_value = $recordSet->fields['feedbackdbelements_field_value'];
            $field_caption = $recordSet->fields['feedbackformelements_field_caption'];
            $default_text = $recordSet->fields['feedbackformelements_default_text'];
            $field_elements = $recordSet->fields['feedbackformelements_field_elements'];
            $required = $recordSet->fields['feedbackformelements_required'];
            $tool_tip = $recordSet->fields['feedbackformelements_tool_tip'];
            $location = $recordSet->fields['feedbackformelements_location'];
            $field_length='';
            // pass the data to the function
            $field = $forms->renderFormElement($field_type, $field_name, $field_value, $field_caption, $default_text, $required, $field_elements, $field_length, $tool_tip);

            switch ($location) {
                case 'headline':
                    $headline .= $field;
                    break;
                case 'top_left':
                    $top_left .= $field;
                    break;
                case 'top_right':
                    $top_right .= $field;
                    break;
                case 'center':
                    $center .= $field;
                    break;
                case 'bottom_left':
                    $bottom_left .= $field;
                    break;
                case 'bottom_right':
                    $bottom_right .= $field;
                    break;
                default:
                    $misc_hold.=$field;
                    break;
            }
            $recordSet->MoveNext();
        }

        // now, display all that stuff

        $jscript .= '{load_css_view_leads}';

        $jscript .= '<script type="text/javascript">
			$(document).ready(function() {
				$("#update_feedback").validate({
					submitHandler: function(form) {
   						$.post("ajax.php?action=ajax_save_feedback", $("#update_feedback").serialize(),
						function(data){
							if(data.error == "1"){
								status_error(data.error_msg);
							}else{
								status_msg("'.$lang['lead_saved'].'")
							}
					 	}, "json");
   						return false;
   					}
				});

    			$("#status").change( function()
            	{
                	confirmStatus()
            	});

            	$("select#user").change( function()
            	{
  	             	confirmAgentChange()
    	        });
				$("#priority").change( function(){
					var loadUrl = "ajax.php?action=ajax_change_lead_priority";
					var staff_notes = $("textarea[name=staff_notes]");

					$.post(loadUrl, {priority: $("#priority").val(), feedback_id: '.$feedback_id.'},
						function(data) {
							if(data.error == "1")
							{
								status_error(data.error_msg);
							}
							else
							{
								status_msg("Priority '.$lang['generic_saved_text'].' ");
							}
						},"json");

					return;
				});
            	function confirmAgentChange()
				{
					$("#assigned_to").text("Changing... ");
					$("#assigned_to").css("color", "blue");

					var loadUrl = "ajax.php?action=ajax_change_lead_agent";
					var user_id = $("select[name=user]");

					var agree = confirm("Are you sure you want to re-assign this Lead?");

					if (agree)
					{
						$.post(loadUrl, {user: user_id.val(), feedback_id: '.$feedback_id.'},
							function(data) {
								if(data.error == "1")
								{
									status_error(data.error_msg);
									$("#assigned_to").text("Assigned To:");
									$("#assigned_to").css("color", "black");
								}
								else
								{
									status_msg("'.$lang['lead_reassigned'].'");
									$("#assigned_to").text("Assigned To:");
									$("#assigned_to").css("color", "black");
								}
							},"json");
						orig_user_id = user_id.val();
						return;
					}
					else
					{
						$("#assigned_to").text("Assigned To:");
						$("#assigned_to").css("color", "black");
						$("#user").val(orig_user_id);
						return;
					}
				}

				function confirmStatus()
				{
					var loadUrl = "ajax.php?action=ajax_change_lead_status";
					var current_status = $("checkbox[name=active]");

					if ($("#status").is(":checked"))
					{
						$("#active_status").text("Changing...");

						var agree = confirm("Change the status to ACTIVE?");
						if (agree)
						{
							$.post(loadUrl, {status: "1", feedback_id: '.$feedback_id.'},
							function(data) {
								if(data.error == "1")
								{
									status_error(data.error_msg);
								}
								else
								{
									status_msg("'.$lang['leadmanager_status_active'].'");
									$("#active_status").text("'.$lang['leadmanager_status_active'].'");
									$("#active_status").css("color", "blue");

									$("a[href*=\'active=\']").each(function () {
   										var href = $(this).attr("href");
    									if ($(this).attr("href").match(/=[0-9]+&active/))
    									{
    										$(this).attr("href", href.replace(/active=0/, "active=1"));
    									}
									});
								}},"json");
							orig_status = current_status.val();
							return false;
						}
						else
						{
							$("#status").attr("checked", false);
							$("#active_status").text("'.$lang['leadmanager_status_inactive'].'");
							$("#active_status").css("color", "red");
						}
					}
					else
					{
						$("#active_status").text("Changing...");
						$("#active_status").css("color", "red");

						var agree = confirm("Change the status to INACTIVE?");
						if (agree)
						{
							$.post(loadUrl, {change_status: "change_status", status: "0", feedback_id: '.$feedback_id.'},
							function(data) {
							if(data.error == "1")
								{
									status_error(data.error_msg);
								}
								else
								{
									status_msg("'.$lang['leadmanager_status_inactive'].'");
									$("#active_status").text("'.$lang['leadmanager_status_inactive'].'");
									$("#active_status").css("color", "red");


									$("a[href*=\'&active=\']").each(function () {
	   									var href = $(this).attr("href");
	    								if ($(this).attr("href").match(/=[0-9]+&active/))
	    								{
	    									$(this).attr("href", href.replace(/active=1/, "active=0"));
	    								}
									});
								}
							},"json");
							return false;
						}
						else
						{
							$("#status").attr("checked", true);
							$("#active_status").text("'.$lang['leadmanager_status_active'].'");
							$("#active_status").css("color", "blue");
						}
					}
				}
			});
			</script>';

        //get the name of the last person who modified this
        $modified_by = $lead_functions->getFeedbackModData($sql_edit, 'modifiedby');
        $page->load_page($config['admin_template_path'] . '/leadmanager_edit.html');

        $page->page = str_replace('{headline}', $headline, $page->page);
        $page->page = str_replace('{top_left}', $top_left, $page->page);
        $page->page = str_replace('{top_right}', $top_right, $page->page);
        $page->page = str_replace('{center}', $center, $page->page);
        $page->page = str_replace('{bottom_left}', $bottom_left, $page->page);
        $page->page = str_replace('{bottom_right}', $bottom_right, $page->page);
        $page->page = str_replace('{misc_hold}', $misc_hold, $page->page);

        $page->replace_tag('leadmanager_pg_First', $this->leadmanager_pagination('First', $all_lead_view));
        $page->replace_tag('leadmanager_pg_Previous', $this->leadmanager_pagination('Previous', $all_lead_view));
        $page->replace_tag('leadmanager_pg_Last', $this->leadmanager_pagination('Last', $all_lead_view));
        $page->replace_tag('leadmanager_pg_Next', $this->leadmanager_pagination('Next', $all_lead_view));

        $page->replace_tag('leadmanager_status', $this->leadmanager_status());

        $page->replace_tag('leadmanager_priority', $this->leadmanager_priority());

        $leadmanager_agents = $lead_functions->contactAgentEmailDropdown($user_id);
        $page->replace_tag('leadmanager_agent_select', $leadmanager_agents);

        $page->replace_tag('agent_id', $user_id);
        $page->replace_tag('feedback_id', $feedback_id);
        $page->replace_tag('creationdate', $creation_date);
        $page->replace_tag('modifiedby', $modified_by);
        $page->replace_tag('lastmodified', $last_modified);

        $page->replace_tag('edit_notes', $edit_notes);

        $page->replace_listing_field_tags($listingsdb_id);

        $edit_listing_link = $config['baseurl'].'/admin/index.php?action=edit_listing&amp;edit='.$listingsdb_id;
        $page->replace_tag('edit_listing_link', $edit_listing_link);
        $page->replace_user_field_tags($member_id);
        $page->replace_permission_tags();
        $page->auto_replace_tags('', true);

        $display .= $page->return_page();

        return $display;
    }

    public function ajax_save_feedback_status()
    {
        global $config;

        include_once $config['basepath'].'/include/lead_functions.inc.php';
        $lead_functions=new lead_functions();
        if (isset($_POST['feedback_id']) && isset($_POST['status'])) {
            $feedback_id = intval($_POST['feedback_id']);
            $status = intval($_POST['status']);

            if ($status == 1) {
                echo 'Active';
            } else {
                echo 'Inactive';
            }

            $lead_functions->set_feedback_status($feedback_id, $status);
            $user_id = $_SESSION['userID'];

            $result = $lead_functions->set_feedback_mods($feedback_id, $user_id);
            if ($result===false) {
                echo 'Cannot Set Modifier';
            }
        }
    }

    public function ajax_delete_lead($lead_id)
    {
        global $conn, $config, $misc;

        include_once $config['basepath'].'/include/login.inc.php';
        $login = new login();
        $login->verify_priv('Admin');

        // delete a listing
        $sql_delete = intval($lead_id);
        // delete a listing
        $sql = 'DELETE FROM ' .  $config['table_prefix'] . 'feedbackdb 
				WHERE feedbackdb_id = '. $sql_delete;
        $recordSet = $conn->Execute($sql);

        if (!$recordSet) {
            $misc->log_error($sql);
        }
        // delete all the elements associated with a listing
        $sql = 'DELETE FROM ' .  $config['table_prefix'] . 'feedbackdbelements 
				WHERE feedbackdb_id = '.$sql_delete;
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
        header('Content-type: application/json');
        return json_encode(['error'=>0,'lead_id' =>$sql_delete]);
    }

    public function ajax_save_feedback()
    {
        global $conn, $config, $misc, $lang;

        include_once $config['basepath'].'/include/login.inc.php';
        include_once $config['basepath'].'/include/forms.inc.php';
        $forms = new forms();
        $login = new login();
        include_once $config['basepath'].'/include/lead_functions.inc.php';
        $lead_functions=new lead_functions();
        $login->verify_priv('Admin');

        // validates the form
        $pass_the_form = $forms->validateForm('feedbackformelements');

        if ($pass_the_form != 'Yes') {
            // if we're not going to pass it, tell that they forgot to fill in one of the fields
            header('Content-type: application/json');
            return json_encode(['error' => '1','error_msg' => $lang['required_fields_not_filled']]);
        }

        if ($pass_the_form == 'Yes') {
            if (isset($_POST['notes']) && $_POST['edit']) {
                $notes = $_POST['notes'];
                $feedbackdb_id = $_POST['edit'];
                $sql_notes = $misc->make_db_safe($notes);
                $sql_feedbackdb_id = intval($feedbackdb_id);
                $owner = $lead_functions->get_feedback_owner($sql_feedbackdb_id);
                $user_id = intval($_SESSION['userID']);
                //Make sure we have permission
                $login_status = $login->verify_priv('edit_all_leads');
                if ($login_status!==true) {
                    if ($user_id != $owner) {
                        header('Content-type: application/json');
                        return json_encode(['error' => '1','error_msg' => 'Permission Denied']);
                    }
                }

                // update the feedback data
                $sql = 'UPDATE ' .  $config['table_prefix'] . 'feedbackdb
	                		SET feedbackdb_notes = '.$sql_notes.'
	                		WHERE feedbackdb_id = '.$feedbackdb_id;
                $recordSet = $conn->Execute($sql);

                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $message = $lead_functions->updateFeedbackData($feedbackdb_id, $owner);
                if ($message == 'success') {
                    $result = $lead_functions->set_feedback_mods($feedbackdb_id, $user_id);
                    if ($result===false) {
                        echo 'Error Cannot set Mod data';
                    }
                    $misc->log_action('Lead number '.$feedbackdb_id.' updated');
                    header('Content-type: application/json');
                    return json_encode(['error' => '0','lead_id' => $feedbackdb_id]);
                } else {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1','error_msg' => 'Misc Error']);
                } // end else
            } else {
                header('Content-type: application/json');
                return json_encode(['error' => '1','error_msg' => 'Incomplete Form Data']);
            }
        }
    }

    public function leadmanager_priority()
    {
        global $config;

        include_once $config['basepath'].'/include/lead_functions.inc.php';
        $lead_functions = new lead_functions();

        if (isset($_GET['feedback_id']) && !empty($_GET['feedback_id'])) {
            $feedback_id = $_GET['feedback_id'];
        }
        $output = $lead_functions->get_leadmanager_priority($feedback_id);
        return $output;
    }

    public function leadmanager_status()
    {
        global $config;

        include_once $config['basepath'].'/include/lead_functions.inc.php';
        $lead_functions = new lead_functions();

        if (isset($_GET['feedback_id']) && !empty($_GET['feedback_id'])) {
            $feedback_id = $_GET['feedback_id'];
        }
        $output = $lead_functions->get_feedback_status($feedback_id);
        return $output;
    }

    public function leadmanager_pagination($next_prev, $all_leads = false)
    {
        global $conn, $config, $misc;

        if (isset($_GET['active']) && $_GET['active'] ==true) {
            $active = 1;
        } else {
            $active = 0;
        }
        if (isset($_GET['action'])) {
            $action = $_GET['action'];
        }
        if (isset($_GET['feedback_id']) && !empty($_GET['feedback_id'])) {
            $feedback_id = intval($_GET['feedback_id']);

            //Set SQL Based ON view my or view all leads
            $perm_sql = ' AND userdb_id = '.intval($_SESSION['userID']).' ';
            if ($all_leads==true) {
                $perm_sql='';
            }

            $sql = 'SELECT feedbackdb_id
				FROM ' . $config['table_prefix'] . 'feedbackdb
				WHERE feedbackdb_status = \''.$active.'\' '.$perm_sql.'
				ORDER by feedbackdb_id';
            $recordSet = $conn->Execute($sql);

            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $num_rows = $recordSet->RecordCount();

            $loop =0;
            while (!$recordSet->EOF) {
                $ID[$loop] =  $recordSet->fields['feedbackdb_id'];
                if ($ID[$loop] ==  $feedback_id) {
                    $cur_ID = $loop;
                }
                $loop++;
                $recordSet->MoveNext();
            }
            $real_rows = $num_rows -1;

            if ($feedback_id == $ID[0]) {
                $previous = $cur_ID;
            } else {
                $previous = $cur_ID -1;
            }

            if ($cur_ID == $real_rows) {
                $next = $real_rows;
            } else {
                $next= $cur_ID +1;
            }

            switch ($next_prev) {
                case 'First':
                    $output='<a href="index.php?action='.$action.'&amp;feedback_id='.$ID[0] .'&amp;active='.$active.'">
							<img src="{template_url}/images/First.png" alt="First"/>
						</a>';
                    break;
                case 'Previous':
                    $output='<a href="index.php?action='.$action.'&amp;feedback_id='.$ID[$previous] .'&amp;active='.$active.'">
							<img src="{template_url}/images/Previous.png" alt="Previous"/>
						</a>';
                    break;
                case 'Next':
                    $output='<a href="index.php?action='.$action.'&amp;feedback_id='.$ID[$next].'&amp;active='.$active.'">
	    					<img src="{template_url}/images/Next.png" alt="Next"/>
	    				</a>';
                    break;
                case 'Last':
                    $output='<a href="index.php?action='.$action.'&amp;feedback_id='.$ID[$real_rows].'&amp;active='.$active.'">
							<img src="{template_url}/images/Last.png" alt="Last"/>
						</a>';
                    break;
                default:
                    $output='';
                    break;
            }
        }
        return $output;
    }

    /*
     * form_edit() Arrange and edit the Lead Forms
     */
    public function form_edit()
    {
        global $config, $lang, $jscript;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User is an Admin
        $security = $login->verify_priv('edit_lead_template');

        //Load the Core Template
        include_once $config['basepath'] . '/include/core.inc.php';
        $page = new page_admin();
        $page->load_page($config['admin_template_path'] . '/lead_forms.html');
        $display = '';
        $display1 = '';

        if ($security) {
            $display1 .= $this->delete_lead_field();
            $display .= $display1;
            $jscript .= '{load_css_lead_forms}'.BR;
            $jscript .= '{load_css_listing_template_editor}'.BR;

            $jscript .= '<script type="text/javascript">
						$(document).ready(function(){

							$( "#tabs" ).uitabs({
								 beforeActivate: (function(event){
									ShowPleaseWait();
								 }),
								 activate: (function(event){
									HidePleaseWait();
								})
							});

							$("#add_field_link").click(function(){
							
								$.get("ajax.php?action=ajax_add_form_field", function(data){
									$("#message_text").html("<div>"+data+"</div>");
									$("#dialog-message").dialog({
										title: "'.$lang['add_field'].'",
										modal: true,
										width: "auto",
										buttons: null,
										position: {
											my: "center top+5",
											at: "top",
											of: window,
											collision: "none"
										},
										open: function(){
											$("#mini-tabs").uitabs();
						           	        
											$("#update_field").validate({
						           	        	errorLabelContainer: "#response_text",
						            	        submitHandler: function(form) {
						            	        	ShowPleaseWait();
						                	    	$.post("ajax.php?action=ajax_insert_form_field", $("#update_field").serialize(),function(data){
						                    	    	if(data.error == "1"){
						                    	    		$("#dialog-message").dialog( "close" ); 	
						                    	    		status_error(data.error_msg)
						                            	}
						                                else{
							                             	document.location = "'.$config['baseurl'].'/admin/index.php?action=leadmanager_form_edit";
							                                $("#dialog-message").dialog( "close" );
							                                status_msg("'.$lang['admin_template_editor_field_added'].'");
							                            }	
							                         },"json");
							                        return false;
							                    },
							   					highlight: function(element) {
											        $(element).css("background", "#FFCCCC");
											        $(element).css("border-color", "#3300ff");
											    },
											    // Called when the element is valid:
											    unhighlight: function(element) {
											        $(element).css("background", "#FFFFCC");
											         $(element).css("border-color", "#AAAAAA");
											    }
							                });
							                $("#update_button").click(function(){
							                	$("#update_field").submit();
							                });
										},
										close: function(event, ui) { 
											$("#message_text").empty();
											$("#dialog-message").dialog( "destroy" ); 
										}
									});
			 					}, "html" );
			 					return false;
			 				});

							$("#headline, #top_left, #top_right, #center, #feature1, #feature2, #bottom_left, #bottom_right").disableSelection();
							
						});

			 			function edit_form_fields(fname, fcaption) {

			 				$.get("ajax.php?action=ajax_get_form_field_info", { edit_field: fname },
			 				
			 					function(data){
									$("#message_text").html("<div>"+data+"</div>");
									$("#dialog-message").dialog({
										title: "'.$lang['leadmanager_edit_text'].' "+fcaption +" ("+fname+")",
										modal: true,
										width: "auto",
										buttons: null,
										position: {
											my: "center top+5",
											at: "top",
											of: window,
											collision: "none"
										},
										open: function(){
											$("#mini-tabs").uitabs();
						           	        
											$("#update_field").validate({
						           	        	errorLabelContainer: "#response_text",
						            	        submitHandler: function(form) {
						                	    	$.post("ajax.php?action=ajax_update_lead_field", $("#update_field").serialize(),function(data){
						                    	    	if(data.error == "1"){
						                    	    		$("#dialog-message").dialog( "close" ); 	
						                    	    		status_error(data.error_msg)
						                            	}
						                                else{
							                                document.location = "index.php?action=leadmanager_form_edit";
							                                $("#dialog-message").dialog( "close" );
							                                status_msg("'.$lang['field_has_been_updated'].'");
							                            }	
							                         },"json");
							                        return false;
							                    },
							   					highlight: function(element) {
											        $(element).css("background", "#FFCCCC");
											        $(element).css("border-color", "#3300ff");
											    },
											    // Called when the element is valid:
											    unhighlight: function(element) {
											        $(element).css("background", "#FFFFCC");
											         $(element).css("border-color", "#AAAAAA");
											    }
							                });
							                $("#update_button").click(function(){
							                	$("#update_field").submit();
							                });
										},
										close: function(event, ui) { 
											$("#message_text").empty();
											$("#dialog-message").dialog( "destroy" ); 
										}
									});
			 					}, "html" );
			 			}

						function save_fields_locations(){

							var current_location = new Array();
			 				var field_name = new Array();
							var post_trigger ="";
							var action="";

			 				$(".qed_list").each(function(item){
								current_location.push($(this).attr("id"));
								field_name.push($(this).sortable("toArray"));
							});
			 				var action = "ajax.php?action=ajax_save_form_field_order";
			 				var post_field_val = current_location;

							$.post(action, {"search_setup[]": post_field_val, "field_name[]": field_name },
								function(data){
									if(data.error == "1"){
										status_error(data.error_msg);
									}
									else{
										status_msg("{lang_admin_template_editor_field_order_set}");
									}
								},"text");

							return false;
						};

			</script>';

            $page->replace_tag('content', $display);

            $form_template_navbar = $this->show_form_navbar();
            $page->replace_tag('form_template_navbar', $form_template_navbar);

            $page->replace_tag('application_status_text', '');
            $page->replace_lang_template_tags(true);
            $page->replace_permission_tags();
            $page->auto_replace_tags('', true);
            return $page->return_page();
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }

        return $display;
    }

    public function edit_form_qed()
    {
        global $lang,$config;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User is an Admin
        $security = $login->verify_priv('edit_lead_template');
        $display ='';

        if ($security === true) {
            //gets the quick editor pane content for template tag replacement later
            $display .= $this->show_quick_field_edit();

            $display .='<script type="text/javascript">
					//UnSet Focus Event for status_msg
					var handler_status_focus = function status_focus(){

					}

					$(".qed_list").sortable({
						connectWith: "ul",
						tolerance: "pointer",
						revert: true,
						scroll: false,
						opacity: 0.9,
						cancel: "#tabnav",
						update: function(event, ui) {
							save_fields_locations();
						}
					});
					$("#edit_field_list").change(function(){
						fname = $("#edit_field_list").val();
						if(fname != ""){
							fcaption = $("#edit_field_list option:selected").text();
							edit_form_fields(fname, fcaption);
							$("#edit_field_list").val("");
							return false;
						}
					});
					$(".edit_field_link").click(function() {
						var fname =  $(this).attr("name");
						var fcaption = $(this).text();
						edit_form_fields(fname, fcaption);
						return false;
 					});

		</script>';
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function edit_form_field($edit_form_field_name)
    {
        global $lang, $config;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('edit_lead_template');

        $display = '';

        if ($security === true) {
            global $conn, $misc;

            $edit_form_field_name = $misc->make_db_safe($edit_form_field_name);

            $sql = 'SELECT * FROM ' . $config['table_prefix'] . 'feedbackformelements
					WHERE feedbackformelements_field_name = '.$edit_form_field_name;
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $id = $recordSet->fields['feedbackformelements_id'];
            $field_type = $recordSet->fields['feedbackformelements_field_type'];
            $field_name = $recordSet->fields['feedbackformelements_field_name'];

            // Multi Lingual Support
            if (!isset($_SESSION['users_lang'])) {
                // Hold empty string for translation fields, as we are workgin with teh default lang
                $default_lang_field_caption = '';
                $default_lang_default_text = '';
                $default_lang_field_elements = '';

                $field_caption = $recordSet->fields['feedbackformelements_field_caption'];
                $default_text = $recordSet->fields['feedbackformelements_default_text'];
                $field_elements = $recordSet->fields['feedbackformelements_field_elements'];
            } else {
                // Store default lang to show for tanslator
                $default_lang_field_caption = $recordSet->fields['feedbackformelements_field_caption'];
                $default_lang_default_text = $recordSet->fields['feedbackformelements_default_text'];
                $default_lang_field_elements = $recordSet->fields['feedbackformelements_field_elements'];
                $default_lang_tool_tip = $recordSet->fields['feedbackformelements_tool_tip'];
                $field_id = intval($recordSet->fields['feedbackformelements_id']);

                $lang_sql = 'SELECT feedbackformelements_field_caption,feedbackformelements_default_text,
								feedbackformelements_field_elements, feedbackformelements_search_label
							FROM ' . $config['lang_table_prefix'] . 'feedbackformelements
							WHERE feedbackformelements_id = '.$field_id;
                $lang_recordSet = $conn->Execute($lang_sql);
                if (!$lang_recordSet) {
                    $misc->log_error($lang_sql);
                }
                $field_caption = $lang_recordSet->fields['feedbackformelements_field_caption'];
                $default_text = $lang_recordSet->fields['feedbackformelements_default_text'];
                $field_elements = $lang_recordSet->fields['feedbackformelements_field_elements'];
            }

            $rank = $recordSet->fields['feedbackformelements_rank'];
            $required = $recordSet->fields['feedbackformelements_required'];
            $location = $recordSet->fields['feedbackformelements_location'];
            $tool_tip = $recordSet->fields['feedbackformelements_tool_tip'];

            $display .= '<div class="edit_form_div">
							
							<div id="mini-tabs">
								<ul>
									<li><a href="#general_opt">' . $lang['general_options'] . '</a></li>
								</ul>
	
								<!-- tab "panes" -->
								<div id="general_opt">
									<div class="form_gen_options">
									<form action="' . $config['baseurl'] . '/admin/index.php?action=leadmanager_form_edit" method="post" id="update_field">
										<div class="form_div">
											<input type="hidden" name="update_id" value="' . $id . '" />
											<input type="hidden" name="old_field_name" value="' . $field_name . '" />
											<div class="field_caption">' . $lang['admin_template_editor_field_name'] . ':</div>
											<div class="field_element">
												<input type="text" class="required" name="edit_field" value="' . $field_name . '" /><div class="required_field"></div>
												<script>
													$(document).ready(function(){
														$("input[name=edit_field]").on("input",function() {
															var oldVal = $(this).val();
															var newVal = oldVal.replace(RegExp("[^A-Za-z0-9_]","g"),"");
															$(this).val(newVal);
														});
													});		
												</script>
											</div>
										</div>
										<div class="clear"></div>
	
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_caption'] . ':</div>
											<div class="field_element">
												<input type="text" class="required" name="field_caption" value = "' . $field_caption . '" />
												<div class="required_field"></div>';

            if (isset($_SESSION['users_lang'])) {
                // Show Fields value in default language.
                $display .= $lang['translate'] . ': ' . $default_lang_field_caption;
            }

            $display .= '					</div>
										</div>
										<div class="clear"></div>
									
									<div class="form_div">
										<div class="field_caption">' . $lang['admin_template_editor_field_required'] . ':</div>
										<div class="field_element">
										<select name="required" size="1">
											<option value="' . $required . '" selected="selected">' . $lang[strtolower($required)] . '</option>
											<option value="No">-----</option>
											<option value="No">' . $lang['no'] . '</option>
											<option value="Yes" >' . $lang['yes'] . '</option>
										</select>
										</div>
									</div>
									<div class="clear"></div>
	
									<div class="form_div">
										<div class="field_caption">' . $lang['admin_template_editor_field_type'] . ':</div>
										<div class="field_element">
											<select name="field_type" size="1">
												<option value="' . $field_type . '" selected="selected">' . $lang[$field_type] . '</option>
												<option value="">-----</option>
												<option value="text">' . $lang['text'] . '</option>
												<option value="textarea" >' . $lang['textarea'] . '</option>
												<option value="select" >' . $lang['select'] . '</option>
												<option value="select-multiple">' . $lang['select-multiple'] . '</option>
												<option value="option" >' . $lang['option'] . '</option>
												<option value="checkbox" >' . $lang['checkbox'] . '</option>
												<option value="divider">' . $lang['divider'] . '</option>
												<option value="price">' . $lang['price'] . '</option>
												<option value="url">' . $lang['url'] . '</option>
												<option value="email">' . $lang['email'] . '</option>
												<option value="number">' . $lang['number'] . '</option>
												<option value="decimal">' . $lang['decimal'] . '</option>
												<option value="date">' . $lang['date'] . '</option>
												<option value="lat">' . $lang['lat'] . '</option>
												<option value="long">' . $lang['long'] . '</option>
											</select>
										</div>
									</div>
									<div class="clear"></div>
	
									<div class="form_div">
										<div class="field_caption">' . $lang['admin_template_editor_field_elements'] . ':<br />
											<span class="small">(' . $lang['admin_template_editor_choices_separated'] . ')</span>
										</div>
										<div class="field_element">
											<textarea name="field_elements" cols="80" rows="5">' . $field_elements . '</textarea>';

            if (isset($_SESSION['users_lang'])) {
                // Show Fields value in default language.
                $display .= '<br />' . $lang['translate'] . ': ' . $default_lang_field_elements;
            }

            $display .= '				</div>
									</div>
									<div class="clear"></div>

									<div class="form_div">
										<div class="field_caption">' . $lang['admin_template_editor_field_default_text'] . ':</div>
										<div class="field_element"><input type="text" name="default_text" value = "' . $default_text . '" />';

            if (isset($_SESSION['users_lang'])) {
                // Show Fields value in default language.
                $display .= $lang['translate'] . ': ' . $default_lang_default_text;
            }

            $display .= '				</div>
									</div>
									<div class="clear"></div>

									<div class="form_div">
										<div class="field_caption">' . $lang['admin_template_editor_tool_tip'] . ':</div>
										<div class="field_element"><textarea name="tool_tip" cols="80" rows="5">' . $tool_tip . '</textarea>';

            if (isset($_SESSION['users_lang'])) {
                // Show Fields value in default language.
                $display .= '<br />' . $lang['translate'] . ': ' . $default_lang_tool_tip;
            }

            $display .= '				</div>
									</div>
									<div class="clear"></div>

									<div class="form_div">
										<div class="field_caption">Location on Form:</div>
										<div class="field_element">
											<select name="location"  class="required" size="1" class="required">
												<option value="' . $location . '" selected="selected">' . $location . '</option>
												<option value="">-- '.$lang['do_not_display'].' --</option>';

            $sections = explode(',', $config['template_lead_sections']);
            foreach ($sections as $section) {
                $display .= '					<option value="' . $section . '">' . $section . '</option>';
            }

            $display .= '					</select><div class="required_field"></div>
										</div>
									</div>
									<div class="clear"></div>

									<div class="form_div">
										<div class="field_caption">Rank/Order at Location:</div>
										<div class="field_element" ><input type="text" name="rank" value = "' . $rank . '" /></div>
									</div>
									<div class="clear"></div>


									</form>
								</div> <!--// end first pane -->
							</div><!--//end panes -->
							
							

						</div>
						<div class="space_10"></div>
								<div style="text-align: center;">
									<a href="#" class="or_std_button medf" id="update_button">' . $lang['update_button'] . '</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<a href="' . $config['baseurl'] . '/admin/index.php?action=leadmanager_form_edit&amp;delete_field=' . urlencode($field_name) . '" class="or_std_button red medf" onclick="return confirmDelete()">' . $lang['delete'] . '</a>
						</div>
						<div class="space_10"></div>
						</div>';

            return $display;
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
    }

    public function show_quick_field_edit()
    {
        global $lang, $config;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('edit_lead_template');

        if ($security === true) {
            global $conn,  $misc;

            $display = '';
            $dla = explode(',', $config['template_lead_sections']);

            foreach ($dla as $key => $display_loc) {
                // Grab a list of field_names in the Database to Edit
                $sql = 'SELECT feedbackformelements_id, feedbackformelements_field_name,
						feedbackformelements_required, feedbackformelements_field_caption
						FROM ' . $config['table_prefix'] . 'feedbackformelements
						WHERE feedbackformelements_location = \''.$display_loc.'\'
						ORDER BY feedbackformelements_rank';
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }

                // if the display location was added after "bottom_right", it is custom
                // definitely not the best way to handle this
                if ($key  >5) {
                    $display_loc_class = 'center_box';
                    $display .= '<div class="'.$display_loc_class.'">'.$display_loc.BR;
                } else {
                    $display_loc_class = $display_loc;
                    $display .= '<div class="'.$display_loc_class.'">'.$display_loc.BR;
                }

                $display .= '	<ul id="'.$display_loc.'" class="qed_list display_location" title="'.$display_loc.'">'.BR;

                while (!$recordSet->EOF) {
                    $fid = $recordSet->fields['feedbackformelements_id'];

                    // Get Caption from users selected language
                    if (!isset($_SESSION['users_lang'])) {
                        $caption = $recordSet->fields['feedbackformelements_field_caption'];
                    } else {
                        $field_id = intval($fid);
                        $sql2 = 'SELECT feedbackformelements_field_caption
								FROM ' . $config['lang_table_prefix'] . 'feedbackformelements
								WHERE feedbackformelements_id = '.$field_id;
                        $recordSet2 = $conn->Execute($sql2);
                        if (!$recordSet2) {
                            $misc->log_error($sql2);
                        }
                        $caption = $recordSet2->fields['feedbackformelements_field_caption'];
                    }

                    if ($recordSet->fields['feedbackformelements_required'] =='Yes') {
                        $required = '<span class="req_field">&nbsp;</span>';
                    } else {
                        $required ='<span class="fill">&nbsp;</span>';
                    }

                    $field_name = $recordSet->fields['feedbackformelements_field_name'];

                    $display .='<li id="'.$field_name.'">'.BR;
                    $display .= '	<div class="block">'.BR;
                    $display .='		<a href="" class="edit_field_link" name="'.$field_name.'">'.$caption.'</a>'.$required.BR;
                    $display .='	</div>'.BR;
                    $display .='</li>'.BR;

                    $recordSet->MoveNext();
                }
                $display .= '	</ul>'.BR;
                $display .= '</div>'.BR;

                if ($key  == 5) {
                    $display .= '<div class="clear"></div>';
                }
            } //end foreach
            return $display;
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
    }

    public function show_form_navbar()
    {
        global $lang, $config;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('edit_lead_template');

        if ($security === true) {
            global $conn, $misc;

            $display = '';
            // Grab a list of field_names in the Database to Edit
            $sql = 'SELECT feedbackformelements_field_name, feedbackformelements_field_caption, feedbackformelements_id
					FROM ' . $config['table_prefix'] . 'feedbackformelements';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $field_names = '<option value="" selected="selected">'.$lang['select_field_to_edit'].'</option>';

            while (!$recordSet->EOF) {
                // Get Caption from users selected language
                if (!isset($_SESSION['users_lang'])) {
                    $caption = $recordSet->fields['feedbackformelements_field_caption'];
                } else {
                    $field_id = intval($recordSet->fields['feedbackformelements_id']);
                    $sql2 = 'SELECT feedbackformelements_field_caption
							FROM ' . $config['lang_table_prefix'] . 'feedbackformelements
							WHERE feedbackformelements_id = '.$field_id;
                    $recordSet2 = $conn->Execute($sql2);
                    if (!$recordSet2) {
                        $misc->log_error($sql2);
                    }
                    $caption = $recordSet2->fields['feedbackformelements_field_caption'];
                }
                if (isset($_GET['edit_field']) && $_GET['edit_field'] == $recordSet->fields['feedbackformelements_field_name']) {
                    $selected = ' selected="selected" ';
                } else {
                    $selected = '';
                }
                $field_names .= '<option value="' . $recordSet->fields['feedbackformelements_field_name'] . '" ' . $selected . '>' . $caption . ' ('.$recordSet->fields['feedbackformelements_field_name'] .')</option>';

                $recordSet->MoveNext();
            }

            $display .= '<div class="template_editor_navbar">'.BR;
            $display .= '	<form action="' . $config['baseurl'] . '/admin/index.php" id="navbar">'.BR;
            $display .= '		<div class="template_editor_navbar_form">' . $lang['field'];
            $display .= '			<input type="hidden" name="action" value="edit_form_template" />'.BR;
            $display .= '			<select id="edit_field_list">'.BR;
            $display .= $field_names;
            $display .= '			</select>'.BR;
            //$display .= '         <span class="or_std_button add_field_link" id="add_field_link">' . $lang['add_field'] . '</span>'.BR;
            $display .= '			<button type="submit" class="or_std_button" id="add_field_link" >'.$lang['add_field'].' <span class="ui-icon ui-icon-plusthick"></span></button>'.BR;

            $display .= '		</div>'.BR;
            $display .= '		<div style="clear: both;"></div>'.BR;
            $display .= '	</form>'.BR;
            $display .= '</div>'.BR;
            $display .= '<div style="clear: both;"></div><br />'.BR;

            return $display;
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
    }

    public function ajax_get_form_field_info()
    {
        global $lang, $config;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User is an Admin
        $security = $login->verify_priv('edit_lead_template');
        $display = '';

        if ($security === true) {
            if (isset($_GET['edit_field'])) {
                $display .= $this->edit_form_field($_GET['edit_field']);
            }
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function ajax_add_form_field()
    {
        global $lang,$config;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User is an Admin
        $security = $login->verify_priv('edit_lead_template');

        if ($security) {
            $display = $this->add_form_template_field();
        } else {
            $display = '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function ajax_save_form_field_order()
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User is an Admin
        $security = $login->verify_priv('edit_lead_template');

        echo '<pre>'.print_r($_POST['field_name'], true).'</pre>';
        echo '<pre>'.print_r($_POST['search_setup'], true).'</pre>';

        $display ='';
        if ($security === true) {
            global $conn, $misc;

            if (isset($_POST['search_setup'])) {
                $count = 0;
                $num_locations = count($_POST['search_setup']);

                while ($count < $num_locations) {
                    //explode the comma delimited field list for this display location
                    $field_name_arr[$count] = explode(',', $_POST['field_name'][$count]);

                    $field_location = $_POST['search_setup'][$count];
                    $display .= $field_location .' - '.$lang['generic_saved_text'].' <br />'.BR;
                    $field_rank =0;

                    foreach ($field_name_arr[$count] as $field_name) {
                        //empty locations are skipped
                        if (!empty($field_name)) {
                            $field_rank = $field_rank+1;
                            $sql = 'UPDATE ' . $config['table_prefix'] . 'feedbackformelements
									SET feedbackformelements_location = \''.$field_location.'\', feedbackformelements_rank = \''.$field_rank.'\'
									WHERE feedbackformelements_field_name = \''.$field_name.'\'';
                            $recordSet = $conn->Execute($sql);
                            if (!$recordSet) {
                                $misc->log_error($sql);
                            }
                        }
                    }
                    $count++;
                }
                $display .= '<center><strong>' . $lang['admin_template_editor_field_order_set'] . ' ('.$num_locations.')</strong></center>';
            }
        } else {
            $display = '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function ajax_insert_form_field()
    {
        global $lang, $config;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('edit_lead_template');

        if ($security === true) {
            global $conn, $misc;

            if (isset($_POST['edit_field']) && !isset($_POST['lang_change'])) {
                $field_type = $misc->make_db_safe($_POST['field_type']);
                $_POST['edit_field'] = str_replace(' ', '_', $_POST['edit_field']);
                $field_name = $misc->make_db_safe($_POST['edit_field']);
                $field_caption = $misc->make_db_safe($_POST['field_caption']);
                $default_text = $misc->make_db_safe($_POST['default_text']);
                $field_elements = $misc->make_db_safe($_POST['field_elements']);
                $rank = intval($_POST['rank']);
                $required = $misc->make_db_safe($_POST['required']);
                $location = $misc->make_db_safe($_POST['location']);
                $tool_tip = $misc->make_db_safe($_POST['tool_tip']);

                $id_rand = rand(0, 999999);

                $sql = 'INSERT INTO ' . $config['table_prefix'] . 'feedbackformelements
						(feedbackformelements_field_type, feedbackformelements_field_name, feedbackformelements_field_caption,
						feedbackformelements_default_text, feedbackformelements_field_elements, feedbackformelements_rank,
						feedbackformelements_required, feedbackformelements_location, feedbackformelements_tool_tip)
						VALUES ('.$field_type.','.$id_rand.','.$field_caption.','.$default_text.','.$field_elements.', '.$rank.', '.$required.', '.$location.', '.$tool_tip.')';
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                // Now we need to get the field ID
                $sql = 'SELECT feedbackformelements_id 
						FROM ' . $config['table_prefix'] . 'feedbackformelements
						WHERE feedbackformelements_field_name = ' . $id_rand;
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $feedbackformelements_id = $recordSet->fields['feedbackformelements_id'];
                // Set Real Name
                $sql = 'UPDATE ' . $config['table_prefix'] . 'feedbackformelements 
						SET feedbackformelements_field_name = ' . $field_name . '
						WHERE feedbackformelements_field_name = ' . $id_rand;
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                // We should now add a blank field for each lead that already exist.
                $sql = 'SELECT feedbackdb_id, userdb_id 
						FROM ' . $config['table_prefix'] . 'feedbackdb';
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $id = [];
                $user = [];
                while (!$recordSet->EOF) {
                    $id[] = $recordSet->fields['feedbackdb_id'];
                    $user[] = $recordSet->fields['userdb_id'];
                    $recordSet->MoveNext();
                }
                $count = count($id);
                $x = 0;
                while ($x < $count) {
                    $sql = 'INSERT INTO ' . $config['table_prefix'] . 'feedbackdbelements
							(feedbackdbelements_field_name, feedbackdb_id,userdb_id,feedbackdbelements_field_value)
							VALUES ('.$field_name.','.$id[$x].','.$user[$x].', \'\')';
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $misc->log_error($sql);
                    }
                    $x++;
                }
            }
            header('Content-type: application/json');
            return json_encode(['error' => '0','field_id' => $feedbackformelements_id]);
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1','error_msg' =>  $lang['access_denied']]);
        }
    }

    public function ajax_update_lead_field()
    {
        global $lang, $config;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $display = '';
        $security = $login->verify_priv('edit_lead_template');
        if ($security === true) {
            global $conn, $misc;

            if (isset($_POST['update_id']) && !isset($_POST['lang_change'])) {
                $id = intval($_POST['update_id']);
                $_POST['old_field_name'] = str_replace(' ', '_', $_POST['old_field_name']);
                $_POST['edit_field'] = str_replace(' ', '_', $_POST['edit_field']);
                $field_name = $misc->make_db_safe($_POST['edit_field']);
                $old_field_name = $misc->make_db_safe($_POST['old_field_name']);
                $required = $misc->make_db_safe($_POST['required']);
                $update_field_name = false;

                if ($old_field_name != $field_name) {
                    $update_field_name = true;
                }

                $field_type = $misc->make_db_safe($_POST['field_type']);
                $field_caption = $misc->make_db_safe($_POST['field_caption']);
                $default_text = $misc->make_db_safe($_POST['default_text']);
                $field_elements = $misc->make_db_safe($_POST['field_elements']);
                $rank = $misc->make_db_safe($_POST['rank']);
                $location = $misc->make_db_safe($_POST['location']);
                $tool_tip = $misc->make_db_safe($_POST['tool_tip']);

                $sql = 'UPDATE ' . $config['table_prefix'] . 'feedbackformelements
							SET feedbackformelements_field_type = '.$field_type.', feedbackformelements_field_name = '.$field_name.',
							feedbackformelements_rank = '.$rank.', feedbackformelements_required = '.$required.',
							feedbackformelements_location = '.$location.', feedbackformelements_tool_tip = '.$tool_tip.'
							WHERE feedbackformelements_id = '.$id;
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                // Update Current language
                if (!isset($_SESSION['users_lang'])) {
                    $lang_sql = 'UPDATE  ' . $config['table_prefix'] . 'feedbackformelements
									SET feedbackformelements_field_caption = '.$field_caption.', feedbackformelements_field_elements = '.$field_elements.', feedbackformelements_default_text = '.$default_text.',
									feedbackformelements_tool_tip = '.$tool_tip.'
									WHERE feedbackformelements_id = '.$id;
                    $lang_recordSet = $conn->Execute($lang_sql);
                    if (!$lang_recordSet) {
                        $misc->log_error($lang_sql);
                    }
                } else {
                    $lang_sql = 'DELETE FROM  ' . $config['lang_table_prefix'] . 'feedbackformelements
									WHERE feedbackformelements_id = '.$id;
                    $lang_recordSet = $conn->Execute($lang_sql);
                    if (!$lang_recordSet) {
                        $misc->log_error($lang_sql);
                    }
                    $lang_sql = 'INSERT INTO ' . $config['lang_table_prefix'] . 'feedbackformelements
									(feedbackformelements_id, feedbackformelements_field_caption, feedbackformelements_default_text,
									feedbackformelements_field_elements, feedbackformelements_tool_tip)
									VALUES ('.$id.', '.$field_caption.','.$default_text.','.$field_elements.','.$tool_tip.')';
                    $lang_recordSet = $conn->Execute($lang_sql);
                    if (!$lang_recordSet) {
                        $misc->log_error($lang_sql);
                    }
                }
                // Check if field name changed, if it as update all feedbackdbelement tables
                if ($update_field_name) {
                    $lang_sql = 'UPDATE  '.$config['table_prefix'].'feedbackdbelements
									SET feedbackdbelements_field_name = '.$field_name.'
									WHERE feedbackdbelements_field_name = '.$old_field_name;
                    $lang_recordSet = $conn->Execute($lang_sql);
                    if (!$lang_recordSet) {
                        $misc->log_error($lang_sql);
                    }
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0','field_id' =>  $id]);
            }
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1','error_msg' =>  $lang['access_denied']]);
        }
        return $display;
    }

    public function add_form_template_field()
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User has cred
        $security = $login->verify_priv('edit_lead_template');
        $display = '';

        if ($security) {
            $display .= '<div class="edit_form_div">
							<div id="mini-tabs">
								<ul>
									<li><a href="#general_opt">' . $lang['general_options'] . '</a></li>
								</ul>

					    		<div id="general_opt">
									<form action="#" method="post" id="update_field">
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_name'] . ':</div>
											<div class="field_element">
												<input type="text" name="edit_field" class="required" value="" /><div class="required_field"></div>
												<script>
													$(document).ready(function(){
														$("input[name=edit_field]").on("input",function() {
															var oldVal = $(this).val();
															var newVal = oldVal.replace(RegExp("[^A-Za-z0-9_]","g"),"");
															$(this).val(newVal);
														});
													});		
												</script>
											</div>
										</div>
										<div class="clear"></div>
	
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_caption'] . ':</div>
											<div class="field_element" ><input type="text" name="field_caption" class="required" value=""><div class="required_field"></div></div>
										</div>
										<div class="clear"></div>
										
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_required'] . ':</div>
											<div class="field_element">
												<select name="required" size="1">
													<option value="No" selected="selected">' . $lang['no'] . '</option>
													<option value="Yes" >' . $lang['yes'] . '</option>
												</select>
											</div>
										</div>
										<div class="clear"></div>
	
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_type'] . ':</div>
											<div class="field_element">
											<select name="field_type" class="required">
												<option value="text" selected="selected">' . $lang['text'] . '</option>
												<option value="textarea" >' . $lang['textarea'] . '</option>
												<option value="select" >' . $lang['select'] . '</option>
												<option value="select-multiple">' . $lang['select-multiple'] . '</option>
												<option value="option" >' . $lang['option'] . '</option>
												<option value="checkbox" >' . $lang['checkbox'] . '</option>
												<option value="divider">' . $lang['divider'] . '</option>
												<option value="price">' . $lang['price'] . '</option>
												<option value="url">' . $lang['url'] . '</option>
												<option value="email">' . $lang['email'] . '</option>
												<option value="number">' . $lang['number'] . '</option>
												<option value="decimal">' . $lang['decimal'] . '</option>
												<option value="date">' . $lang['date'] . '</option>
												<option value="lat">' . $lang['lat'] . '</option>
												<option value="long">' . $lang['long'] . '</option>
											</select>
											</div>
										</div>
										<div class="clear"></div>
	
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_elements'] . ':<br />
												<span class="small">(' . $lang['admin_template_editor_choices_separated'] . ')</span>
											</div>
											<div class="field_element"><textarea name="field_elements"  cols="80" rows="5"></textarea></div>
										</div>
										<div class="clear"></div>
	
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_default_text'] . ':</div>
											<div class="field_element"><input type="text" name="default_text" value="" /></div>
										</div>
										<div class="clear"></div>
	
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_tool_tip'] . ':</div>
											<div class="field_element" align="left"><textarea name="tool_tip" cols="80" rows="5"></textarea></div>
										</div>
										<div class="clear"></div>
	
										<div class="form_div">
											<div class="field_caption">Location on Form:</div>
											<div class="field_element">
												<select name="location" size="1" class="required">
													<option value="" selected="selected"></option>
													<option value="">-- '.$lang['do_not_display'].' --</option>';

            $sections = explode(',', $config['template_lead_sections']);
            foreach ($sections as $section) {
                $display .= '						<option value="' . $section . '">' . $section . '</option>';
            }

            $display .= '						</select><div class="required_field"></div>
											</div>
										</div>
										<div class="clear"></div>
	
										<div class="form_div">
											<div class="field_caption">Rank/Order at Location:</div>
											<div class="field_element"><input type="text" name="rank" value="0"></div>
										</div>
										<div class="clear"></div>
										
									</form>
									
								</div><!--// end first pane -->
									
							</div><!-- //end tabs-->';

            $display .='	<div class="space_10">&nbsp;</div>
			
							<div style="text-align: center;">
								<a href="#" class="or_std_button medf" id="update_button">' . $lang['add_field'] . '</a>
							</div>
							<div class="space_10">&nbsp;</div>
						</div>';
        } else {
            $display = '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function addlead_create_lead()
    {
        global $config, $conn, $misc;

        include_once $config['basepath'] . '/include/listing.inc.php';
        $listing_pages = new listing_pages();
        include_once $config['basepath'] . '/include/lead_functions.inc.php';
        $lead_functions = new lead_functions();

        if (isset($_POST['listing_id'])) {
            $listingID = intval($_POST['listing_id']);
            $memberID = intval($_POST['member_id']);
            $agent_id = $listing_pages->get_listing_agent_value('userdb_id', $listingID);
        } else {
            $listingID=0;
            $agent_id = intval($_POST['agent_id']);
            $memberID = intval($_POST['member_id']);
        }

        $notes = $_POST['notes'];
        if ($agent_id > 0 && $memberID > 0) {
            //START - Determine AgentID Based on listingID
            //Check to see if agent's notifications should be redirected to the floor agent.
            $sql = 'SELECT userdb_send_notifications_to_floor FROM ' . $config['table_prefix'] . 'userdb WHERE userdb_id = '.$agent_id;
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $userdb_send_notifications_to_floor = $recordSet->fields['userdb_send_notifications_to_floor'];
            if ($userdb_send_notifications_to_floor == 1) {
                //Get Floor Agent ID
                $sql = 'SELECT controlpanel_floor_agent, controlpanel_floor_agent_last FROM ' . $config['table_prefix_no_lang'] . 'controlpanel';
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $controlpanel_floor_agent = $recordSet->fields['controlpanel_floor_agent'];
                $controlpanel_floor_agent_last = $recordSet->fields['controlpanel_floor_agent_last'];
                $floor_agents = explode(',', $controlpanel_floor_agent);
                if (count($floor_agents)>1) {
                    $update_floor=false;
                    //Determine Last Floor Agent
                    if ($controlpanel_floor_agent_last>1) {
                        $first_agent_id = $floor_agents[0];
                        $use_next = false;
                        $found_agent = false;
                        foreach ($floor_agents as $key => $floor_id) {
                            if (!$use_next) {
                                if ($floor_id == $controlpanel_floor_agent_last) {
                                    $use_next = true;
                                }
                            } else {
                                $new_agent_id = $floor_id;
                                if ($new_agent_id>1) {
                                    $found_agent = true;
                                    $update_floor=true;
                                    $agent_id = $new_agent_id;
                                }
                            }
                        }
                        if (!$found_agent) {
                            //No Last Agent set so use the first in list
                            $new_agent_id = $floor_agents[0];
                            if ($new_agent_id>1) {
                                $update_floor=true;
                                $agent_id = $new_agent_id;
                            }
                        }
                    } else {
                        //No Last Agent set so use the first in list
                        $new_agent_id = $floor_agents[0];
                        if ($new_agent_id>1) {
                            $update_floor=true;
                            $agent_id = $new_agent_id;
                        }
                    }
                    if ($update_floor) {
                        $sql = 'UPDATE ' . $config['table_prefix_no_lang'] . 'controlpanel SET controlpanel_floor_agent_last = '.$agent_id;
                        $recordSet = $conn->Execute($sql);
                        if (!$recordSet) {
                            $misc->log_error($sql);
                        }
                    }
                } else {
                    $new_agent_id = $floor_agents[0];
                    if ($new_agent_id>1) {
                        $agent_id = $new_agent_id;
                    }
                }
            }
            //END Agent ID Detection
            $sql_notes = addslashes($notes);
            $sql = 'INSERT INTO ' . $config['table_prefix'] . "feedbackdb (feedbackdb_notes, userdb_id, listingdb_id, feedbackdb_creation_date, feedbackdb_last_modified, feedbackdb_status, feedbackdb_priority,feedbackdb_member_userdb_id )
			                		VALUES ('$sql_notes', $agent_id, $listingID, ".$conn->DBTimeStamp(time()).','.$conn->DBTimeStamp(time()).", 1, 'Normal',$memberID) ";
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $new_feedback_id = $conn->Insert_ID(); // this is the new feedback's ID number
            // now that's taken care of, it's time to insert all the rest
            // of the variables into the database
            $message = $lead_functions->updateFeedbackData($new_feedback_id, $agent_id, $listingID);
            //$sql_user = $misc->make_db_safe($agent_id);
            if ($message == 'success') {
                //get the Agent's full name & email
                $sql = 'SELECT userdb_user_first_name, userdb_user_last_name, userdb_emailaddress
										FROM ' . $config['table_prefix'] . "userdb
										WHERE userdb_id = $agent_id";
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $user_name = $recordSet->fields['userdb_user_first_name'].' '.$recordSet->fields['userdb_user_last_name'];
                $listing_emailAddress = $recordSet->fields['userdb_emailaddress'];
                // Report that Feedback has been sent to the AGENT
                //$output .= '<div id="feedback_sent_message">'.$lang['your_feedback_has_been_sent'].' '.$user_name.'</div>';
                $misc->log_action("Created feedback $new_feedback_id for $user_name");
                $lead_functions->send_agent_feedback_notice($new_feedback_id);
                if (isset($_POST['sendmember_email']) && $_POST['sendmember_email']==1) {
                    $lead_functions->send_user_feedback_notice($new_feedback_id);
                }
                include_once $config['basepath'] . '/include/hooks.inc.php';
                $hooks = new hooks();
                $hooks->load('after_new_lead', $new_feedback_id);
                return ['error'=>false,'lead_id'=>$new_feedback_id];
            } else {
                return ['error'=>true];
                //$output .= '<p>There\'s been a problem -- please contact the site administrator</p>';
            } // end else
        }
    }

    public function delete_lead_field()
    {
        global $lang, $config;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('edit_lead_template');
        if ($security === true) {
            global  $conn, $misc;

            if (isset($_GET['delete_field']) && !isset($_POST['lang_change'])) {
                $field_name = $misc->make_db_safe($_GET['delete_field']);
                $sql = 'SELECT feedbackformelements_id FROM ' . $config['table_prefix'] . 'feedbackformelements
						WHERE feedbackformelements_field_name = '.$field_name;
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                // Delete All Translation for this field.
                $configured_langs = explode(',', $config['configured_langs']);
                while (!$recordSet->EOF) {
                    $feedbackformelements_id = $recordSet->fields['feedbackformelements_id'];
                    foreach ($configured_langs as $configured_lang) {
                        $sql = 'DELETE FROM ' . $config['table_prefix_no_lang'] . $configured_lang . '_feedbackformelements
								WHERE feedbackformelements_id = '.$feedbackformelements_id;
                        $recordSet = $conn->Execute($sql);
                        if (!$recordSet) {
                            $misc->log_error($sql);
                        }
                    }
                }
                // Cleanup any feedbackdbelements entries from this field.
                foreach ($configured_langs as $configured_lang) {
                    $sql = 'DELETE FROM ' . $config['table_prefix_no_lang'] . $configured_lang . '_feedbackdbelements
							WHERE feedbackdbelements_field_name = '.$field_name;
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $misc->log_error($sql);
                    }
                }
            }
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
    }
}
