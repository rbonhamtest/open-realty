<?php


class login
{
    public $debug;

    public function check_login()
    {
        /* Check if user has been remembered */
        if (isset($_COOKIE['cookname']) && isset($_COOKIE['cookpass'])) {
            $_SESSION['username'] = $_COOKIE['cookname'];
            $_SESSION['userpassword'] = $_COOKIE['cookpass'];
        }
        /* Username and password have been set */
        if (isset($_SESSION['username']) && isset($_SESSION['userpassword'])) {
            /* Confirm that username and password are valid */
            if ($this->confirm_user($_SESSION['username'], $_SESSION['userpassword']) != 0) {
                /* Variables are incorrect, user not logged in */
                unset($_SESSION['username']);
                unset($_SESSION['userpassword']);
                if (isset($_COOKIE['cookname']) && isset($_COOKIE['cookpass'])) {
                    // Destroy Cookie
                    setcookie('cookname', '', time() - 3600, '/');
                    setcookie('cookpass', '', time() - 3600, '/');
                }
                return false;
            }
            return true;
        }
        /* User not logged in */
        else {
            return false;
        }
    }

    public function verify_member_email($email)
    {
        global $config, $conn, $misc;

        header('Content-type: application/json');

        //Make sure this from our site.
        if (!$misc->referer_check()) {
            return json_encode(['error'=>true,'error_code'=>1, 'error_msg'=>'Form Security Violation1']);
        } else {
            //Make Sure Email is Valid
            $valid = $misc->validate_email($email);
            if (!$valid) {
                return json_encode(['error'=>true,'error_code'=>4,'error_msg'=>'The Email address you entered is invalid.']);
            }
            $sql = 'SELECT * FROM '.$config['table_prefix'].'userdb 
							WHERE  userdb_emailaddress = \'' . addslashes($email).'\'';
            $recordSet = $conn->Execute($sql);
            //print_r($recordSet);
            if (!$recordSet || ($recordSet->RecordCount() < 1)) {
                return json_encode(['error'=>true,'error_code'=>2,'error_msg'=>'Not A Member']);
            }
            if ($recordSet->fields['userdb_active'] == 'yes') {
                return json_encode(['error'=>false]);
            } else {
                return json_encode(['error'=>true,'error_code'=>3,'error_msg'=>'Member Account is Disabled']);
            }
        }
    }

    public function ajax_check_member_login($email, $pass)
    {
        global $config, $conn, $misc;

        header('Content-type: application/json');

        //Make sure this from our site.
        if (!$misc->referer_check()) {
            return json_encode(['error'=>true,'error_code'=>1, 'error_msg'=>'Form Security Violation2']);
        } else {
            //Make Sure Email is Valid
            $valid = $misc->validate_email($email);
            if (!valid) {
                return json_encode(['error'=>true,'error_code'=>4,'error_msg'=>'The Email address you entered is invalid.']);
            }
            $sql = 'SELECT * FROM '.$config['table_prefix'].'userdb WHERE  userdb_emailaddress = \'' . addslashes($email).'\'';
            $recordSet = $conn->Execute($sql);
            //print_r($recordSet);
            if (!$recordSet || ($recordSet->RecordCount() < 1)) {
                return json_encode(['error'=>true,'error_code'=>2,'error_msg'=>'Not A Member']);
            }
            if ($recordSet->fields['userdb_active'] == 'yes') {
                $_POST['user_name'] = $recordSet->fields['userdb_user_name'];
                $_POST['user_pass']= trim($pass);
                $login_status = $this->loginCheck('Member', true);
                if (!$login_status) {
                    return json_encode(['error'=>true,'error_code'=>5,'error_msg'=>'Password Incorrect']);
                } else {
                    //Return Users First and Last Name
                    include_once $config['basepath'] . '/include/user.inc.php';
                    $user = new user();
                    $fname = $user->get_user_single_item('userdb_user_first_name', $_SESSION['userID']);
                    $lname = $user->get_user_single_item('userdb_user_last_name', $_SESSION['userID']);
                    return json_encode(['error'=>false,'fname'=>$fname,'lname'=>$lname]);
                }
            } else {
            }
        }
    }

    public function loginCheck($priv_level_needed, $internal = false)
    {
        global $conn, $config, $lang;

        $display = '';
        $new_login = true;
        if (isset($_SESSION['username']) && isset($_SESSION['userpassword'])) {
            $new_login = $this->confirm_user($_SESSION['username'], $_SESSION['userpassword']);
            if ($new_login==0) {
                $new_login=false;
            } else {
                $new_login=true;
            }
        }

        $checked = $this->check_login();
        if (!$checked and !isset($_POST['user_name'])) {
            if ($internal !== true) {
                return $this->display_login($priv_level_needed);
            } else {
                return false;
            }
        } elseif (isset($_POST['user_name'])) {
            if (!$_POST['user_name'] || !$_POST['user_pass']) {
                if ($internal !== true) {
                    $display .= $this->display_login($priv_level_needed, $lang['required_field_not_filled']);
                    return $display;
                } else {
                    return false;
                }
            }
            /* Spruce up username, check length */
            $_POST['user_name'] = trim($_POST['user_name']);
            if (strlen($_POST['user_name']) > 30) {
                if ($internal !== true) {
                    $display .= $this->display_login($priv_level_needed, $lang['username_excessive_length']);
                    return $display;
                } else {
                    return false;
                }
            }
            /* Checks that username is in database and password is correct */
            $md5pass = md5($_POST['user_pass']);
            $result = $this->confirm_user($_POST['user_name'], $md5pass);
            /* Check error codes */
            if ($result == 1) {
                if ($internal !== true) {
                    $display .= $this->display_login($priv_level_needed, $lang['nonexistent_username']);
                    return $display;
                } else {
                    return false;
                }
            } elseif ($result == 2) {
                if ($internal !== true) {
                    $display .= $this->display_login($priv_level_needed, $lang['incorrect_password']);
                    return $display;
                } else {
                    return false;
                }
            } elseif ($result == 3) {
                if ($internal !== true) {
                    $display .= $this->display_login($priv_level_needed, $lang['inactive_user']);
                    return $display;
                } else {
                    return false;
                }
            }
        }
        if (isset($_POST['user_name']) || $checked) {
            global $misc;

            /* Username and password correct, register session variables */
            if (isset($_POST['user_name'])) {
                $_POST['user_name'] = stripslashes($_POST['user_name']);
                $_SESSION['username'] = $_POST['user_name'];
                $_SESSION['userpassword'] = $md5pass;
            }

            $username = $conn->qstr($_SESSION['username']);
            //$username = mysql_real_escape_string($_SESSION['username']);
            $sql = 'SELECT * FROM ' . $config['table_prefix'] . 'userdb 
					WHERE  userdb_user_name='.$username.'';
            $recordSet = $conn->Execute($sql);

            $_SESSION['userID'] = $recordSet->fields['userdb_id'];
            $_SESSION['admin_privs'] = $recordSet->fields['userdb_is_admin'];
            $_SESSION['active'] = $recordSet->fields['userdb_active'];
            $_SESSION['isAgent'] = $recordSet->fields['userdb_is_agent'];
            $_SESSION['featureListings'] = $recordSet->fields['userdb_can_feature_listings'];
            $_SESSION['viewLogs'] = $recordSet->fields['userdb_can_view_logs'];
            $_SESSION['moderator'] = $recordSet->fields['userdb_can_moderate'];
            $_SESSION['editpages'] = $recordSet->fields['userdb_can_edit_pages'];
            $_SESSION['havevtours'] = $recordSet->fields['userdb_can_have_vtours'];
            $_SESSION['haveuserfiles'] = $recordSet->fields['userdb_can_have_user_files'];
            $_SESSION['havefiles'] = $recordSet->fields['userdb_can_have_files'];
            $_SESSION['is_member'] = 'yes';

            // New Permissions with OR 2.1
            $_SESSION['edit_site_config'] = $recordSet->fields['userdb_can_edit_site_config'];
            $_SESSION['edit_member_template'] = $recordSet->fields['userdb_can_edit_member_template'];
            $_SESSION['edit_agent_template'] = $recordSet->fields['userdb_can_edit_agent_template'];
            $_SESSION['edit_listing_template'] = $recordSet->fields['userdb_can_edit_listing_template'];
            $_SESSION['export_listings'] = $recordSet->fields['userdb_can_export_listings'];
            $_SESSION['edit_all_listings'] = $recordSet->fields['userdb_can_edit_all_listings'];
            $_SESSION['edit_all_users'] = $recordSet->fields['userdb_can_edit_all_users'];
            $_SESSION['edit_property_classes'] = $recordSet->fields['userdb_can_edit_property_classes'];
            $_SESSION['edit_expiration'] =  $recordSet->fields['userdb_can_edit_expiration'];
            $_SESSION['blog_user_type']=$recordSet->fields['userdb_blog_user_type'];
            $_SESSION['can_manage_addons']=$recordSet->fields['userdb_can_manage_addons'];
            $_SESSION['edit_lead_template']=$recordSet->fields['userdb_can_edit_lead_template'];
            $_SESSION['edit_all_leads']=$recordSet->fields['userdb_can_edit_all_leads'];
            /**
             * This is the cool part: the user has requested that we remember that
             * he's logged in, so we set two cookies. One to hold his username,
             * and one to hold his md5 encrypted password. We set them both to
             * expire in 100 days. Now, next time he comes to our site, we will
             * log him in automatically.
             */
            if (isset($_POST['remember'])) {
                setcookie('cookname', $_SESSION['username'], time() + 60 * 60 * 24 * 100, '/');
                setcookie('cookpass', $_SESSION['userpassword'], time() + 60 * 60 * 24 * 100, '/');
            }
            //call the after login plugin function
            if ($new_login) {
                include_once $config['basepath'] . '/include/hooks.inc.php';
                $hooks = new hooks();
                $hooks->load('after_user_login', $recordSet->fields['userdb_id']);
            }
            if (!$this->verify_priv($priv_level_needed)) {
                if ($internal !== true) {
                    $display .= $this->display_login($priv_level_needed, $lang['access_denied']);
                    return $display;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
    }

    public function display_login($priv_level_needed, $error_msg = '')
    {
        global $config;

        $login_status = $this->loginCheck('Member', true);
        if (isset($_SERVER['HTTP_REFERER']) && $_GET['action']=='member_login') {
            if (strpos($_SERVER['HTTP_REFERER'], $config['baseurl'])===0) {
                if (!isset($_SESSION['login_referer'])) {
                    $_SESSION['login_referer']=$_SERVER['HTTP_REFERER'];
                }
            }
        }
        if ($login_status == true) {
            if ($_SESSION['login_referer']) {
                $referer_url = $_SESSION['login_referer'];
                unset($_SESSION['login_referer']);
            } else {
                $referer_url = $config['baseurl'] . '/index.php';
            }
            header('Location: ' . $referer_url);
        } else {
            global $lang, $conn, $misc;

            /*
            if(isset($_SESSION['language_template'])){
                $language_template = $_SESSION['language_template']; // store for the Multilingual add-on
            }
            if(isset($_SESSION['template'])){
                $template = $_SESSION['template']; // store for the Multilingual add-on
            }
            @session_destroy();
            session_start();
            if(isset($language_template)){
                $_SESSION['language_template'] = $language_template; // restore for the Multilingual add-on
            }
            if(isset($template)){
                $_SESSION['template'] = $template; // restore for the Multilingual add-on
            }
            */
            $guidestring = '';
            $display = '';
            foreach ($_GET as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $vitem) {
                        $guidestring .= '&amp;' . urlencode("$k") . '[]=' . urlencode("$vitem");
                    }
                } else {
                    $guidestring .= '&amp;' . urlencode("$k") . '=' . urlencode("$v");
                }
            }

            if (isset($_GET['action']) && $_GET['action'] == 'member_login') {
                if (isset($_POST['user_name'])) {
                    if (!$_POST['user_name'] || !$_POST['user_pass']) {
                        $error_msg .= $lang['required_field_not_filled'];
                    } else {
                        /* Spruce up username, check length */
                        $_POST['user_name'] = trim($_POST['user_name']);
                        if (strlen($_POST['user_name']) > 30) {
                            $error_msg .= $lang['username_excessive_length'];
                        }
                        /* Checks that username is in database and password is correct */
                        $md5pass = md5($_POST['user_pass']);
                        $result = $this->confirm_user($_POST['user_name'], $md5pass);
                        /* Check error codes */
                        if ($result == 1) {
                            $error_msg .= $lang['nonexistent_username'];
                        } elseif ($result == 2) {
                            $error_msg .= $lang['incorrect_password'];
                        } elseif ($result == 3) {
                            $error_msg .= $lang['inactive_user'];
                        }
                    }
                }
            }
            if ($priv_level_needed == 'Member') {
                include_once $config['basepath'] . '/include/core.inc.php';
                $page = new page_user();
                $page->load_page($config['template_path'].'/login.html');
            } else {
                include_once $config['basepath'] . '/include/core.inc.php';
                $page = new page_admin();
                $page->load_page($config['admin_template_path'].'/login.html');
            }

            if ($error_msg != '') {
                $page->page = $page->cleanup_template_block('login_error_msg', $page->page);
                $page->replace_tag('login_error_msg', $error_msg);
            } else {
                $page->page = $page->remove_template_block('login_error_msg', $page->page);
            }

            $display .= $page->return_page();

            // Run the cleanup for the forgot password table
            global $db_type;
            if ($db_type == 'mysql' || $db_type =='mysqli' || $db_type =='pdo') {
                $sql = 'DELETE FROM ' . $config['table_prefix_no_lang'] . 'forgot WHERE forgot_time < NOW() - INTERVAL 1 DAY';
            } else {
                $sql = 'DELETE FROM ' . $config['table_prefix_no_lang'] . 'forgot WHERE forgot_time < NOW() - INTERVAL \'1 DAY\'';
            }
            $recordSet = $conn->execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            return $display;
        }
    }

    public function verify_priv($priv_level_needed)
    {
        if (!isset($_SESSION['is_member'])) {
            return false;
        }
        switch ($priv_level_needed) {
            case 'Agent':
                if ($_SESSION['isAgent'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
                // case 'canEditForms':
                // if($_SESSION['editForms'] == 'yes' || $_SESSION['admin_privs'] == 'yes')
                // {
                // return TRUE;
                // }
                // break;
            case 'Admin':
                if ($_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'canViewLogs':
                if ($_SESSION['viewLogs'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'CanEditExpiration':
                if ($_SESSION['edit_expiration']  == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'editpages':
                if ($_SESSION['editpages'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'havevtours':
                if ($_SESSION['havevtours'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'havefiles':
                if ($_SESSION['havefiles'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'Member':
                if ($_SESSION['is_member'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_site_config':
                if ($_SESSION['edit_site_config'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_member_template':
                if ($_SESSION['edit_member_template'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_lead_template':
                if ($_SESSION['edit_lead_template'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_agent_template':
                if ($_SESSION['edit_agent_template'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_listing_template':
                if ($_SESSION['edit_listing_template'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'export_listings':
                if ($_SESSION['export_listings'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_all_listings':
                if ($_SESSION['edit_all_listings'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_all_leads':
                if ($_SESSION['edit_all_leads'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_all_users':
                if ($_SESSION['edit_all_users'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_property_classes':
                if ($_SESSION['edit_property_classes'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'can_access_blog_manager':
                if ($_SESSION['blog_user_type'] >1 || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'is_blog_editor':
                if ($_SESSION['blog_user_type'] ==4 || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'can_manage_addons':
                if ($_SESSION['can_manage_addons'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            default:
                return false;
                break;
        } // End switch($priv_level_needed)
        return false;
    } // End Function verify_priv()

    public function confirm_user($username, $password)
    {
        global $conn, $config, $lang, $misc;

        //$username = array($username);
        $username = $conn->qstr($username);

        /* Verify that user is in database */
        $sql = 'SELECT * 
				FROM ' . $config['table_prefix'] . 'userdb 
				WHERE  userdb_user_name='.$username.'';
        $recordSet = $conn->Execute($sql);

        if (!$recordSet || ($recordSet->RecordCount() < 1)) {
            return 1; //Indicates username failure
        }
        if ($recordSet->fields['userdb_active'] != 'yes') {
            return 3; //Indicates user is inactive
        }
        /* Retrieve password from result, strip slashes */
        $dbarray['password'] = $recordSet->fields['userdb_user_password'];

        /* Validate that password is correct */
        if ($password == $dbarray['password']) {
            return 0; //Success! Username and password confirmed
        } else {
            return 2; //Indicates password failure
        }
    }

    public function log_out($type = 'admin')
    {
        $old_id = $_SESSION['userID'];
        unset($_SESSION['username']);
        unset($_SESSION['userpassword']);
        unset($_SESSION['userID']);
        unset($_SESSION['featureListings']);
        unset($_SESSION['viewLogs']);
        unset($_SESSION['admin_privs']);
        unset($_SESSION['active']);
        unset($_SESSION['isAgent']);
        unset($_SESSION['moderator']);
        unset($_SESSION['editpages']);
        unset($_SESSION['havevtours']);
        unset($_SESSION['is_member']);
        // New Permissions with OR 2.1
        unset($_SESSION['edit_site_config']);
        unset($_SESSION['edit_member_template']);
        unset($_SESSION['edit_agent_template']);
        unset($_SESSION['edit_listing_template']);
        unset($_SESSION['export_listings']);
        unset($_SESSION['edit_all_listings']);
        unset($_SESSION['edit_all_users']);
        unset($_SESSION['edit_property_classes']);
        unset($_SESSION['edit_expiration']);
        unset($_SESSION['blog_user_type']);
        // Destroy Cookie
        setcookie('cookname', '', time() - 3600, '/');
        setcookie('cookpass', '', time() - 3600, '/');
        @session_destroy();

        global $config;

        //call the after logout plugin function
        include_once $config['basepath'] . '/include/hooks.inc.php';
        $hooks = new hooks();
        $hooks->load('after_user_logout', $old_id);
        unset($old_id);

        // Refresh the screen
        if ($type == 'admin') {
            header('Location:' . $config['baseurl'] . '/admin/');
        } else {
            header('Location:' . $config['baseurl'] . '/index.php');
        }
        die();
    }

    public function ajax_forgot_password($admin = true)
    {
        global $lang;
        $result = $this->forgot_password($admin);
        if ($result == $lang['check_your_email']) {
            return json_encode(['error'=>false,'msg'=>'<div style="font-weight: bold; text-align: center;  padding: 20px;">'.$lang['check_your_email'].'</div>']);
        } else {
            return json_encode(['error'=>true,'error_msg'=>$result]);
        }
    }

    public function forgot_password($admin = true)
    {
        $email = $_POST['email'];
        if (is_string($email)) {
            global  $config, $lang, $conn, $misc;

            $valid = $misc->validate_email($email);
            if ($valid) {
                $email = $conn->qstr($email);
                //$email = mysql_real_escape_string($email);
                // Verify the user has not tried to reset more then 3 times in 24 hours.
                $sql = 'SELECT forgot_id FROM ' . $config['table_prefix_no_lang'] . 'forgot 
						WHERE forgot_email = '.$email.' 
						AND forgot_time > NOW() - INTERVAL 1 DAY';
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                if ($recordSet->Recordcount() > 3) {
                    return $lang['to_many_password_reset_attempts'];
                }
                if ($config['demo_mode'] == 1) {
                    return $lang['password_reset_denied_demo_mode'];
                }
                $sql = 'SELECT userdb_user_name, userdb_emailaddress 
						FROM ' . $config['table_prefix'] . 'userdb 
						WHERE userdb_emailaddress='.$email.'';
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $num = $recordSet->RecordCount();
                if ($num == 1) {
                    $forgot_rand = mt_rand(100000, 999999);
                    $user_email = $recordSet->fields['userdb_emailaddress'];
                    $user_name = $recordSet->fields['userdb_user_name'];
                    $sql = 'INSERT INTO ' . $config['table_prefix_no_lang'] . "forgot (forgot_rand, forgot_email) 
							VALUES ($forgot_rand,'$user_email')";
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $misc->log_error($sql);
                    }
                    if ($admin) {
                        $forgot_link = $config['baseurl'] . '/admin/index.php?action=forgot&id=' . $forgot_rand . '&email=' . $user_email;
                    } else {
                        $forgot_link = $config['baseurl'] . '/index.php?action=forgot&id=' . $forgot_rand . '&email=' . $user_email;
                    }
                    $message = $lang['your_username'] . ' ' .$user_name . "\r\n";
                    ;
                    $message .= $lang['click_to_reset_password'] . "\r\n";
                    $message .= $forgot_link . "\r\n";
                    $message .= $lang['link_expires'] . "\r\n";
                    $misc->send_email($config['admin_name'], $config['admin_email'], $user_email, $message, $lang['forgotten_password']);

                    return '<div style="font-weight: bold; text-align: center; padding: 20px;">'.$lang['check_your_email'].'</div>';
                } else {
                    return '<font color="red">' . $lang['email_invalid_email_address'] . '</font>';
                }
            } else {
                return '<font color="red">'.$lang['email_invalid_email_address']. '</font>';
            }
        }
    }

    public function forgot_password_reset()
    {
        global $config, $lang, $conn, $misc;

        $data = '';

        if (!isset($_POST['user_pass'])) {
            if (isset($_GET['id']) || isset($_GET['email'])) {
                $id = intval($_GET['id']);
                echo $_GET['email'];
                $email = filter_var($_GET['email'], FILTER_VALIDATE_EMAIL);
                var_dump($email);
                if ($email !== false) {
                    $email = $conn->qstr($email);
                } else {
                    $email =$conn->qstr('');
                }

                //is this an agent
                $is_agent = $this->is_user_agent('', $_GET['email']);

                //set where the form posts to based on user type
                if ($is_agent) {
                    $action_var =  $config['baseurl'] . '/admin/index.php?action=forgot';
                } else {
                    $action_var =  $config['baseurl'] . '/index.php?action=forgot';
                }

                $sql = 'SELECT forgot_id FROM ' . $config['table_prefix_no_lang'] . 'forgot 
						WHERE forgot_email = '.$email." 
						AND forgot_rand = $id 
						AND forgot_time > NOW() - INTERVAL 1 DAY";
                echo $sql;
                $recordSet = $conn->execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $num = $recordSet->RecordCount();
                if ($num == 1) {
                    $data .= '<div style="text-align: center; padding: 20px;">
								<form id="pass_reset" action="'.$action_var.'" method="post">
									<input type="hidden" name="rand_id" value="' . htmlentities($_GET['id']) . '">
									<input type="hidden" name="email" value="' . htmlentities($_GET['email']) . '">
									<p>' . $lang['enter_new_password'] . ' <input type="password" name="user_pass" /></p>
									<p><input class="or_std_button" type="submit" value="' . $lang['reset_password'] . '" /></p>
								</form>
							</div>';
                } else {
                    $data .= $lang['invalid_expired_link'];
                }
            } else {
                $data .= $lang['invalid_expired_link'];
            }
        } else {
            $id = intval($_POST['rand_id']);

            $email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);

            if ($email !== false) {
                $email = $conn->qstr($email);
            } else {
                $email =$conn->qstr('');
            }

            $sql = 'SELECT forgot_id 
					FROM ' . $config['table_prefix_no_lang'] . 'forgot 
					WHERE forgot_email = '.$email." 
					AND forgot_rand = $id 
					AND forgot_time > NOW() - INTERVAL 1 DAY";
            $recordSet = $conn->execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $num = $recordSet->RecordCount();
            if ($num == 1) {
                // Delete ID from Forgot list
                $delete_id = intval($recordSet->fields['forgot_id']);
                $sql = 'DELETE FROM ' . $config['table_prefix_no_lang'] . "forgot 
						WHERE forgot_id = $delete_id";
                $recordSet = $conn->execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                // Set Password
                $md5_pass = md5($_POST['user_pass']);

                $md5_pass = $conn->qstr($md5_pass);
                //$md5_pass = mysql_real_escape_string($md5_pass);

                $sql = 'UPDATE ' . $config['table_prefix'] . "userdb 
						SET userdb_user_password = $md5_pass 
						WHERE userdb_emailaddress = ".$email.'';
                $recordSet = $conn->execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                } else {
                    $data .= '<div style="text-align: center; padding: 20px;">
								<h3>' . $lang['password_changed'] . '</h3>
								<br />
								'.$lang['login'].': <a href="' . $config['baseurl'] . '/admin/index.php">' . $config['baseurl'] . '/admin/index.php</a>
							</div>';
                }
            } else {
                $data .= $lang['invalid_expired_link'];
            }
        }
        return $data;
    }

    public function ajax_reset_password()
    {
        global $config, $lang, $conn, $misc;

        $display = '';
        $has_permission=true;

        //Check for edit all user permissions
        $security = $this->verify_priv('edit_all_users');
        if (!$security) {
            //No permission to change
            header('Content-type: application/json');
            return json_encode(['error' => '1','error_msg' => $lang['access_denied']]);
        } else {
            $userID = intval($_GET['user_id']);
            // no touching the admin if you ain't the admin yo-self
            if ($userID ===1 && $_SESSION['userID'] !==1) {
                header('Content-type: application/json');
                return json_encode(['error' => '1','error_msg' => $lang['access_denied']]);
            }

            // Generate and set password
            $newpass = $misc->generatePassword();
            $md5_pass = md5($newpass);
            $md5_pass = $conn->qstr($md5_pass);

            $sql = 'UPDATE ' . $config['table_prefix'] . 'userdb 
					SET userdb_user_password = '.$md5_pass.' 
					WHERE userdb_id = '.$userID.'';
            $recordSet = $conn->execute($sql);

            if (!$recordSet) {
                //no such user ID
                $misc->log_error($sql);
            } else {
                //booleans & form vars are a bitch sometimes 'cause $_GET always contains strings
                $mailchecked = filter_input(INPUT_GET, 'sendmail', FILTER_VALIDATE_BOOLEAN, ['flags' => FILTER_NULL_ON_FAILURE]);

                if ($mailchecked) {
                    //send account info email
                    include_once $config['basepath'] . '/include/user_manager.inc.php';
                    $usermg = new user_managment();

                    //is this an agent
                    $is_agent = $this->is_user_agent($userID);

                    if ($is_agent || $userID ===1) {
                        $usermg-> send_user_signup_email($userID, 'agent', $newpass);
                    } else {
                        $usermg-> send_user_signup_email($userID, 'member', $newpass);
                    }

                    header('Content-type: application/json');
                    return json_encode(['error' => '0','user_id' => $_GET['user_id'], 'newpass' => $newpass, 'sendmail' => true]);
                } else {
                    header('Content-type: application/json');
                    return json_encode(['error' => '0','user_id' => $_GET['user_id'], 'newpass' => $newpass, 'sendmail' => false]);
                }
            }
        }
    }

    public function is_user_agent($user_id = '', $user_email = '')
    {
        global $config, $conn, $misc;

        //returns true if the given user_id or email addy is an Agent
        $is_agent = false;

        if ($user_id != '') {
            $user_id = intval($user_id);
            $where_clause = 'userdb_id = '.$user_id.'';
        } else {
            $user_email = $conn->qstr($user_email);
            $where_clause = 'userdb_emailaddress = '.$user_email;
        }

        $sql = 'SELECT userdb_is_agent, userdb_is_admin 
				FROM ' .$config['table_prefix']. 'userdb 
				WHERE ' .$where_clause.'';
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
        $userdb_is_agent = $recordSet->fields['userdb_is_agent'];
        $userdb_is_admin = $recordSet->fields['userdb_is_admin'];
        if ($userdb_is_agent =='yes') {
            $is_agent = true;
        } else {
            if ($userdb_is_admin =='yes') {
                $is_agent = true;
            }
        }

        return $is_agent;
    }
} //End class login
