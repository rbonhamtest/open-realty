<?php

class menu_editor
{
    public function ajax_get_menus()
    {
        global $api;
        $api_result = $api->load_local_api('menu__metadata', []);
        return json_encode($api_result);
    }

    public function ajax_get_menu_items($menu_id)
    {
        global $api;
        $api_result = $api->load_local_api('menu__read', ['menu_id'=>$menu_id]);
        return json_encode($api_result);
    }

    public function ajax_get_menu_item_details($item_id)
    {
        global $api;
        $api_result = $api->load_local_api('menu__item_details', ['item_id'=>$item_id]);
        return json_encode($api_result);
    }

    public function ajax_set_menu_order($menu_id, $menu_items)
    {
        global $api;
        $api_result = $api->load_local_api('menu__set_menu_order', ['menu_id'=>$menu_id,'menu_items'=>$menu_items]);
        return json_encode($api_result);
    }

    public function ajax_save_menu_item($item_id, $item_name, $item_type, $item_value, $item_target, $item_class, $visible_guest, $visible_member, $visible_agent, $visible_admin)
    {
        global $api;
        $api_result = $api->load_local_api('menu__save_menu_item', ['item_id'=>$item_id,'item_name'=>$item_name,'item_type'=>$item_type,'item_value'=>$item_value,
                'item_target'=>$item_target,'item_class'=>$item_class,'visible_guest'=>$visible_guest,'visible_member'=>$visible_member,'visible_agent'=>$visible_agent,'visible_admin'=>$visible_admin, ]);
        return json_encode($api_result);
    }

    public function ajax_add_menu_item($menu_id, $item_name, $parent_id)
    {
        global $api;
        $api_result = $api->load_local_api('menu__add_menu_item', ['menu_id'=>$menu_id,'item_name'=>$item_name,'parent_id'=>$parent_id]);
        return json_encode($api_result);
    }

    public function ajax_delete_menu_item($item_id)
    {
        global $api;
        $api_result = $api->load_local_api('menu__delete_menu_item', ['item_id'=>$item_id]);
        return json_encode($api_result);
    }

    public function ajax_delete_menu($menu_id)
    {
        global $api;
        $api_result = $api->load_local_api('menu__delete', ['menu_id'=>$menu_id]);
        return json_encode($api_result);
    }

    public function ajax_create_menu($menu_name)
    {
        global $api;
        $api_result = $api->load_local_api('menu__create', ['menu_name'=>$menu_name]);
        return json_encode($api_result);
    }

    public function render_menu($menu_id)
    {
        global $config,$jscript,$lang,$api;
        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $api_result = $api->load_local_api('menu__read', ['menu_id'=>$menu_id]);
        if ($api_result['error']) {
            return $api_result['error_msg'];
        }
        $display = '';
        //$display .= '<pre>'.print_r($api_result['menu'],TRUE).'</pre>';
        $display .= '<ul class="or_menu" id="or_menu_'.$menu_id.'">'.$this->build_menu_html($api_result['menu'], 0).'</ul>';
        return $display;
    }

    private function build_menu_html($menuarray, $parent_id)
    {
        global $config;
        include_once $config['basepath'] . '/include/core.inc.php';
        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $page = new page_user();
        $menu = '';
        foreach ($menuarray[$parent_id] as $order => $menu_item) {
            $item_id = $menu_item['item_id'];
            $item_name = $menu_item['item_name'];
            $item_type = $menu_item['item_type'];
            $item_target = $menu_item['item_target'];
            $item_value = $menu_item['item_value'];
            $item_class = $menu_item['item_class'];
            $item_types = [1=>'action',2=>'page',3=>'custom',4=>'divider','5'=>'block',6=>'blog'];
            //Get Users permission Levels
            $is_agent = $login_status = $login->verify_priv('Agent');
            $is_admin = $login_status = $login->verify_priv('Admin');
            $is_member = $login_status = $login->verify_priv('Member');
            //Get Items Visible States
            $visible_guest = $menu_item['visible_guest'];
            $visible_member = $menu_item['visible_member'];
            $visible_agent = $menu_item['visible_agent'];
            $visible_admin = $menu_item['visible_admin'];
            $visible = false;
            if (!$is_member && !$is_agent && ! $is_admin && $visible_guest) {
                $visible = true;
            } elseif ($is_member && !$is_agent && ! $is_admin && $visible_member) {
                $visible = true;
            } elseif ($is_agent && ! $is_admin && $visible_agent) {
                $visible = true;
            } elseif ($is_admin && $visible_admin) {
                $visible = true;
            }
            if (!$visible) {
                continue;
            }
            //Build URL..
            $url = '';
            switch ($item_type) {
                case 1:
                    //Action
                    if ($item_value!='') {
                        $url = '{'.$item_value.'}';
                    } else {
                        $url = '#';
                    }
                    break;
                case 2:
                    //Page
                    if ($item_value>0) {
                        $url = '{page_link_'.intval($item_value).'}';
                    } else {
                        $url = '#';
                    }
                    break;
                case 6:
                        //Page
                    if ($item_value>0) {
                        $url = '{blog_link_'.intval($item_value).'}';
                    } else {
                        $url = '#';
                    }
                    break;
                case 3:
                    //Custom
                    $url = $item_value;
                    break;
                case 4:
                    //Divider
                    $item_type = $menu_item['item_type'];
                    if (isset($menuarray[$item_id])) {
                        $menu .= '<li class="or_menu_item or_menu_parent or_menu_item_type_'.$item_types[$item_type].'" id="or_menu_item_'.$item_id.'">'.$item_name.'<ul>';
                        $menu .= $this->build_menu_html($menuarray, $item_id);
                        $menu .= '</ul></li>';
                    } else {
                        $menu .='<li class="or_menu_item or_menu_item_type_'.$item_types[$item_type].'" id="or_menu_item_'.$item_id.'">'.$item_name.'</li>';
                    }
                    continue(2);
                case 5:
                    switch ($item_value) {
                        case 'blog_recent_comments_block':
                            $menu .= '{blog_recent_comments_block}
								<li class="or_menu_item or_menu_item_type_'.$item_types[$item_type].'"><a href="{blog_recent_comments_url}" class="'.$item_class.'" title="{blog_recent_comments_title}">{blog_recent_comments_title}</a></li>
								{/blog_recent_comments_block}';
                            break;
                        case 'blog_recent_post_block':
                            $menu .= '{blog_recent_post_block}
							<li class="or_menu_item or_menu_item_type_'.$item_types[$item_type].'"><a href="{blog_recent_post_url}" class="'.$item_class.'" title="{blog_recent_post_title}">{blog_recent_post_title}</a></li>
							{/blog_recent_post_block}';
                            break;
                        case 'blog_archive_link_block':
                            $menu .= '{blog_archive_link_block}
								<li class="or_menu_item or_menu_item_type_'.$item_types[$item_type].'"><a href="{blog_archive_url}" class="'.$item_class.'" title="{blog_archive_title}">{blog_archive_title}</a></li>
							{/blog_archive_link_block}';
                            break;
                        case 'blog_category_link_block':
                            $menu .= '{blog_category_link_block}
								<li class="or_menu_item or_menu_item_type_'.$item_types[$item_type].'"><a href="{blog_cat_url}" class="'.$item_class.'" title="{blog_cat_title}">{blog_cat_title}</a></li>
							{/blog_category_link_block}';
                            break;
                        case 'pclass_searchlinks_block':
                            global $api;
                            $class_metadata = $api->load_local_api('pclass__metadata', []);
                            $keys = array_keys($class_metadata['metadata']);
                            if (is_array($keys)) {
                                foreach ($keys as $class_id) {
                                    $menu.= '<li class="or_menu_item or_menu_item_type_'.$item_types[$item_type].'"><a href="'.$config['baseurl'].'/index.php?action=search_step_2&amp;pclass%5B%5D='.$class_id.'" class="'.$item_class.'" title="' . $class_metadata['metadata'][$class_id]['name'].'">'.$class_metadata['metadata'][$class_id]['name'].'</a></li>'.BR;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                    continue(2);
            }
            $item_type = $menu_item['item_type'];
            if (isset($menuarray[$item_id])) {
                $menu .= '<li class="or_menu_item or_menu_parent or_menu_item_type_'.$item_types[$item_type].'" id="or_menu_item_'.$item_id.'"><a href="'.$url.'" title="'.$item_name.'" class="'.$item_class.'" target="'.$item_target.'">'.$item_name.'</a><ul>';
                $menu .= $this->build_menu_html($menuarray, $item_id);
                $menu .= '</ul></li>';
            } else {
                $menu .='<li class="or_menu_item or_menu_item_type_'.$item_types[$item_type].'" id="or_menu_item_'.$item_id.'"><a href="'.$url.'" title="'.$item_name.'" class="'.$item_class.'"  target="'.$item_target.'">'.$item_name.'</a></li>';
            }
        }
        $page->page = $menu;
        $page->replace_urls();
        $page->replace_blog_template_tags();
        $page->auto_replace_tags();
        $page->replace_lang_template_tags();
        $menu = $page->return_page();

        return $menu;
    }

    public function show_editor()
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('editpages');
        $display = '';
        if ($security === true) {
            global $misc, $jscript;

            $jscript .= '<script type="text/javascript">
				
				$(document).ready(function(){
					$("#tabs").uitabs();
				});
				
			</script>';
            $jscript .= '<script type="text/javascript" src="'.$config['baseurl'].'/include/class/jquery/jquery-migrate-3.0.0.min.js"></script>
						<script type="text/javascript" src="'.$config['baseurl'].'/include/class/jquery/jquery_plugins/jstree/jquery.jstree.js"></script>
						
			<script type="text/javascript">
				$(document).ready(function(){
				//Load the List of Menus on Page Load
				$.get("ajax.php?action=ajax_get_menus", function(data) {
					if(data.error){
					alert("{lang_emenu_error}");
					}
					else {
					jQuery.each(data.menus , function(index, value){
						//$("<option value=\'"+index+"\'>"+value+"</option>").appendTo("#menu_selection");
						$("#menu_selection").append("<option value=\'"+index+"\'>("+index+") "+value+"</option>");
					});
					}
				}, "json");
				
				$("#menu_editor_new_menu").click(function(){
					$("#message_text").html(" <form id=\'add_menu_form\'>{lang_emenu_name} <input type=\'text\' id=\'add_menu_name\' /><br /><br /> <br />\
												<div style=\'text-align: center;\'> <a href=\'#\' class=\'or_std_button\' id=\'add_menu_save\'>{lang_emenu_add_menu}</a> </div> \
												</form>");
					$("#dialog-message").dialog({
						title: "{lang_emenu_add_menu}",
						modal: true,
						width: "auto",
						buttons: null,
						position: {
							my: "center top+80",
							at: "top",
							of: window,
							collision: "none"
						},
						open: function(){
							$("#add_menu_name").focus();
							$("#add_menu_save").click(function(){
								if($("#add_menu_name").val() == ""){alert("{lang_emenu_error_enter_menu_name}");return false;}
								$.post("ajax.php?action=ajax_create_menu", {
									"menu_name": $("#add_menu_name").val()
								},
								function(data){
									if(!data.error){
										$("#dialog-message").dialog( "close" );
										status_msg("{lang_emenu_added}");
										$("#menu_selection").append("<option value=\'"+data.menu_id+"\'>"+data.menu_name+"</option>");
										$("#menu_selection").val(data.menu_id).change();
									}
								}, "json");
							});
							$("#add_menu_form").submit(function(){
								$("#add_menu_save").click();
								return false;
							});
						}	
					});
				});
				$("#menu_editor_delete_menu").click(function(){
					var menu_id = $("#menu_selection  option:selected").val();
					var menu_name = $("#menu_selection  option:selected").text();
					if(menu_id != ""){
						var confirmdelete =confirm("{lang_emenu_delete_menu} "+menu_name);
						if (confirmdelete==true){
							$.post("ajax.php?action=ajax_delete_menu", {
								"menu_id": menu_id
							},function(data){
								if(!data.error){
									status_msg("{lang_emenu_deleted}");
									$("#menu_selection option[value=\'"+menu_id+"\']").remove();
									$("#menu_selection").val("").change();
								}
								}, "json");
						}
					}
				});
				//When a menu is selected get a list of the menu items.
				$("#menu_selection").change(function(){

					var menu_id = $(this).val();
					var menu_name = $(this).find("option:selected").text().replace(/\(\d+\)/,\'\');
					$("#menu_editor_detail_container").html("");
					if(menu_id == ""){
						$("#cur_menu_name").text("");
						$("#menu_editor_jstree_container").html("<ul id=\'p_0\'><li><a href=\'#\'></a><ul id=\'t_0\'></ul></li></ul");
					}else{
						$("#cur_menu_name").html("&#123;render_menu_"+menu_id+"&#125;");
						$.get("ajax.php?action=ajax_get_menu_items&menu_id="+menu_id, function(data) {
							if(data.error){
							alert("{lang_emenu_error}");
							}else{
								$("#menu_editor_jstree_container").html(\'<ul id="p_0"><li rel="root"><a href="#">\'+menu_name+\'</a><ul id="t_0"></ul></li></ul>\');

								jQuery.each(data.menu , function(parent_id, itemarray){
								jQuery.each(itemarray , function(key, subitem_array){
									subitem_id = subitem_array.item_id;
									subitem_name = subitem_array.item_name;
									subitem_type = subitem_array.item_type;
									var irel = "";
									switch(subitem_type){
										case "5":
											irel = "rel=\'block\'";
											break;
										case "4":
											irel = "rel=\'divider\'";
											break;

									}
									var iclass = "";
									if(subitem_array.visible_guest != 1 && subitem_array.visible_member != 1 && subitem_array.visible_agent != 1 && subitem_array.visible_admin != 1){
										iclass = "menu_item_invisible";
									}
									if(subitem_id in data.menu){
										//This Item as Sub Items.
											$("#t_"+parent_id).append(\'<li id="i_\'+subitem_id+\'" \'+irel+\' class="\'+iclass+\'"><a href="#">\'+subitem_name+\'</a><ul id="t_\'+subitem_id+\'"></ul></li>\');
									}else{
										//This Item has no children.
											$("#t_"+parent_id).append(\'<li id="i_\'+subitem_id+\'" \'+irel+\' class="\'+iclass+\'"><a href="#">\'+subitem_name+\'</a></li>\');
									}
								});
							});
							//debugger;
							$("#menu_editor_jstree_container").jstree({
								"contextmenu" : {
									"items" : function(node){
										return {
											"new":{
												"label": "{lang_emenu_add_item}",
												"action": function(){

												$("#message_text").html("<form id=\'add_item_form\'>Menu Item Name: <input type=\'text\' id=\'add_item_name\' /><br /><br />  <br /> \
																		<div style=\'text-align: center;\'> <a href=\'#\' class=\'or_std_button\' id=\'add_item_save\'>{lang_emenu_add_item}</a></div> \
																		</form>");
												$("#dialog-message").dialog({
													title: "{lang_emenu_add_menu}",
													modal: true,
													width: "auto",
													buttons: null,
													position: {
														my: "center top+80",
														at: "top",
														of: window,
														collision: "none"
													},
													open: function(){
														$("#add_item_name").focus();
															$("#add_item_save").click(function(){
																if($("#add_item_name").val() == ""){alert("{lang_emenu_error_enter_item_name}");return false;}
																$.post("ajax.php?action=ajax_add_menu_item", {
																	"menu_id": menu_id,
																	"item_name": $("#add_item_name").val(),
																	"parent_id": function(){
																		if(node.attr("id") == undefined) {
																			return 0;
																		}else{
																			return node.attr("id").substring(2);
																		}
																	}
																},
																function(data){
																	if(!data.error){
																		$("#dialog-message").dialog( "close" );
																		status_msg("{lang_emenu_item_added}");
																		$("#menu_editor_jstree_container").jstree("create_node",node,"last",{
																			"attr": {
																				"id" : "i_"+data.item_id,
																				"rel":"default"
																			},
																			"data" : $("#add_item_name").val()
																		});
																		$("#menu_editor_jstree_container").jstree("select_node", "#i_"+data.item_id);
																		$("#i_"+data.item_id).parents(".jstree-closed").each(function () {
																				$("#menu_editor_jstree_container").jstree("open_node", this, false, true);
																		});
																	}
																	}, "json");
															});
															$("#add_item_form").submit(function(){
																$("#add_item_save").click();
																return false;
															});
														}	
													});

												}
											},
											"delete": {
												"label": "{lang_emenu_delete_item}",
												"action": function(){
													if (confirm("{lang_emenu_delete_item_confirm}")) {
														$.post("ajax.php?action=ajax_delete_menu_item", {
															"item_id": function(){
																if(node.attr("id") == undefined) {
																	return 0;
																} else {
																	return node.attr("id").substring(2);
																}
															}
														},function(data){
															if(!data.error){
																status_msg("{lang_emenu_item_deleted}");
																$("#menu_editor_jstree_container").jstree("delete_node",node);
															}
														}, "json");
													}

												}
											}
										}
									}
								},
								"themes" : {
									"theme" : "classic"
								},
								"core" : {
									"animation" : 150
								},
								"types" : {
									"valid_children" : [ "root" ],
									"types" : {
										"root" : {
											"valid_children" : [ "default","divider", "block" ],
											"icon" : {
												"image" : "'.$config['baseurl'].'/images/menu_editor_sprite.png"
											}
										},
										"block" : {
											"valid_children" : ["none"],
											"max_children": 0,
											"icon" : {
												"image" : "'.$config['baseurl'].'/images/menu_editor_sprite.png",
												"position": "0px -48px"
											}
										},
										"divider" : {
											"valid_children" : [ "divider", "default","block" ],
											"icon" : {
												"image" : "'.$config['baseurl'].'/images/menu_editor_sprite.png",
												"position": "0px -16px"
											}
										},
										"default" : {
											"valid_children" : [ "divider", "default","block" ],
											"icon" : {
												"image" : "'.$config['baseurl'].'/images/menu_editor_sprite.png",
												"position": "0px -32px"
											}
										}
									}
								},
								"plugins" : [ "themes", "html_data", "dnd", "ui","crrm","contextmenu","types" ]
							}).bind("select_node.jstree", function (event, data) {
								item_obj = data.rslt.obj[0];
								item_id = data.rslt.obj[0].id.substring(2);
								if(item_id == ""){
									$("#menu_editor_detail_container").html("");
								}else{
									$.get("ajax.php?action=ajax_get_menu_item_details&item_id="+item_id, function(data) {
										if(data.error){
										alert("{lang_emenu_error}");
										}else{
										var visible_guest = "";
										var visible_member = "";
										var visible_agent = "";
										var visible_admin = "";

										if(data.menu_item.visible_guest == "1"){
											visible_guest = " checked=\'checked\' ";
										}
										if(data.menu_item.visible_member == "1"){
											visible_member = " checked=\'checked\' ";
										}
										if(data.menu_item.visible_agent == "1"){
											visible_agent = " checked=\'checked\' ";
										}
										if(data.menu_item.visible_admin == "1"){
											visible_admin = " checked=\'checked\' ";
										}

										$("#menu_editor_detail_container").html("<form id=\'item_detail_form\'> \
											<fieldset><legend>{lang_emenu_item_name}</legend> \
											<input type=\'text\' name=\'item_name\' id=\'item_name\' value=\'\' /></fieldset> \
											<fieldset><legend>{lang_emenu_viewable_by}</legend> \
											{lang_emenu_viewable_guest} <input type=\'checkbox\' name=\'visible_guest\' id=\'visible_guest\' "+visible_guest+" value=\'1\'> \
											{lang_emenu_viewable_members} <input type=\'checkbox\' name=\'visible_member\' id=\'visible_member\' "+visible_member+" value=\'1\'> \
											{lang_emenu_viewable_agents} <input type=\'checkbox\' name=\'visible_agent\' id=\'visible_agent\' "+visible_agent+" value=\'1\'> \
											{lang_emenu_viewable_admins} <input type=\'checkbox\' name=\'visible_admin\' id=\'visible_admin\' "+visible_admin+" value=\'1\'> \
											</fieldset> \
											<fieldset id=\'fs_menu_item_type\'><legend>{lang_emenu_item_type} </legend> \
											<select name=\'item_type\' id=\'item_type\'> \
											<option value=\'\'>{lang_emenu_select_item_type}</a> \
											<option value=\'1\'>{lang_emenu_type_action}</a> \
											<option value=\'2\'>{lang_emenu_type_page}</a> \
											<option value=\'6\'>{lang_emenu_type_blog}</a> \
											<option value=\'5\'>{lang_emenu_type_block_elements}</a> \
											<option value=\'3\'>{lang_emenu_type_customurl}</a> \
											<option value=\'4\'>{lang_emenu_type_divider}</a> \
											</select> \
											</fieldset> \
										</form>");
										$("#item_name").val(data.menu_item.item_name);
										$("#cur_menu_item_name").text(data.menu_item.item_name+" ("+item_id+")");
										$("#item_type").val(data.menu_item.item_type);

										var item_type = data.menu_item.item_type;
										var item_target = data.menu_item.item_target;
										var item_class = data.menu_item.item_class;
										if(item_target == "_blank"){
											var target_blank_select = " selected=\'selected\' ";
										}else{
											var target_blank_select ="";
										}
										if(item_target == "_self"){
											var target_self_select = " selected=\'selected\' ";
										}else{
											var target_self_select ="";
										}
										if(item_target == "_parent"){
											var target_parent_select = " selected=\'selected\' ";
										}else{
											var target_parent_select ="";
										}
										if(item_target == "_top"){
											var target_top_select = " selected=\'selected\' ";
										}else{
											var target_top_select ="";
										}
										switch(item_type){
											case "1":
												//Build a drop down of actions
												$("#fs_menu_item_type").append("<div id=\'item_value_container\'>{lang_emenu_internal_action} <br /> <select name=\'item_value\' id=\'item_value\'></select><br />\
														{lang_emenu_target}<br />\
														<select name=\'item_target\' id=\'item_target\'>\
														<option value=\'_self\' "+target_self_select+">_self</option>\
														<option value=\'_blank\' "+target_blank_select+">_blank</option>\
														<option value=\'_parent\' "+target_parent_select+">_parent</option>\
														<option value=\'_top\' "+target_top_select+">_top</option>\
														</select><br />\
														{lang_emenu_custom_css} <br /> <input type=\'text\' name=\'item_class\' id=\'item_class\' value=\'"+item_class+"\' /> \
														</div>");
												$("#item_value").append("<option value=\'\'>{lang_emenu_action_select}</option>");
												if(data.menu_item.item_value == "url_view_agents"){
													$("#item_value").append("<option value=\'url_view_agents\' selected=\'selected\'>{lang_menu_view_agents}</option>");
												}else{
													$("#item_value").append("<option value=\'url_view_agents\'>{lang_menu_view_agents}</option>");
												}
												if(data.menu_item.item_value == "url_search"){
													$("#item_value").append("<option value=\'url_search\' selected=\'selected\'>{lang_menu_search_listings}</option>");
												}else{
													$("#item_value").append("<option value=\'url_search\'>{lang_menu_search_listings}</option>");
												}
												if(data.menu_item.item_value == "url_blog"){
													$("#item_value").append("<option value=\'url_blog\' selected=\'selected\'>{lang_menu_blog}</option>");
												}else{
													$("#item_value").append("<option value=\'url_blog\'>{lang_menu_blog}</option>");
												}
												if(data.menu_item.item_value == "url_index"){
													$("#item_value").append("<option value=\'url_index\' selected=\'selected\'>{lang_menu_home}</option>");
												}else{
													$("#item_value").append("<option value=\'url_index\'>{lang_menu_home}</option>");
												}
												if(data.menu_item.item_value == "url_search_results"){
													$("#item_value").append("<option value=\'url_search_results\' selected=\'selected\'>{lang_menu_user_browse_listings}</option>");
												}else{
													$("#item_value").append("<option value=\'url_search_results\'>{lang_menu_user_browse_listings}</option>");
												}
												if(data.menu_item.item_value == "url_logout"){
													$("#item_value").append("<option value=\'url_logout\' selected=\'selected\'>{lang_menu_logout}</option>");
												}else{
													$("#item_value").append("<option value=\'url_logout\'>{lang_menu_logout}</option>");
												}
												if(data.menu_item.item_value == "url_member_signup"){
													$("#item_value").append("<option value=\'url_member_signup\' selected=\'selected\'>{lang_menu_member_signup}</option>");
												}else{
													$("#item_value").append("<option value=\'url_member_signup\'>{lang_menu_member_signup}</option>");
												}
												if(data.menu_item.item_value == "url_agent_signup"){
													$("#item_value").append("<option value=\'url_agent_signup\' selected=\'selected\'>{lang_menu_agent_signup}</option>");
												}else{
													$("#item_value").append("<option value=\'url_agent_signup\'>{lang_menu_agent_signup}</option>");
												}
												if(data.menu_item.item_value == "url_member_login"){
													$("#item_value").append("<option value=\'url_member_login\' selected=\'selected\'>{lang_menu_member_login}</option>");
												}else{
													$("#item_value").append("<option value=\'url_member_login\'>{lang_menu_member_login}</option>");
												}
												if(data.menu_item.item_value == "url_agent_login"){
													$("#item_value").append("<option value=\'url_agent_login\' selected=\'selected\'>{lang_menu_agent_login}</option>");
												}else{
													$("#item_value").append("<option value=\'url_agent_login\'>{lang_menu_agent_login}</option>");
												}
												if(data.menu_item.item_value == "url_view_favorites"){
													$("#item_value").append("<option value=\'url_view_favorites\' selected=\'selected\'>{lang_menu_url_view_favorites}</option>");
												}else{
													$("#item_value").append("<option value=\'url_view_favorites\'>{lang_menu_url_view_favorites}</option>");
												}
												if(data.menu_item.item_value == "url_view_calculator"){
													$("#item_value").append("<option value=\'url_view_calculator\' selected=\'selected\'>{lang_menu_calculators}</option>");
												}else{
													$("#item_value").append("<option value=\'url_view_calculator\'>{lang_menu_calculators}</option>");
												}
												if(data.menu_item.item_value == "url_view_saved_searches"){
													$("#item_value").append("<option value=\'url_view_saved_searches\' selected=\'selected\'>{lang_menu_saved_searches}</option>");
												}else{
													$("#item_value").append("<option value=\'url_view_saved_searches\'>{lang_menu_saved_searches}</option>");
												}
												if(data.menu_item.item_value == "url_edit_profile"){
													$("#item_value").append("<option value=\'url_edit_profile\' selected=\'selected\'>{lang_menu_edit_profile}</option>");
												}else{
													$("#item_value").append("<option value=\'url_edit_profile\'>{lang_menu_edit_profile}</option>");
												}

												if(data.menu_item.item_value == "rss_lastmodified"){
													$("#item_value").append("<option value=\'rss_lastmodified\' selected=\'selected\'>{lang_menu_rss_lastmodified}</option>");
												}else{
													$("#item_value").append("<option value=\'rss_lastmodified\'>{lang_menu_rss_lastmodified}</option>");
												}
												if(data.menu_item.item_value == "rss_latestlisting"){
													$("#item_value").append("<option value=\'rss_latestlisting\' selected=\'selected\'>{lang_menu_rss_latestlisting}</option>");
												}else{
													$("#item_value").append("<option value=\'rss_latestlisting\'>{lang_menu_rss_latestlisting}</option>");
												}
												if(data.menu_item.item_value == "rss_featured"){
													$("#item_value").append("<option value=\'rss_featured\' selected=\'selected\'>{lang_menu_rss_featured}</option>");
												}else{
													$("#item_value").append("<option value=\'rss_featured\'>{lang_menu_rss_featured}</option>");
												}
												if(data.menu_item.item_value == "rss_blog_posts"){
													$("#item_value").append("<option value=\'rss_blog_posts\' selected=\'selected\'>{lang_menu_rss_blog_posts}</option>");
												}else{
													$("#item_value").append("<option value=\'rss_blog_posts\'>{lang_menu_rss_blog_posts}</option>");
												}
												if(data.menu_item.item_value == "rss_blog_comments"){
													$("#item_value").append("<option value=\'rss_blog_comments\' selected=\'selected\'>{lang_menu_rss_blog_comments}</option>");
												}else{
													$("#item_value").append("<option value=\'rss_blog_comments\'>{lang_menu_rss_blog_comments}</option>");
												}

												break;
											case "2":
												//Build a drop down of wysiwyg pages
													jQuery.ajax({
														url: "ajax.php?action=ajax_get_pages",
														success: function(pdata) {
															if(pdata.error){
																alert("{lang_emenu_error}");
															}else{
																$("#fs_menu_item_type").append("<div id=\'item_value_container\'><span>{lang_emenu_page_editor} </span><br /><select name=\'item_value\' id=\'item_value\'></select><br />\
																		{lang_emenu_target} <br />\
																		<select name=\'item_target\' id=\'item_target\'>\
																		<option value=\'_self\' "+target_self_select+">_self</option>\
																		<option value=\'_blank\' "+target_blank_select+">_blank</option>\
																		<option value=\'_parent\' "+target_parent_select+">_parent</option>\
																		<option value=\'_top\' "+target_top_select+">_top</option>\
																		</select><br />\
																		{lang_emenu_custom_css} <br /> <input type=\'text\' name=\'item_class\' id=\'item_class\' value=\'"+item_class+"\' /> \
																		</div>");
																$("#item_value").append("<option value=\'\'>{lang_emenu_select_page}</option>");
																jQuery.each(pdata.pages , function(page_id, page_name){
																	if(page_id == data.menu_item.item_value){
																		$("#item_value").append("<option value=\'"+page_id+"\' selected=\'selected\'>"+page_name+"</option>");
																	}else{
																		$("#item_value").append("<option value=\'"+page_id+"\'>"+page_name+"</option>");
																	}
																});
															}
														},
														async: false,
														dataType: "json"
													});
												break;
											case "6":
												//Build a drop down of blogs pages
													jQuery.ajax({
														url: "ajax.php?action=ajax_get_blogs",
														success: function(pdata) {
															if(pdata.error){
																alert("{lang_emenu_error}");
															}else{
																$("#fs_menu_item_type").append("<div id=\'item_value_container\'><span>{lang_emenu_blog_article} </span><br /><select name=\'item_value\' id=\'item_value\'></select><br />\
																		{lang_emenu_target}<br />\
																		<select name=\'item_target\' id=\'item_target\'>\
																		<option value=\'_self\' "+target_self_select+">_self</option>\
																		<option value=\'_blank\' "+target_blank_select+">_blank</option>\
																		<option value=\'_parent\' "+target_parent_select+">_parent</option>\
																		<option value=\'_top\' "+target_top_select+">_top</option>\
																		</select><br />\
																		{lang_emenu_custom_css} <br /> <input type=\'text\' name=\'item_class\' id=\'item_class\' value=\'"+item_class+"\' /> \
																		</div>");
																$("#item_value").append("<option value=\'\'>{lang_emenu_select_page}</option>");
																jQuery.each(pdata.blogs , function(blog_id, blog_name){
																	if(blog_id == data.menu_item.item_value){
																		$("#item_value").append("<option value=\'"+blog_id+"\' selected=\'selected\'>"+blog_name+"</option>");
																	}else{
																		$("#item_value").append("<option value=\'"+blog_id+"\'>"+blog_name+"</option>");
																	}
																});
															}
														},
														async: false,
														dataType: "json"
													});
												break;
											case "3":
												//Build a text box for custom url entry.
												$("#fs_menu_item_type").append("<div id=\'item_value_container\'>{lang_emenu_type_customurl} <br /><input type=\'text\' name=\'item_value\' id=\'item_value\' value=\'"+data.menu_item.item_value+"\' /><br />\
														{lang_emenu_target} <br />\
														<select name=\'item_target\' id=\'item_target\'>\
														<option value=\'_self\' "+target_self_select+">_self</option>\
														<option value=\'_blank\' "+target_blank_select+">_blank</option>\
														<option value=\'_parent\' "+target_parent_select+">_parent</option>\
														<option value=\'_top\' "+target_top_select+">_top</option>\
														</select><br />\
														{lang_emenu_custom_css} <br /> <input type=\'text\' name=\'item_class\' id=\'item_class\' value=\'"+item_class+"\' /> \
														</div>");
												$("#item_value").focus();

												break;
											case "4":
												//Do nothing this is just a divider
												$("#fs_menu_item_type").append("<div id=\'item_value_container\'><input type=\'hidden\' name=\'item_value\' id=\'item_value\' value=\'\' />\
													<input type=\'hidden\' name=\'item_target\' id=\'item_target\' value=\'_self\' /><br />\
													<input type=\'hidden\' name=\'item_class\' id=\'item_class\' value=\'\' /> \
													</div>");
												break;
											case "5":
												//Build a drop down of Block Items
												$("#fs_menu_item_type").append("<div id=\'item_value_container\'>{lang_emenu_block_element} <br /> <select name=\'item_value\' id=\'item_value\'></select> <br />\
														{lang_emenu_target} <br />\
														<select name=\'item_target\' id=\'item_target\'>\
														<option value=\'_self\' "+target_self_select+">_self</option>\
														<option value=\'_blank\' "+target_blank_select+">_blank</option>\
														<option value=\'_parent\' "+target_parent_select+">_parent</option>\
														<option value=\'_top\' "+target_top_select+">_top</option>\
														</select><br />\
														{lang_emenu_custom_css}<br /> <input type=\'text\' name=\'item_class\' id=\'item_class\' value=\'"+item_class+"\' /> \
														</div>");
												$("#item_value").append("<option value=\'\'>{lang_emenu_select_block_element}</option>");
												if(data.menu_item.item_value == "blog_recent_comments_block"){
													$("#item_value").append("<option value=\'blog_recent_comments_block\' selected=\'selected\'>{lang_menu_blog_recent_comments_block}</option>");
												}else{
													$("#item_value").append("<option value=\'blog_recent_comments_block\'>{lang_menu_blog_recent_comments_block}</option>");
												}
												if(data.menu_item.item_value == "blog_recent_post_block"){
													$("#item_value").append("<option value=\'blog_recent_post_block\' selected=\'selected\'>{lang_menu_blog_recent_post_block}</option>");
												}else{
													$("#item_value").append("<option value=\'blog_recent_post_block\'>{lang_menu_blog_recent_post_block}</option>");
												}
												if(data.menu_item.item_value == "blog_archive_link_block"){
													$("#item_value").append("<option value=\'blog_archive_link_block\' selected=\'selected\'>{lang_menu_blog_archive_link_block}</option>");
												}else{
													$("#item_value").append("<option value=\'blog_archive_link_block\'>{lang_menu_blog_archive_link_block}</option>");
												}
												if(data.menu_item.item_value == "blog_category_link_block"){
													$("#item_value").append("<option value=\'blog_category_link_block\' selected=\'selected\'>{lang_menu_blog_category_link_block}</option>");
												}else{
													$("#item_value").append("<option value=\'blog_category_link_block\'>{lang_menu_blog_category_link_block}</option>");
												}
												//pclass_names_searchlinks
												if(data.menu_item.item_value == "pclass_searchlinks_block"){
													$("#item_value").append("<option value=\'pclass_searchlinks_block\' selected=\'selected\'>{lang_menu_pclass_searchlinks_block}</option>");
												}else{
													$("#item_value").append("<option value=\'pclass_searchlinks_block\'>{lang_menu_pclass_searchlinks_block}</option>");
												}
												break;
										}
										$("#item_type").change(function(){
											$("#item_value_container").remove();
											switch($(this).val()){
												case "1":
													//Build a drop down of actions
													$("#item_type").after("<div id=\'item_value_container\'>{lang_emenu_or_action} <br /> \
															<select name=\'item_value\' id=\'item_value\'></select><br />\
															{lang_emenu_target} <br />\
															<select name=\'item_target\' id=\'item_target\'>\
															<option value=\'_self\'>_self</option>\
															<option value=\'_blank\'>_blank</option>\
															<option value=\'_parent\'>_parent</option>\
															<option value=\'_top\'>_top</option>\
															</select><br />\
															{lang_emenu_custom_css} <br /> <input type=\'text\' name=\'item_class\' id=\'item_class\' value=\'\' /> \
															</div>");
													$("#item_value").append("<option value=\'\'>{lang_emenu_action_select}</option>");
													$("#item_value").append("<option value=\'url_view_agents\'>{lang_menu_view_agents}</option>");
													$("#item_value").append("<option value=\'url_search\'>{lang_menu_search_listings}</option>");
													$("#item_value").append("<option value=\'url_blog\'>{lang_menu_blog}</option>");
													$("#item_value").append("<option value=\'url_index\'>{lang_menu_home}</option>");
													$("#item_value").append("<option value=\'url_search_results\'>{lang_menu_user_browse_listings}</option>");
													$("#item_value").append("<option value=\'url_logout\'>{lang_menu_logout}</option>");
													$("#item_value").append("<option value=\'url_member_signup\'>{lang_menu_member_signup}</option>");
													$("#item_value").append("<option value=\'url_agent_signup\'>{lang_menu_agent_signup}</option>");
													$("#item_value").append("<option value=\'url_member_login\'>{lang_menu_member_login}</option>");
													$("#item_value").append("<option value=\'url_agent_login\'>{lang_menu_agent_login}</option>");
													$("#item_value").append("<option value=\'url_view_favorites\'>{lang_menu_url_view_favorites}</option>");
													$("#item_value").append("<option value=\'url_view_calculator\'>{lang_menu_calculators}</option>");
													$("#item_value").append("<option value=\'url_view_saved_searches\'>{lang_menu_saved_searches}</option>");
													$("#item_value").append("<option value=\'url_edit_profile\'>{lang_menu_edit_profile}</option>");
													$("#item_value").append("<option value=\'rss_lastmodified\'>{lang_menu_rss_lastmodified}</option>");
													$("#item_value").append("<option value=\'rss_latestlisting\'>{lang_menu_rss_latestlisting}</option>");
													$("#item_value").append("<option value=\'rss_featured\'>{lang_menu_rss_featured}</option>");
													$("#item_value").append("<option value=\'rss_blog_posts\'>{lang_menu_rss_blog_posts}</option>");
													$("#item_value").append("<option value=\'rss_blog_comments\'>{lang_menu_rss_blog_comments}</option>");


													break;
												case "2":
													//Build a drop down of wysiwyg pages
														jQuery.ajax({
															url: "ajax.php?action=ajax_get_pages",
															success: function(pdata) {
																if(pdata.error){
																	alert("{lang_emenu_error}");
																}else{
																	$("#item_type").after("<div id=\'item_value_container\'>{lang_emenu_page_editor} <br /><select name=\'item_value\' id=\'item_value\'></select><br />\
																			{lang_emenu_target} <br />\
																			<select name=\'item_target\' id=\'item_target\'>\
																			<option value=\'_self\'>_self</option>\
																			<option value=\'_blank\'>_blank</option>\
																			<option value=\'_parent\'>_parent</option>\
																			<option value=\'_top\'>_top</option>\
																			</select><br />\
																			{lang_emenu_custom_css} <br /> <input type=\'text\' name=\'item_class\' id=\'item_class\' value=\'\' /> \
																			</div>");
																	$("#item_value").append("<option value=\'\'>{lang_emenu_action_select}</option>");
																	jQuery.each(pdata.pages , function(page_id, page_name){
																		$("#item_value").append("<option value=\'"+page_id+"\'>"+page_name+"</option>");
																	});
																}
															},
															async: false,
															dataType: "json"
														});
													break;
												case "6":
													//Build a drop down of blogs
														jQuery.ajax({
															url: "ajax.php?action=ajax_get_blogs",
															success: function(pdata) {
																if(pdata.error){
																	alert("{lang_emenu_error}");
																}else{
																	$("#item_type").after("<div id=\'item_value_container\'>{lang_emenu_blog_article} <br /><select name=\'item_value\' id=\'item_value\'></select><br />\
																			{lang_emenu_target} <br />\
																			<select name=\'item_target\' id=\'item_target\'>\
																			<option value=\'_self\'>_self</option>\
																			<option value=\'_blank\'>_blank</option>\
																			<option value=\'_parent\'>_parent</option>\
																			<option value=\'_top\'>_top</option>\
																			</select><br />\
																			{lang_emenu_custom_css} <br /> <input type=\'text\' name=\'item_class\' id=\'item_class\' value=\'\' /> \
																			</div>");
																	$("#item_value").append("<option value=\'\'>{lang_emenu_action_select}</option>");
																	jQuery.each(pdata.blogs , function(blog_id, blog_name){
																		$("#item_value").append("<option value=\'"+blog_id+"\'>"+blog_name+"</option>");
																	});
																}
															},
															async: false,
															dataType: "json"
														});
													break;
												case "3":
													//Build a text box for custom url entry.
													$("#item_type").after("<div id=\'item_value_container\'>{lang_emenu_type_customurl} <br /><input type=\'text\' name=\'item_value\' id=\'item_value\' value=\'http://\' /> <br />\
															{lang_emenu_target} <br />\
															<select name=\'item_target\' id=\'item_target\'>\
															<option value=\'_self\'>_self</option>\
															<option value=\'_blank\'>_blank</option>\
															<option value=\'_parent\'>_parent</option>\
															<option value=\'_top\'>_top</option>\
															</select><br />\
															{lang_emenu_custom_css} <br /> <input type=\'text\' name=\'item_class\' id=\'item_class\' value=\'\' /> \
															</div>");
													$("#item_value").focus();
													break;
												case "4":
													//Do nothing this is just a divider
													$("#item_type").after("<div id=\'item_value_container\'><input type=\'hidden\' name=\'item_value\' id=\'item_value\' value=\'\' />\
															<input type=\'hidden\' name=\'item_target\' id=\'item_target\' value=\'_self\' /><br />\
															<input type=\'hidden\' name=\'item_class\' id=\'item_class\' value=\'\' /> \
															</div>");
													break;
												case "5":
													//Build a drop down of Block Items
													$("#fs_menu_item_type").append("<div id=\'item_value_container\'>{lang_emenu_block_element} <br /> <select name=\'item_value\' id=\'item_value\'></select> <br />\
															{lang_emenu_target} <br />\
															<select name=\'item_target\' id=\'item_target\'>\
															<option value=\'_self\' "+target_self_select+">_self</option>\
															<option value=\'_blank\' "+target_blank_select+">_blank</option>\
															<option value=\'_parent\' "+target_parent_select+">_parent</option>\
															<option value=\'_top\' "+target_top_select+">_top</option>\
															</select><br />\
															{lang_emenu_custom_css} <br /> <input type=\'text\' name=\'item_class\' id=\'item_class\' value=\'\' /> \
															</div>");
													$("#item_value").append("<option value=\'\' selected=\'selected\'>{lang_emenu_select_block_element}</option>");
													$("#item_value").append("<option value=\'blog_recent_comments_block\'>{lang_menu_blog_recent_comments_block}</option>");
													$("#item_value").append("<option value=\'blog_recent_post_block\'>{lang_menu_blog_recent_post_block}</option>");
													$("#item_value").append("<option value=\'blog_archive_link_block\'>{lang_menu_blog_archive_link_block}</option>");
													$("#item_value").append("<option value=\'blog_category_link_block\'>{lang_menu_blog_category_link_block}</option>");
													$("#item_value").append("<option value=\'pclass_searchlinks_block\'>{lang_menu_pclass_searchlinks_block}</option>");
													//pclass_searchlinks_block
													break;
											}
										});
										$("#item_detail_form").append("<div id=\'menu_save_div\'><span id=\'save_item\' class=\'or_std_button\'>{lang_emenu_save_item}</span></div>");
										$("#save_item").click(function(){
											//Validate
											if($("#item_name").val() == ""){
												alert("{lang_emenu_error_enter_item_name}");
												return false;
											}
											if($("#item_type").val() == ""){
												alert("{lang_emenu_error_enter_item_type}");
												return false;
											}
											if($("#item_value").val() == "" && $("#item_type").val() != "4"){
												alert("{lang_emenu_error_enter_item_value}");
												return false;
											}
											$.post("ajax.php?action=ajax_save_menu_item", {
												"item_id": item_id,
												"item_name": $("#item_name").val(),
												"item_type": $("#item_type").val(),
												"item_value":$("#item_value").val(),
												"item_target":$("#item_target").val(),
												"item_class":$("#item_class").val(),
												"visible_guest": function(){
													if($("#visible_guest").is(":checked")){
														return 1;
													}else{
														return 0;
													}
												},
												"visible_member": function(){
													if($("#visible_member").is(":checked")){
														return 1;
													}else{
														return 0;
													}
												},
												"visible_agent": function(){
													if($("#visible_agent").is(":checked")){
														return 1;
													}else{
														return 0;
													}
												},
												"visible_admin": function(){
													if($("#visible_admin").is(":checked")){
														return 1;
													}else{
														return 0;
													}
												},
											},function(data){
												if(!data.error){
													$("#menu_editor_jstree_container").jstree("rename_node", item_obj, $("#item_name").val());
													//Set Type
													switch($("#item_type").val()){
														case "4":
															$(item_obj).attr("rel","divider");
															break;
														case "5":
															$(item_obj).attr("rel","block");
															break;
														default:
															$(item_obj).attr("rel","default");
															break;
													}
													if(!$("#visible_guest").is(":checked") && !$("#visible_member").is(":checked") && !$("#visible_agent").is(":checked") && !$("#visible_admin").is(":checked")){
														$(item_obj).addClass("menu_item_invisible");
													}else{
														$(item_obj).removeClass("menu_item_invisible");
													}
													status_msg("{lang_emenu_item_saved}");
												}
												}, "json");
										});

										}
									}, "json");
								}
							}).bind( "move_node.jstree", function( e, data ) {
								var myelements = tree_order(0);
								$.post("ajax.php?action=ajax_set_menu_order", {
									"menu_id": menu_id,
									"menu_items": myelements
								},function(data){
								}, "json");
							}).bind("loaded.jstree", function (event, data) {
								$("#menu_editor_jstree_container").jstree("open_node","#t_0");
							});

							}
						}, "json");
					}
				});
			});
			function tree_order(parent){
				var elements = {};
				var parent_id;
				if(parent == 0){
					var parent_id = "t_0";
				}else{
					var parent_id = "i_"+parent+" > ul";
				}
				$("#"+parent_id+" > li").each(function(index,elm) {
					var item_id = $(elm).attr("id").substring(2);
					elements[item_id] = { "order" :	index,"parent": parent};
					if ($("#"+$(elm).attr("id")+" > ul").length) {
						var new_elements = tree_order(item_id);
						var old_elements = elements;
						elements = $.extend(old_elements,new_elements);
					}
				});
				return elements;
			}
			</script>';

            //Load the Core Template
            include_once $config['basepath'] . '/include/core.inc.php';

            $page = new page_admin();

            //Load Template File
            $page->load_page($config['admin_template_path'] . '/menu_editor_index.html');

            //We are done finish output
            $page->replace_lang_template_tags(true);
            $page->replace_permission_tags();
            $page->auto_replace_tags('', true);
            $display .= $page->return_page();
        } else {
            $display = '{lang_emenu_permission_denied}';
        }

        return $display;
    }
}
