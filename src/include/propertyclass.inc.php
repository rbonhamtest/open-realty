<?php
class propertyclass
{
    public function show_classes($status_text = '')
    {
        global $config;

        $display = '';
        // Verify User is an Admin
        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('edit_property_classes');

        if ($security === true) {
            global $conn, $misc, $lang;

            //Load the Core Template
            include_once $config['basepath'] . '/include/core.inc.php';
            $page = new page_admin();
            $page->load_page($config['admin_template_path'] . '/pclass_editor.html');

            $display .= '<div class="insert_class_top_contents">
							<a class="or_std_button" href="index.php?action=insert_property_class"><span class="ui-icon ui-icon-plusthick"></span> {lang_property_class_insert}</a>
						</div>
						<div class="space_10"></div>
						<table align="center" class="admin_property_class_table">
							<thead>
							<tr>
								<th></th>
								<th><span class="pclass_column_title">{lang_property_class_id}</span></th>
								<th><span class="pclass_column_title">{lang_property_class_name}</span></th>
								<th><span class="pclass_column_title">{lang_property_class_rank}</span></th>
								<th><span class="pclass_column_title">{lang_action}</span></th>
							</tr>
							</thead>
							
							<tbody>';

            $sql = 'SELECT * FROM ' . $config['table_prefix'] . 'class 
					ORDER BY class_rank';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }

            while (!$recordSet->EOF) {
                $class_name = $recordSet->fields['class_name'];
                $class_id = $recordSet->fields['class_id'];
                $class_rank = $recordSet->fields['class_rank'];

                $display .= '<tr id="'.$class_id.'">
								<td> <span class="ui-icon ui-icon-arrowthick-2-n-s"></span></td>
								<td>'.$class_id.'</td><td>'.$class_name.'</td><td class="clrank">'.$class_rank.'</td>
								<td style="padding: 5px;">
									<div class="classactions">
										<a class="or_std_button modify_class medf" href="ajax.php?action=ajax_modify_property_class&amp;id='.$class_id.'"><img src="{template_url}/images/generic_icon_edit.png" title="{lang_edit}" width="16" height="16" />{lang_edit}</a>
										<a class="or_std_button red medf" href="index.php?action=delete_property_class&amp;id='.$class_id.'" onclick="return confirmDelete(\'{lang_delete_prop_class}\')"><img src="{template_url}/images/generic_icon_delete.png" title="{lang_delete}" width="16" height="16" />{lang_delete}</a>
									</div>
								</td>
							</tr>';
                $recordSet->MoveNext();
            }

            $display .= '	</tbody>
						</table>';

            $display .= '<script>
						    $("tbody").sortable({
						    	helper: fixWidthHelper,
						    	tolerance: "pointer",
						    	opacity: 1,
						    	cursor: "move",
								revert: true,
								scroll: false,
								update: function(event, ui) {
									save_class_rank();
									var $lis = $(this).children("tr");
									$lis.each(function() {
										var newVal = $(this).index() + 1;
										$( this ).find(".clrank").text(newVal);
									});
								}
						    });
						    
						  	function save_class_rank(){
								var current_location = new Array();
								var class_id = new Array();
								var post_trigger ="";
								var action="";
						
								$("tbody").each(function(item){
									current_location.push($(this).attr("id"));
									class_id.push($(this).sortable("toArray"));
								});
								var action = "ajax.php?action=ajax_save_class_rank";
								var post_field_val = "class_rank";
						
								$.post(action, {"search_setup": post_field_val, "class_id[]": class_id },
									function(data){
										if(data.error == "1") {
										status_error(data.error_msg);
									}
									else {
										status_msg("Rank {lang_generic_saved_text} ");
									}
						
								},"text");
						
								return false;
							};
						    
						    function fixWidthHelper(e, ui) {
								ui.children().each(function() {
						    		$(this).width($(this).width());
								});
						    	return ui;
							}
						
						   	$(".modify_class").click( function(event){
						   		event.preventDefault(); 
								$.ajax({
									url: $(this).attr("href"),
									dataType: "html",
									success: function(data){
										$("#message_text").html(data);
										$("#dialog-message").dialog({
											title: "Edit Class",
											modal: true,
											width: "auto",
											buttons: null,
											position: {
												my: "center top+80",
												at: "top",
												of: window,
												collision: "none"
											},
											open: function(){
												$( "#modify_pclass_form" ).validate({
							            	        submitHandler: function(form) {
							            	        	//ShowPleaseWait();
							                	    	$.post("ajax.php?action=ajax_modify_property_class", $("#modify_pclass_form").serialize(),function(data){
							                    	    	if(data.error == "1"){
							                    	    		$("#dialog-message").dialog( "close" ); 	
							                    	    		status_error(data.error_msg)
							                            	}
							                                else{
								                             	document.location = "{baseurl}/admin/index.php?action=show_property_classes&statustext="+data.statustext+"";
								                                $("#dialog-message").dialog( "close" );
								                                status_msg("{lang_template_editor_field_added}");
								                            }	
								                         },"json");
								                        return false;
								                    },
								   					highlight: function(element) {
												        $(element).css("background", "#FFCCCC");
												        $(element).css("border-color", "#3300ff");
												    },
												    // Called when the element is valid:
												    unhighlight: function(element) {
												        $(element).css("background", "#FFFFCC");
												         $(element).css("border-color", "#AAAAAA");
												    }
								                });
											},
											close: function(event, ui) { 
												$( "#message_text" ).empty();
												$( "#dialog-message" ).dialog( "destroy" ); 
											}
										});
									}
								});	
								return false;
							});
						</script>';

            $page->replace_tag('content', $display);
            $page->replace_tag('application_status_text', $status_text);
            $page->replace_lang_template_tags(true);
            $page->replace_permission_tags();
            $page->auto_replace_tags('', true);

            return $page->return_page();
        }
        return $display;
    }

    public function insert_property_class()
    {
        global  $config;
        // Verify User is an Admin
        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('edit_property_classes');
        $display = '';
        if ($security === true) {
            global $conn, $misc, $lang, $jscript;

            //Load the Core Template
            include_once $config['basepath'] . '/include/core.inc.php';
            $page = new page_admin();
            $page->load_page($config['admin_template_path'] . '/pclass_editor.html');

            // Get Max rank
            $sql = 'SELECT max(class_rank) as max_rank 
					FROM ' . $config['table_prefix'] . 'class';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $rank = $recordSet->fields['max_rank'];
            $rank++;

            if ($_POST['class_name']) {
                $class_name = $misc->make_db_safe($_POST['class_name']);

                $class_rank = abs(intval($_POST['class_rank']));
                if ($class_rank == 0) {
                    $class_rank = $rank;
                }

                $sql = 'INSERT INTO ' . $config['table_prefix'] . 'class (class_name,class_rank) 
						VALUES (' . $class_name . ',' . $class_rank . ')';
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                } else {
                    $new_class_id = $conn->Insert_ID();
                }
                if (isset($_POST['field_id']) && $_POST['field_id'] != '') {
                    foreach ($_POST['field_id'] as $field_id) {
                        $sql = 'INSERT INTO ' . $config['table_prefix_no_lang'] . 'classformelements (class_id,listingsformelements_id) 
								VALUES ('.$new_class_id.','.$field_id.')';
                        $recordSet2 = $conn->Execute($sql);
                        if (!$recordSet2) {
                            $misc->log_error($sql);
                        }
                    }
                }
                $display .= $lang['property_class_updated'] . '<br />';
                $display .= $this->show_classes();
            } else {
                $jscript .= '<script type="text/javascript">
								$(document).ready(function(){
									$("#insert_pclass_form").validate({
					   					highlight: function(element) {
									        $(element).css("background", "#FFCCCC");
									        $(element).css("border-color", "#3300ff");
									    },
									    // Called when the element is valid:
									    unhighlight: function(element) {
									        $(element).css("background", "#FFFFCC");
									         $(element).css("border-color", "#AAAAAA");
									    }
									});
									$("#select_all").click(function() {
    									var checked = $(this).prop("checked");
    									$("#new_class_fields").find("input:checkbox").prop("checked", checked);
									});
								});
							</script>';

                $display .= '<form id="insert_pclass_form" action="index.php?action=insert_property_class" method="POST">
								<div class="insert_class_top_contents">
									<div>
										<label for="class_name">{lang_property_class_name}:</label> <input class="required class_box_input1" type="text" value="" name="class_name" />
										<label for="class_rank">{lang_property_class_rank}:</label> <input class="required class_box_input2" type="text" value="' . $rank . '" name="class_rank" />
									</div>
								</div>
								
								<div class="insert_class_box">';

                $sql = 'SELECT listingsformelements_id, listingsformelements_field_caption 
						FROM ' . $config['table_prefix'] . 'listingsformelements 
						ORDER BY listingsformelements_field_caption';
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }

                $display .= '		<table class="admin_option_table">
										<tr>
											<td colspan="3" class="admin_option_caption">
												{lang_property_class_apply}
											</td>
										</tr>
										<tr>
											<td colspan="3" style="padding-left:0px;padding-top:5px;">
												<div style="padding: 10px 0px;">
													<input type="checkbox" id="select_all" /><label for="select_all">{lang_property_class_select_all_fields}: </label>
												</div>
											</td>
										</tr>
										<tbody id="new_class_fields">
										<tr>';

                $count = 1;
                $numcols = 5;
                while (!$recordSet->EOF) {
                    $field_id = $recordSet->fields['listingsformelements_id'];
                    $field_caption = $recordSet->fields['listingsformelements_field_caption'];
                    $display .= '<td><input type="checkbox" name="field_id[]" value="'.$field_id.'" /> ' . $field_caption . '</td>';
                    if ($count % $numcols == 0) {
                        $display .= '</tr><tr>';
                    }
                    $count++;
                    $recordSet->MoveNext();
                }

                $display .= '			</tbody>
										</tr>
									</table>
									<div class="space_20"></div>
									<div style="text-align:center;">
										<input class="or_std_button medf"  type="submit" value=" {lang_submit} " />
									</div>
									<div class="space_15"></div>
								</div>
							</form>';

                $page->replace_tag('content', $display);
                $page->replace_tag('application_status_text', '');
                $page->replace_lang_template_tags(true);
                $page->replace_permission_tags();
                $page->auto_replace_tags('', true);
                return $page->return_page();
            }
        }
        return $display;
    }

    public function delete_property_class()
    {
        global $config;

        // Verify User is an Admin
        $display = '';
        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('edit_property_classes');

        if ($security === true) {
            global $conn, $lang, $misc;

            //Load the Core Template
            include_once $config['basepath'] . '/include/core.inc.php';
            $page = new page_admin();
            $page->load_page($config['admin_template_path'] . '/pclass_editor.html');

            if (isset($_GET['id'])) {
                $class_id = intval($_GET['id']);
                // Now remove any fields associated with the class that are no longer associtaed with any other classes
                // First we have to determine which form elements belong to other classes.
                $sql = 'SELECT DISTINCT (listingsformelements_id) 
						FROM ' . $config['table_prefix_no_lang'] . "classformelements 
						WHERE class_id <> $class_id";
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $other_class_id = '';
                while (!$recordSet->EOF) {
                    if ($other_class_id == '') {
                        $other_class_id .= $recordSet->fields['listingsformelements_id'];
                    } else {
                        $other_class_id .= ',' . $recordSet->fields['listingsformelements_id'];
                    }
                    $recordSet->MoveNext();
                }
                if ($other_class_id == '') {
                    $other_class_id = '0';
                }
                // Ok now grab a list of the id's to delete them from the listingformelements table.
                // Also delete them from the lass_form_elements.
                $sql = 'SELECT DISTINCT (listingsformelements_id) 
						FROM ' . $config['table_prefix_no_lang'] . "classformelements 
						WHERE class_id = $class_id 
						AND listingsformelements_id NOT IN ($other_class_id)";

                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $ids = '';
                while (!$recordSet->EOF) {
                    if ($ids == '') {
                        $ids .= $recordSet->fields['listingsformelements_id'];
                    } else {
                        $ids .= ',' . $recordSet->fields['listingsformelements_id'];
                    }
                    $recordSet->MoveNext();
                }
                if ($ids == '') {
                    $ids = '0';
                }
                $sql = 'DELETE FROM  ' . $config['table_prefix_no_lang'] . "classformelements 
						WHERE listingsformelements_id  IN ($ids)";
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $sql = 'SELECT listingsformelements_field_name 
						FROM ' . $config['table_prefix'] . "listingsformelements 
						WHERE listingsformelements_id  IN ($ids)";
                $recordSet1 = $conn->Execute($sql);
                if (!$recordSet1) {
                    $misc->log_error($sql);
                }
                while (!$recordSet1->EOF) {
                    $field_name = $misc->make_db_safe($recordSet1->fields['listingsformelements_field_name']);
                    // Delete All Translationf for this field.
                    $configured_langs = explode(',', $config['configured_langs']);
                    while (!$recordSet->EOF) {
                        $listingsformelements_id = $recordSet->fields['listingsformelements_id'];
                        foreach ($configured_langs as $configured_lang) {
                            $sql = 'DELETE FROM ' . $config['table_prefix_no_lang'] . $configured_lang . "_listingsformelements 
									WHERE listingsformelements_id IN ($ids)";
                            $recordSet = $conn->Execute($sql);
                            if (!$recordSet) {
                                $misc->log_error($sql);
                            }
                        }
                    }
                    // Cleanup any listingdbelemts entries from this field.
                    $sql = 'SELECT listingsdbelements_id 
							FROM ' . $config['table_prefix'] . "listingsdbelements 
							WHERE listingsdbelements_field_name = $field_name";
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $misc->log_error($sql);
                    }
                    while (!$recordSet->EOF) {
                        $listingsdbelements_id = $recordSet->fields['listingsdbelements_id'];
                        foreach ($configured_langs as $configured_lang) {
                            $sql = 'DELETE FROM ' . $config['table_prefix_no_lang'] . $configured_lang . "_listingsdbelements 
									WHERE listingsdbelements_id = $listingsdbelements_id";
                            $recordSet = $conn->Execute($sql);
                            if (!$recordSet) {
                                $misc->log_error($sql);
                            }
                        }
                    }
                    $recordSet1->MoveNext();
                }

                $sql = 'SELECT listingsdb_id 
						FROM ' . $config['table_prefix'] . "listingsdb 
						WHERE listingsdb_pclass_id = $class_id";
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $ids = '';
                while (!$recordSet->EOF) {
                    if ($ids == '') {
                        $ids .= $recordSet->fields['listingsdb_id'];
                    } else {
                        $ids .= ',' . $recordSet->fields['listingsdb_id'];
                    }
                    $recordSet->MoveNext();
                }
                if ($ids == '') {
                    $ids = '0';
                }
                // now that we have the listingids delete the listings and any associated listingsdbelements
                $configured_langs = explode(',', $config['configured_langs']);
                $listingsformelements_id = $recordSet->fields['listingsformelements_id'];
                foreach ($configured_langs as $configured_lang) {
                    $sql = 'DELETE FROM  ' . $config['table_prefix_no_lang'] . $configured_lang . "_listingsdb 
							WHERE listingsdb_id  IN ($ids)";
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $misc->log_error($sql);
                    }
                    $sql = 'DELETE FROM  ' . $config['table_prefix_no_lang'] . $configured_lang . "_listingsdbelements 
							WHERE listingsdb_id  IN ($ids)";
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $misc->log_error($sql);
                    }
                }
                // Get all images and vtours and delete the images.
                // listingsvtours_id, userdb_id, listingsvtours_caption, listingsvtours_file_name, listingsvtours_thumb_file_name, listingsvtours_description, listingsdb_id, listingsvtours_rank, listingsvtours_active
                $sql = 'SELECT  listingsvtours_thumb_file_name, listingsvtours_file_name 
						FROM  ' . $config['table_prefix'] . "listingsvtours 
						WHERE listingsdb_id  IN ($ids)";
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                while (!$recordSet->EOF) {
                    $file_name = $recordSet->fields['listingsvtours_file_name'];
                    $thumb_name = $recordSet->fields['listingsvtours_thumb_file_name'];
                    @unlink("$config[vtour_upload_path]/$file_name");
                    @unlink("$config[vtour_upload_path]/$file_name");
                    $recordSet->MoveNext();
                }
                // listingsimages_id, userdb_id, listingsimages_caption, listingsimages_file_name, listingsimages_thumb_file_name, listingsimages_description, listingsdb_id, listingsimages_rank, listingsimages_active
                $sql = 'SELECT  listingsimages_thumb_file_name, listingsimages_file_name 
						FROM  ' . $config['table_prefix'] . "listingsimages 
						WHERE listingsdb_id  IN ($ids)";
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                while (!$recordSet->EOF) {
                    $file_name = $recordSet->fields['listingsimages_file_name'];
                    $thumb_name = $recordSet->fields['listingsimages_thumb_file_name'];
                    @unlink("$config[listings_upload_path]/$file_name");
                    @unlink("$config[listings_upload_path]/$file_name");
                    $recordSet->MoveNext();
                }
                // Now delete DB records of the images and vtours for all langs.
                foreach ($configured_langs as $configured_lang) {
                    $sql = 'DELETE FROM  ' . $config['table_prefix_no_lang'] . $configured_lang . "_listingsimages 
							WHERE listingsdb_id  IN ($ids)";
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $misc->log_error($sql);
                    }
                    $sql = 'DELETE FROM  ' . $config['table_prefix_no_lang'] . $configured_lang . "_listingsvtours 
							WHERE listingsdb_id  IN ($ids)";
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $misc->log_error($sql);
                    }
                }
                // Now we jsut need to delete all associates from the classformelements and class tables.
                $sql = 'DELETE FROM  ' . $config['table_prefix_no_lang'] . "classformelements 
						WHERE class_id = $class_id";
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $configured_langs = explode(',', $config['configured_langs']);
                $listingsformelements_id = $recordSet->fields['listingsformelements_id'];
                foreach ($configured_langs as $configured_lang) {
                    $sql = 'DELETE FROM  ' . $config['table_prefix_no_lang'] . $configured_lang . "_class 
							WHERE class_id = $class_id";
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $misc->log_error($sql);
                    }
                }
                $display = $this->show_classes($lang['property_class_deleted']);
            }
        } else {
            $display = 'Permission Denied - Delete Pclass';
        }
        return $display;
    }

    public function ajax_save_class_rank()
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User is an Admin
        $security = $login->verify_priv('edit_property_classes');

        $display ='';
        if ($security === true) {
            global $conn, $misc;
            if (isset($_POST['search_setup'])) {
                $rank_field = 'class_rank';

                //explode the comma delimited field list for this display location
                $class_id_arr = explode(',', $_POST['class_id'][0]);
                $class_rank =0;

                foreach ($class_id_arr as $class_id) {
                    //empty locations are skipped
                    if (!empty($class_id)) {
                        $num_of_fields =count($class_id_arr);
                        $class_rank = $class_rank+1;

                        $sql = 'UPDATE ' . $config['table_prefix'] . 'class
						SET  '.$rank_field." = '".$class_rank."'
						WHERE class_id = '".$class_id."'";
                        $recordSet = $conn->Execute($sql);
                        if (!$recordSet) {
                            $misc->log_error($sql);
                        }
                    }
                }
                $display .= '<center><strong>' . $lang['admin_template_editor_field_order_set'] . ' ('.$num_locations.')</strong></center>';
            }
        } else {
            $display = '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function ajax_modify_property_class()
    {
        global  $config;

        // Verify User is an Admin
        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('edit_property_classes');
        $display = '';
        if ($security === true) {
            //Load the Core Template
            global $conn, $misc, $lang, $jscript;

            if (isset($_GET['id'])) {
                $display .= '<form id="modify_pclass_form" action="ajax.php?action=ajax_modify_property_class" method="POST">';

                $class_id = intval($_GET['id']);
                $sql = 'SELECT class_name, class_rank 
						FROM ' . $config['table_prefix'] . 'class 
						WHERE class_id = ' . $class_id;
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                while (!$recordSet->EOF) {
                    $class_name = $recordSet->fields['class_name'];
                    $class_rank = $recordSet->fields['class_rank'];

                    $display .= '	<div class="modify_fe">
										<div class="caption">'.$lang['property_class_name_update'] .'</div>
										<div class="class_box">
											<input class="required input1" type="text" value="' . $class_name . '" name="class_name" />
										</div>	
									</div>
									<div class="modify_fe">
										<div class="caption">'.$lang['property_class_rank_update'] .'</div>
										<div class="class_box">
											<input class="required input2" type="text" value="' . $class_rank . '" name="class_rank" />
										</div>
									</div>
									<input type="hidden" name="class_id" value="' . intval($_GET['id']) . '" />';

                    $recordSet->MoveNext();
                }
                $display .= '		<div style="text-align:center;">
										<input class="or_std_button modify_class_box_button" type="submit" value="'.$lang['submit'] .'" />
									</div>	
	
							</form>

							
								<!--<div style="float: right;">
									<a href="#" ><img src="'.$config['admin_template_url'].'/images/info.png" alt="get help"/></a>
								</div>-->';
                return $display;
            } elseif ($_POST['class_id']) {
                // Get Max rank
                $sql = 'SELECT max(class_rank) as max_rank 
						FROM ' . $config['table_prefix'] . 'class';
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $rank = $recordSet->fields['max_rank'];
                $rank++;

                $class_id = intval($_POST['class_id']);
                $class_name = $misc->make_db_safe($_POST['class_name']);
                $class_rank = abs(intval($_POST['class_rank']));
                if ($class_rank == 0) {
                    $class_rank = $rank;
                }
                $sql = 'UPDATE ' . $config['table_prefix'] . 'class 
						SET class_name = ' . $class_name . ',class_rank = ' . $class_rank . ' 
						WHERE class_id = ' . $class_id;
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'statustext' => $lang['property_class_updated'] ]);
            }
        }
    }


    /******************* DEPRECATED BELOW *************************************/

    /**
     * Modify Property Class (deprecated)
     *
     * @param   none
     * @return  $page or string $display
     */
    public function modify_property_class()
    {
        global  $config;

        // Verify User is an Admin
        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('edit_property_classes');
        $display = '';
        if ($security === true) {
            //Load the Core Template
            global $conn, $misc, $lang, $jscript;

            if (isset($_GET['id'])) {
                include_once $config['basepath'] . '/include/core.inc.php';
                $page = new page_admin();
                $page->load_page($config['admin_template_path'] . '/pclass_editor.html');

                $jscript .= '<script type="text/javascript">
								$(document).ready(function(){
									$("#modify_pclass_form").validate({
										highlight: function(element) {
								    		$(element).css("background", "#FFCCCC");
								        	$(element).css("border-color", "#3300ff");
								    	},
								    	// Called when the element is valid:
										unhighlight: function(element) {
								        	$(element).css("background", "#FFFFCC");
								        	$(element).css("border-color", "#AAAAAA");
								    	}
									});
								});
							</script>';

                $display .= '<div class="modify_class_box">	
								<form id="modify_pclass_form" action="index.php?action=modify_property_class" method="POST">';

                $class_id = intval($_GET['id']);
                $sql = 'SELECT class_name, class_rank 
						FROM ' . $config['table_prefix'] . 'class 
						WHERE class_id = ' . $class_id;
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                while (!$recordSet->EOF) {
                    $class_name = $recordSet->fields['class_name'];
                    $class_rank = $recordSet->fields['class_rank'];
                    $display .= '<div class="modify_class_box_contents">
									<div>
										{lang_property_class_name_update}<input class="required class_box_input1" type="text" value="' . $class_name . '" name="class_name" />
									</div>	
									<div>
										{lang_property_class_rank_update}<input class="required class_box_input2" type="text" value="' . $class_rank . '" name="class_rank" />
										<input type="hidden" name="class_id" value="' . intval($_GET['id']) . '" />
									</div>	
								</div>zzz';
                    $recordSet->MoveNext();
                }
                $display .= '	<input class="or_std_button modify_class_box_button" type="submit" value="{lang_submit}" />
								</form>
							
								<div class="clear"></div>
							</div>
							
								<!--<div style="float: right;">
									<a href="#" ><img src="'.$config['admin_template_url'].'/images/info.png" alt="get help"/></a>
								</div>-->';

                $page->replace_tag('content', $display);
                $page->replace_tag('application_status_text', '');
                $page->replace_lang_template_tags(true);
                $page->replace_permission_tags();
                $page->auto_replace_tags('', true);
                return $page->return_page();
            } elseif ($_POST['class_id']) {
                // Get Max rank
                $sql = 'SELECT max(class_rank) as max_rank 
						FROM ' . $config['table_prefix'] . 'class';
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $rank = $recordSet->fields['max_rank'];
                $rank++;

                $class_id = intval($_POST['class_id']);
                $class_name = $misc->make_db_safe($_POST['class_name']);
                $class_rank = abs(intval($_POST['class_rank']));
                if ($class_rank == 0) {
                    $class_rank = $rank;
                }
                $sql = 'UPDATE ' . $config['table_prefix'] . 'class 
						SET class_name = ' . $class_name . ',class_rank = ' . $class_rank . ' 
						WHERE class_id = ' . $class_id;
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $display .= $this->show_classes($lang['property_class_updated']);
            }
        }
        return $display;
    }
}
