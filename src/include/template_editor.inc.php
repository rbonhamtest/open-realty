<?php
class template_editor
{

    /*********************************/
    /* EDIT AGENT AND MEMBER FIELDS */
    /*******************************/

    public function edit_user_template($type)
    {
        global $config;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        include_once $config['basepath'] . '/include/core.inc.php';
        $page = new page_admin();

        if ($type == 'member') {
            $page->load_page($config['admin_template_path'] . '/member_template_editor.html');
            $security = $login->verify_priv('edit_member_template');
        } elseif ($type == 'agent') {
            $page->load_page($config['admin_template_path'] . '/agent_template_editor.html');
            $security = $login->verify_priv('edit_agent_template');
        } else {
            return ('Invalid user type');
        }

        $display ='';

        if ($security === true) {
            global $conn, $misc, $jscript, $lang;

            $display1 .= $this->delete_user_field($type);
            $display .= $display1;

            // Grab the list of fields set to be on the search results page sorted by search_result_rank
            $sql = 'SELECT '.$type.'formelements_id, '.$type.'formelements_field_name,
						'.$type.'formelements_required, '.$type.'formelements_field_caption, 
						'.$type.'formelements_rank
						FROM ' . $config['table_prefix'] . $type.'formelements
						ORDER BY '.$type.'formelements_rank;';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $display .= '	<div class="top_left">
								<ul id="user_page_order">'.BR;

            while (!$recordSet->EOF) {
                $fid = $recordSet->fields[$type.'formelements_id'];
                $f_rank = $recordSet->fields[$type.'formelements_rank'];
                // Get Caption from users selected language
                if (!isset($_SESSION['users_lang'])) {
                    $caption = $recordSet->fields[$type.'formelements_field_caption'];
                } else {
                    $field_id = intval($fid);
                    $sql2 = 'SELECT '.$type.'formelements_field_caption
								FROM ' . $config['lang_table_prefix'] . $type."formelements
								WHERE $type.'formelements_id = $field_id";
                    $recordSet2 = $conn->Execute($sql2);
                    if (!$recordSet2) {
                        $misc->log_error($sql2);
                    }
                    $caption = $recordSet2->fields[$type.'formelements_field_caption'];
                }

                if ($recordSet->fields[$type.'formelements_required'] =='Yes') {
                    $required = '<span class="req_field">&nbsp;</span>';
                } else {
                    $required ='<span class="fill">&nbsp;</span>';
                }

                $field_name = $recordSet->fields[$type.'formelements_field_name'];

                $display .='<li id="'.$field_name.'">'.BR;
                $display .= '	<div class="block">'.BR;
                $display .='		<a href="" class="edit_field_link" name="'.$field_name.'">'.$caption.'</a><span class="sortable-number">'.$f_rank.'</span>'.$required.BR;
                $display .='	</div>'.BR;
                $display .='</li>'.BR;

                $recordSet->MoveNext();
            }

            $display .= '	</ul></div><div style="clear: both;"></div>'.BR;

            $jscript .= '{load_css_listing_template_editor}'.BR;

            $display.='
			<script type="text/javascript">
			$(document).ready(function(){
				$( "#tabs" ).uitabs({
					beforeActivate: (function(event){
						ShowPleaseWait();
					}),
					activate: (function(event){
						HidePleaseWait();
					})
				});
				
				$("#user_page_order").sortable({
					tolerance: "pointer",
					revert: true,
					scroll: false,
					opacity: 0.9,
					cancel: "#tabnav",
					update: function(event, ui) {
						save_fields_locations("user_page_order");
						var $lis = $(this).children("li");
						$lis.each(function() {
							var newVal = $(this).index() + 1;
							$( this ).find("span.sortable-number").text(newVal);
						});
					}
				});

				$(".edit_field_link").click(function() {
					var fname =  $(this).attr("name");
					var fcaption = $(this).text();
					if (fname === undefined){
						fname =$("#edit_field_list").val();
						fcaption = $("#edit_field_list option:selected").text();
					}
					edit_user_fields(fname, fcaption);
					return false;
	 			});
	 			
	 			$("#add_field_link").click(function(){
					$.ajax({
						url: "ajax.php?action=ajax_add_user_field&user_type='.$type.'",
						dataType: "html",
						success: function(data){
							$("#message_text").html("<div>"+data+"</div>");
							$("#dialog-message").dialog({
								title: "'.$lang['add_field'].'",
								modal: true,
								width: "auto",
								buttons: null,
								position: {
									my: "center top+5",
									at: "top",
									of: window,
									collision: "none"
								},
								open: function(){
									$( "#mini-tabs" ).uitabs();
				           	        
									$( "#update_field" ).validate({
				           	        	errorLabelContainer: "#response_text",
				            	        submitHandler: function(form) {
				            	        	//ShowPleaseWait();
				                	    	$.post("ajax.php?action=ajax_insert_user_field", $("#update_field").serialize(),function(data){
				                    	    	if(data.error == "1"){
				                    	    		$("#dialog-message").dialog( "close" ); 	
				                    	    		status_error(data.error_msg)
				                            	}
				                                else{
					                             	document.location = "'.$config['baseurl'].'/admin/index.php?action=edit_user_template&type=' . $type . '";
					                                $("#dialog-message").dialog( "close" );
					                                status_msg("'.$lang['admin_template_editor_field_added'].'");
					                            }	
					                         },"json");
					                        return false;
					                    },
					   					highlight: function(element) {
									        $(element).css("background", "#FFCCCC");
									        $(element).css("border-color", "#3300ff");
									    },
									    // Called when the element is valid:
									    unhighlight: function(element) {
									        $(element).css("background", "#FFFFCC");
									         $(element).css("border-color", "#AAAAAA");
									    }
					                });
					                $( "#update_button" ).click(function(){
					                	$( "#update_field" ).submit();
					                });
								},
								close: function(event, ui) { 
									$( "#message_text" ).empty();
									$( "#dialog-message" ).dialog( "destroy" ); 
								}
							});
			 			}
			 		});	
			 		return false;
				});
				

				$("#edit_field_list").change(function(){
					fname = $("#edit_field_list").val();
					if(fname != ""){
						fcaption = $("#edit_field_list option:selected").text();
						edit_user_fields(fname, fcaption);
						$("#edit_field_list").val("");
						return false;
					}
				});
	 		});	
	 		function save_fields_locations(type){
					var current_location = new Array();
		 			var field_name = new Array();
					var post_trigger ="";
					var action="";
	 				$("#user_page_order").each(function(item){
						current_location.push($(this).attr("id"));
						field_name.push($(this).sortable("toArray"));
					});
 					var action = "ajax.php?action=ajax_save_user_rank";
 					var post_field_val = "search_order";

					$.post(action, {"user_type": "'.$type.'", "field_name[]": field_name },
					function(data){
						if(data.error == "1") {
							status_error(data.error_msg);
						}
						else {
							status_msg("{lang_admin_template_editor_field_order_set}");
						}
					},"text");
					return false;
				};
				
				function edit_user_fields(fname, fcaption) {
					$.get("ajax.php?action=ajax_get_user_field_info", { edit_field: fname, user_type: "'.$type.'" },
					function(data) {
						$("#message_text").html("<div>"+data+"</div>");
						
						$("#dialog-message").dialog({
							title: "'.$lang['leadmanager_edit_text'].' "+fcaption +" ("+fname+")",
							modal: true,
							width: "auto",
							buttons: null,
							position: {
								my: "center top+5",
								at: "top",
								of: window,
								collision: "none"
							},
							open: function(){
								$( "#mini-tabs" ).uitabs();
								$("#update_field").validate({
				          	        errorLabelContainer: "#response_text",
				           	        submitHandler: function(form) {
				               	    	$.post("ajax.php?action=ajax_update_user_field", $("#update_field").serialize(),function(data){
				                   	    	if(data.error == "1"){
				                   	    		$("#dialog-message").dialog( "close" ); 	
				                   	    		status_error(data.error_msg)
				                           	}
				                               else{
				                                document.location = "index.php?action=edit_user_template&type='.$type.'";
				                                $("#dialog-message").dialog( "close" );
				                                status_msg("'.$lang['field_has_been_updated'].'");
				                            }	
				                         },"json");
				                        return false;
				                    },
				   					highlight: function(element) {
								        $(element).css("background", "#FFCCCC");
								        $(element).css("border-color", "#3300ff");
								    },
								    // Called when the element is valid:
								    unhighlight: function(element) {
								        $(element).css("background", "#FFFFCC");
								         $(element).css("border-color", "#AAAAAA");
								    }
				                });
				                $("#update_button").click(function(){
				                	$("#update_field").submit();
				                });
							},
							close: function(event, ui) { 
								$("#message_text").empty();
								$("#dialog-message").dialog( "destroy" ); 
							}
						});
			 		}, "html"
			 	);
			 }

		</script>';

            $page->replace_tag('content', $display);

            $user_template_navbar = $this->show_user_navbar($type);
            $page->replace_tag('user_template_navbar', $user_template_navbar);

            $page->replace_tag('application_status_text', '');
            $page->replace_lang_template_tags(true);
            $page->replace_permission_tags();
            $page->auto_replace_tags('', true);
            return $page->return_page();
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return 	$display;
    }

    public function show_user_navbar($type)
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        if ($type == 'member') {
            $security = $login->verify_priv('edit_member_template');
        } else {
            $security = $login->verify_priv('edit_agent_template');
        }
        if ($security) {
            global $conn, $jscript, $misc;

            $display = '';
            // Grab a list of pages in the Database to Edit
            $sql = 'SELECT ' . $type . 'formelements_field_name, ' . $type . 'formelements_field_caption 
					FROM ' . $config['table_prefix'] . $type . 'formelements';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $field_names = '<option value="" selected="selected">'.$lang['select_field_to_edit'].'</option>';

            while (!$recordSet->EOF) {
                $caption = $recordSet->fields[$type . 'formelements_field_caption'];
                $field_names .= '<option value="' . $recordSet->fields[$type . 'formelements_field_name'] . '">' . $caption .' ('. $recordSet->fields[$type . 'formelements_field_name'] . ')</option>';
                $recordSet->MoveNext();
            }
            //<span class="or_std_button" id="add_field_link" >'.$lang['add_field'].'</span>
            $display.=	'<div class="template_editor_navbar">
							<form action="' . $config['baseurl'] . '/admin/ajax.php?action=ajax_edit_user_template" method="POST" id="user_field_navbar_form">
								<input type="hidden" name="user_type" value="' . $type . '" />
								<div class="template_editor_navbar_form">
									'.$lang['field'].'
			 						<select id="edit_field_list" name="edit_field" class="edit_field">
										'.$field_names.'
									</select>
									<button class="or_std_button" id="add_field_link" ><span class="ui-icon ui-icon-plusthick"></span> '.$lang['add_field'].'</button>
									
								</div>
								<div style="clear: both;"></div>
							</form>
						</div>
						<div style="clear: both;"></div>
						<br />';

            return $display;
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
    }

    public function ajax_add_user_field($type)
    {
        global $config, $lang;
        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();

        if ($type == 'member') {
            $security = $login->verify_priv('edit_member_template');
        } elseif ($type == 'agent') {
            $security = $login->verify_priv('edit_agent_template');
        } else {
            return ('Invalid user type');
        }

        if ($security) {
            $display = $this->add_user_template_field($type);
        } else {
            $display = '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function add_user_template_field($type)
    {
        global $config, $lang, $jscript;

        $display = '';
        include_once $config['basepath'] . '/include/core.inc.php';
        $page = new page_admin();
        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();

        if ($type == 'member') {
            $security = $login->verify_priv('edit_member_template');
        } else {
            $security = $login->verify_priv('edit_agent_template');
        }

        if ($security === true) {
            global $conn, $misc;

            $display = '';

            $display .= '<div class="edit_field_div">

								<div id="mini-tabs">
									<ul>
										<li><a href="#gen_opt">' . $lang['general_options'] . '</a></li>
									</ul>
									<!-- tab "panes" -->
	    							<div id="gen_opt">
										<form action="' . $config['baseurl'] . '/admin/index.php?action=edit_' . $type . '_template_add_field" method="post" id="update_field">
											<input type="hidden" name="user_type" value="' . $type . '">
											<table id="user_field_table">
												<tr>
													<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_field_name'] . ':</td>
													<td class="templateEditorHead" align="left"><input type="text" name="edit_field" value="" required />
														<script>
															$(document).ready(function(){
																$("input[name=edit_field]").on("input",function() {
																	var oldVal = $(this).val();
																	var newVal = oldVal.replace(RegExp("[^A-Za-z0-9_]","g"),"");
																	$(this).val(newVal);
																});
															});		
														</script>
													</td>
												</tr>
												<tr>
													<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_field_caption'] . ':</td>
													<td class="templateEditorHead" align="left"><input type="text" name="field_caption" value="" required /></td>
												</tr>
												<tr>
													<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_field_type'] . ':</td>
													<td class="templateEditorHead" align="left">
														<select name="field_type" size="1">
														<option value="text" selected="selected">' . $lang['text'] . '</option>
														<option value="textarea" >' . $lang['textarea'] . '</option>
														<option value="select" >' . $lang['select'] . '</option>
														<option value="select-multiple">' . $lang['select-multiple'] . '</option>
														<option value="option" >' . $lang['option'] . '</option>
														<option value="checkbox" >' . $lang['checkbox'] . '</option>
														<option value="divider">' . $lang['divider'] . '</option>
														<option value="price">' . $lang['price'] . '</option>
														<option value="url">' . $lang['url'] . '</option>
														<option value="email">' . $lang['email'] . '</option>
														<option value="number">' . $lang['number'] . '</option>
														<option value="decimal">' . $lang['decimal'] . '</option>
														<option value="date">' . $lang['date'] . '</option>
														<option value="lat">' . $lang['lat'] . '</option>
														<option value="long">' . $lang['long'] . '</option>
														</select>
													</td>
												</tr>
												<tr>
													<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_field_required'] . ':</td>
													<td class="templateEditorHead" align="left">
														<select name="required" size="1">
														<option value="No"  selected="selected">' . $lang['no'] . '</option>
														<option value="Yes" >' . $lang['yes'] . '</option>
														</select>
													</td>
												</tr>

												<tr>
													<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_field_elements'] . ':
														<br />
														<div class="small">(' . $lang['admin_template_editor_choices_separated'] . ')</div>
													</td>
													<td class="templateEditorHead" align="left"><input type="text" name="field_elements" value=""></td>
												</tr>
												<tr>
													<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_field_default_text'] . ':</td>
													<td class="templateEditorHead" align="left"><input type="text" name="default_text" value = ""></td>
												</tr>
												<tr>
													<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_tool_tip'] . ':</td>
													<td class="templateEditorHead" align="left"><textarea name="tool_tip"></textarea></td>
												</tr>
												<tr>
													<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_field_rank'] . ':</td>
													<td class="templateEditorHead" align="left" ><input type="text" name="rank" value="0"></td>
												</tr>
											</table>
										</form>
										
									</div>
								</div>

								<div class="space_10"></div>
								<div style="text-align: center">
									<a href="#" class="or_std_button medf" id="update_button">' . $lang['add_field'] . '</a>
								</div>	
								<div class="space_10"></div>
						</div>';
        } else {
            $display =  '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function ajax_insert_user_field()
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();

        if (isset($_POST['user_type'])) {
            $type = $_POST['user_type'];
        }
        if ($type == 'member') {
            $security = $login->verify_priv('edit_member_template');
        } else {
            $security = $login->verify_priv('edit_agent_template');
        }

        if ($security === true) {
            global $conn, $misc;
            if (isset($_POST['edit_field']) && !isset($_POST['lang_change'])) {
                $field_type = $misc->make_db_safe($_POST['field_type']);
                $_POST['edit_field'] = str_replace(' ', '_', $_POST['edit_field']);
                $field_name = $misc->make_db_safe($_POST['edit_field']);
                $field_caption = $misc->make_db_safe($_POST['field_caption']);
                $default_text = $misc->make_db_safe($_POST['default_text']);
                $field_elements = $misc->make_db_safe($_POST['field_elements']);
                $rank = intval($_POST['rank']);
                $required = $misc->make_db_safe($_POST['required']);
                $tool_tip = $misc->make_db_safe($_POST['tool_tip']);
                if ($type == 'member') {
                    $sql = 'INSERT INTO ' . $config['table_prefix'] . $type . 'formelements
							(' . $type . 'formelements_field_type, ' . $type . 'formelements_field_name, ' . $type . 'formelements_field_caption, ' . $type . 'formelements_default_text, ' . $type . 'formelements_field_elements, ' . $type . 'formelements_rank, ' . $type . 'formelements_required, ' . $type . "formelements_tool_tip)
							VALUES ($field_type,$field_name,$field_caption,$default_text,$field_elements,$rank,$required,$tool_tip)";
                } else {
                    $sql = 'INSERT INTO ' . $config['table_prefix'] . $type . 'formelements
							(' . $type . 'formelements_field_type, ' . $type . 'formelements_field_name, ' . $type . 'formelements_field_caption, ' . $type . 'formelements_default_text, ' . $type . 'formelements_field_elements, ' . $type . 'formelements_rank, ' . $type . 'formelements_required,' . $type . 'formelements_display_priv, ' . $type . "formelements_tool_tip)
							VALUES ($field_type,$field_name,$field_caption,$default_text,$field_elements,$rank,$required,0,$tool_tip)";
                }
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0','field_name' => $field_name]);
            }
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1','error_msg' =>  $lang['access_denied']]);
        }
    }

    public function edit_user_field($listing_field_name, $type)
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        if ($type == 'member') {
            $security = $login->verify_priv('edit_member_template');
        } else {
            $security = $login->verify_priv('edit_agent_template');
        }
        if ($security === true) {
            global $conn, $misc;

            $edit_listing_field_name = $misc->make_db_safe($listing_field_name);
            $sql = 'SELECT * FROM ' . $config['table_prefix'] . $type . 'formelements
					WHERE ' . $type . 'formelements_field_name = ' . $edit_listing_field_name;
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $id = $recordSet->fields[$type . 'formelements_id'];
            $field_type = $recordSet->fields[$type . 'formelements_field_type'];
            $field_name = $recordSet->fields[$type . 'formelements_field_name'];
            $field_caption = $recordSet->fields[$type . 'formelements_field_caption'];
            $default_text = $recordSet->fields[$type . 'formelements_default_text'];
            $field_elements = $recordSet->fields[$type . 'formelements_field_elements'];
            $rank = $recordSet->fields[$type . 'formelements_rank'];
            $required = $recordSet->fields[$type . 'formelements_required'];
            $tool_tip = $recordSet->fields[$type . 'formelements_tool_tip'];
            if ($type == 'agent') {
                $display_priv = $recordSet->fields['agentformelements_display_priv'];
            }

            $display = '';

            $display .= '<div class="edit_field_div">
							<form action="' . $config['baseurl'] . '/admin/index.php?action=edit_user_template&amp;type=' . $type . '" method="post"  id="update_field">
							
								<div id="mini-tabs">
									<ul>
										<li><a href="#gen_opt">' . $lang['general_options'] . '</a></li>
									</ul>

								
									<!-- 1st pane General Options -->
									<div id="gen_opt">
										<table id="user_field_table">
											<tr>
												<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_field_name'] . ':</td>
												<td class="templateEditorHead" align="left">
													<input type="hidden" name="update_id" value="' . $id . '">
													<input type="hidden" name="user_type" value="' . $type . '">
													<input type="hidden" name="old_field_name" value="' . $field_name . '" />
													<input type="text" name="edit_field" value="' . $field_name . '" required />
													<script>
														$(document).ready(function(){
															$("input[name=edit_field]").on("input",function() {
																var oldVal = $(this).val();
																var newVal = oldVal.replace(RegExp("[^A-Za-z0-9_]","g"),"");
																$(this).val(newVal);
															});
														});		
													</script>
												</td>
											</tr>
											<tr>
												<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_field_caption'] . ':</td>
												<td class="templateEditorHead" align="left"><input type="text" name="field_caption" value = "' . $field_caption . '" required /></td>
											</tr>
											<tr>
												<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_field_type'] . ':</td>
												<td class="templateEditorHead" align="left">
												<select name="field_type" size="1">
												<option value="' . $field_type . '" selected="selected">' . $lang[$field_type] . '</option>
												<option value="">-----</option>
												<option value="text">' . $lang['text'] . '</option>
												<option value="textarea" >' . $lang['textarea'] . '</option>
												<option value="select" >' . $lang['select'] . '</option>
												<option value="select-multiple">' . $lang['select-multiple'] . '</option>
												<option value="option" >' . $lang['option'] . '</option>
												<option value="checkbox" >' . $lang['checkbox'] . '</option>
												<option value="divider">' . $lang['divider'] . '</option>
												<option value="price">' . $lang['price'] . '</option>
												<option value="url">' . $lang['url'] . '</option>
												<option value="email">' . $lang['email'] . '</option>
												<option value="number">' . $lang['number'] . '</option>
												<option value="decimal">' . $lang['decimal'] . '</option>
												<option value="date">' . $lang['date'] . '</option>
												<option value="lat">' . $lang['lat'] . '</option>
												<option value="long">' . $lang['long'] . '</option>
												</select>
												</td>
											</tr>
											<tr>
												<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_field_required'] . ':</td>
												<td class="templateEditorHead" align="left">
												<select name="required" size="1">
														<option value="' . $required . '" selected="selected">' . $lang[strtolower($required)] . '</option>
												<option value="No">-----</option>
												<option value="No">' . $lang['no'] . '</option>
												<option value="Yes" >' . $lang['yes'] . '</option>
												</select>
												</td>
											</tr>
											
											<tr>
												<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_field_elements'] . ':
													<br />
													<div class="small">(' . $lang['admin_template_editor_choices_separated'] . ')</div>
												</td>
												<td class="templateEditorHead" align="left"><input type="text" name="field_elements" value = "' . $field_elements . '"></td>
											</tr>
											<tr>
												<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_field_default_text'] . ':</td>
												<td class="templateEditorHead" align="left"><input type="text" name="default_text" value = "' . $default_text . '"></td>
											</tr>
											<tr>
												<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_tool_tip'] . ':</td>
												<td class="templateEditorHead" align="left"><textarea name="tool_tip" >' . $tool_tip . '</textarea></td>
												</tr>
											<tr>
												<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_field_rank'] . ':</td>
												<td class="templateEditorHead" align="left" ><input type="text" name="rank" value = "' . $rank . '"></td>
											</tr>';

            if ($type == 'agent') {
                $display .= '					<tr>
												<td align="right" class="templateEditorHead" >' . $lang['admin_template_editor_field_display_priv'] . ':</td>
												<td class="templateEditorHead" align="left">
												<select name="display_priv" size="1">
													<option value="' . $display_priv . '" selected="selected">' . $lang['display_priv_' . $display_priv] . '</option>
													<option value="0">-----</option>
													<option value="0">' . $lang['display_priv_0'] . '</option>
													<option value="1" >' . $lang['display_priv_1'] . '</option>
													<option value="2" >' . $lang['display_priv_2'] . '</option>
													<option value="3" >' . $lang['display_priv_3'] . '</option>
												</select>
												</td>
											</tr>';
            }

            $display .= '					
										</table>
									</div>
									
								</div>
								
							</form>
							<div class="space_10"></div>
							<div style="text-align: center">
								<a href="#" class="or_std_button medf" id="update_button">' . $lang['update_button']  . '</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<a class="or_std_button red medf" href="' . $config['baseurl'] . '/admin/index.php?action=edit_user_template&amp;type=' . $type . '&amp;delete_field=' . urlencode($listing_field_name) . '" onclick="return confirmDelete()">' . $lang['delete'] . '</a>
							</div>	
							<div class="space_10"></div>

						</div>';

            return $display;
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
    }

    public function ajax_update_user_field()
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();

        if (isset($_POST['user_type'])) {
            $type = $_POST['user_type'];
        }
        if ($type == 'member') {
            $security = $login->verify_priv('edit_member_template');
        } else {
            $security = $login->verify_priv('edit_agent_template');
        }
        if ($security === true) {
            global $conn, $misc;
            if (isset($_POST['update_id']) && !isset($_POST['lang_change'])) {
                $id = $_POST['update_id'];
                $field_type = $misc->make_db_safe($_POST['field_type']);
                $_POST['edit_field'] = str_replace(' ', '_', $_POST['edit_field']);
                $_POST['old_field_name'] = str_replace(' ', '_', $_POST['old_field_name']);
                $field_name = $misc->make_db_safe($_POST['edit_field']);
                $old_field_name = $misc->make_db_safe($_POST['old_field_name']);
                //See if we are updating field name
                $update_field_name = false;
                if ($old_field_name != $field_name) {
                    $update_field_name = true;
                }
                $field_caption = $misc->make_db_safe($_POST['field_caption']);
                $default_text = $misc->make_db_safe($_POST['default_text']);
                $field_elements = $misc->make_db_safe($_POST['field_elements']);
                $rank = intval($_POST['rank']);
                $required = $misc->make_db_safe($_POST['required']);
                $tool_tip = $misc->make_db_safe($_POST['tool_tip']);
                if ($type == 'agent') {
                    $display_priv = $misc->make_db_safe($_POST['display_priv']);
                    $sql = 'UPDATE ' . $config['table_prefix'] . $type . 'formelements
							SET ' . $type . 'formelements_field_type = ' . $field_type . ', ' . $type . 'formelements_field_name = ' . $field_name . ', ' . $type . 'formelements_field_caption = ' . $field_caption . ', ' . $type . 'formelements_default_text = ' . $default_text . ', ' . $type . 'formelements_field_elements = ' . $field_elements . ', ' . $type . 'formelements_rank = ' . $rank . ', ' . $type . 'formelements_required = ' . $required . ', ' . $type . 'formelements_display_priv  = ' . $display_priv . ', ' . $type . 'formelements_tool_tip  = ' . $tool_tip . '
							WHERE ' . $type . 'formelements_id = ' . $id;
                } else {
                    $sql = 'UPDATE ' . $config['table_prefix'] . $type . 'formelements
							SET ' . $type . 'formelements_field_type = ' . $field_type . ', ' . $type . 'formelements_field_name = ' . $field_name . ', ' . $type . 'formelements_field_caption = ' . $field_caption . ', ' . $type . 'formelements_default_text = ' . $default_text . ', ' . $type . 'formelements_field_elements = ' . $field_elements . ', ' . $type . 'formelements_rank = ' . $rank . ', ' . $type . 'formelements_required = ' . $required . ', ' . $type . 'formelements_tool_tip  = ' . $tool_tip . '
							WHERE ' . $type . 'formelements_id = ' . $id;
                }
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                if ($update_field_name) {
                    $lang_sql = 'UPDATE  ' . $config['table_prefix'] . "userdbelements 
								SET userdbelements_field_name = $field_name
								WHERE userdbelements_field_name = $old_field_name";
                    $lang_recordSet = $conn->Execute($lang_sql);
                    if (!$lang_recordSet) {
                        $misc->log_error($lang_sql);
                    }
                }
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }

                header('Content-type: application/json');
                return json_encode(['error' => '0','field_id' =>  $id]);
            }
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1','error_msg' =>  $lang['access_denied']]);
        }
    }

    public function ajax_get_user_field_info($type)
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();

        if ($type == 'member') {
            $security = $login->verify_priv('edit_member_template');
        } elseif ($type == 'agent') {
            $security = $login->verify_priv('edit_agent_template');
        } else {
            return ('Invalid user type');
        }

        $display = '';

        if ($security === true) {
            if (isset($_GET['edit_field'])) {
                $display .= $this->edit_user_field($_GET['edit_field'], $type);
            }
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function ajax_save_user_rank()
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();

        if (isset($_POST['user_type'])) {
            if ($_POST['user_type'] =='agent') {
                $rank_field = 'agentformelements_rank';
            } elseif ($_POST['user_type'] =='member') {
                $rank_field = 'memberformelements_rank';
            }
            $type = $_POST['user_type'];
        }
        $sec_type = 'edit_'.$type.'_template';

        //security check
        $security = $login->verify_priv($sec_type);
        if ($security === true) {
            global $conn, $misc;

            //explode the comma delimited field list for this display location
            $field_name_arr = explode(',', $_POST['field_name'][0]);
            $rank =0;

            foreach ($field_name_arr as $field_name) {
                //empty locations are skipped
                if (!empty($field_name)) {
                    $num_of_fields =count($field_name_arr);
                    $rank = $rank+1;

                    $sql = 'UPDATE ' . $config['table_prefix'] . $type .'formelements
					SET  '.$rank_field." = '".$rank."'
					WHERE ".$type ."formelements_field_name = '".$field_name."'";
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $misc->log_error($sql);
                    }
                }
            }
            $display = '<center><strong>' . $lang['admin_template_editor_field_order_set'] . ' ('.$num_locations.')</strong></center>';
        } else {
            $display = '<div class="error_text">' . $lang['access_denied'].'</div>';
        }
        return $display;
    }

    public function delete_user_field($type)
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        if ($type == 'member') {
            $security = $login->verify_priv('edit_member_template');
        } else {
            $security = $login->verify_priv('edit_agent_template');
        }
        if ($security) {
            global $conn, $misc;
            if (isset($_GET['delete_field']) && !isset($_POST['lang_change'])) {
                $field_name = $misc->make_db_safe($_GET['delete_field']);
                $sql = 'DELETE FROM ' . $config['table_prefix'] . $type . 'formelements
						WHERE ' . $type . 'formelements_field_name = ' . $field_name;
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
            }
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
    }

    /************************/
    /* EDIT LISTING FIELDS */
    /**********************/

    public function edit_listing_template()
    {
        global $config;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User is an Admin
        $security = $login->verify_priv('edit_listing_template');

        //Load the Core Template
        include_once $config['basepath'] . '/include/core.inc.php';
        $page = new page_admin();
        $page->load_page($config['admin_template_path'] . '/listing_template_editor.html');
        $display = '';
        $display1 = '';
        $quick_edit ='';

        if ($security) {
            global $conn, $lang, $jscript, $misc;
            $display1 .= $this->delete_listing_field();
            //$display1 .= $this->save_search_setup();
            $display .= $display1;

            $jscript .= '{load_css_listing_template_editor}'.BR;

            $jscript .= '<script type="text/javascript">
						$(document).ready(function(){
							$( "#tabs" ).uitabs({
								 beforeActivate: (function(event){
									ShowPleaseWait();
								 }),
								 activate: (function(event){
									HidePleaseWait();
								})
							});
							
							$("#add_field_link").click(function(){
								$.ajax({
									url: "ajax.php?action=ajax_add_listing_field",
									dataType: "html",
									success: function(data){
										$("#message_text").html("<div>"+data+"</div>");
										$("#dialog-message").dialog({
											title: "'.$lang['add_field'].'",
											modal: true,
											width: "auto",
											buttons: null,
											position: {
												my: "center top+5",
												at: "top",
												of: window,
												collision: "none"
											},
											open: function(){
												$( "#mini-tabs" ).uitabs();
							           	        
												$( "#update_field" ).validate({
							           	        	errorLabelContainer: "#response_text",
							            	        submitHandler: function(form) {
							            	        	//ShowPleaseWait();
							                	    	$.post("ajax.php?action=ajax_insert_listing_field", $("#update_field").serialize(),function(data){
							                    	    	if(data.error == "1"){
							                    	    		$("#dialog-message").dialog( "close" ); 	
							                    	    		status_error(data.error_msg)
							                            	}
							                                else{
								                             	document.location = "'.$config['baseurl'].'/admin/index.php?action=edit_listing_template";
								                                $("#dialog-message").dialog( "close" );
								                                status_msg("'.$lang['admin_template_editor_field_added'].'");
								                            }	
								                         },"json");
								                        return false;
								                    },
								   					highlight: function(element) {
												        $(element).css("background", "#FFCCCC");
												        $(element).css("border-color", "#3300ff");
												    },
												    // Called when the element is valid:
												    unhighlight: function(element) {
												        $(element).css("background", "#FFFFCC");
												         $(element).css("border-color", "#AAAAAA");
												    }
								                });
								                $( "#update_button" ).click(function(){
								                	$( "#update_field" ).submit();
								                });
											},
											close: function(event, ui) { 
												$( "#message_text" ).empty();
												$( "#dialog-message" ).dialog( "destroy" ); 
											}
										});
			 						}
			 					});
			 					return false;
							});
							
							$("#headline, #top_left, #top_right, #center, #feature1, #feature2, #bottom_left, #bottom_right").disableSelection();
							
							$(".equal_top").equalHeights(); 
							$(".equal_bottom").equalHeights(); 
						});

			 			function edit_listing_fields(fname, fcaption) {
			 				$.get("ajax.php?action=ajax_get_listing_field_info", { edit_field: fname },
								
			 					function(data){
									$("#message_text").html("<div>"+data+"</div>");
									$("#dialog-message").dialog({
										title: "'.$lang['leadmanager_edit_text'].' "+fcaption +" ("+fname+")",
										modal: true,
										width: "auto",
										buttons: null,
										position: {
											my: "center top+5",
											at: "top",
											of: window,
											collision: "none"
										},
										open: function(){
											$( "#mini-tabs" ).uitabs();
						           	        
											$("#update_field").validate({
						           	        	errorLabelContainer: "#response_text",
						            	        submitHandler: function(form) {
						                	    	$.post("ajax.php?action=ajax_update_listing_field", $("#update_field").serialize(),function(data){
						                    	    	if(data.error == "1"){
						                    	    		$("#dialog-message").dialog( "close" ); 	
						                    	    		status_error(data.error_msg)
						                            	}
						                                else{
							                                document.location = "index.php?action=edit_listing_template";
							                                $("#dialog-message").dialog( "close" );
							                                status_msg("'.$lang['field_has_been_updated'].'");
							                            }	
							                         },"json");
							                        return false;
							                    },
							   					highlight: function(element) {
											        $(element).css("background", "#FFCCCC");
											        $(element).css("border-color", "#3300ff");
											    },
											    // Called when the element is valid:
											    unhighlight: function(element) {
											        $(element).css("background", "#FFFFCC");
											         $(element).css("border-color", "#AAAAAA");
											    }
							                });
							                $("#update_button").click(function(){
							                	$("#update_field").submit();
							                });
										},
										close: function(event, ui) { 
											$("#message_text").empty();
											$("#dialog-message").dialog( "destroy" ); 
										}
									});
			 					}, "html"
			 				);
			 			}

						function save_fields_locations(type){

							var current_location = new Array();
			 				var field_name = new Array();
							var post_trigger ="";
							var action="";

			 				if (type == "search_page_order"){
			 					$("#search_page_order").each(function(item){
									current_location.push($(this).attr("id"));
									field_name.push($(this).sortable("toArray"));
								});
			 					var action = "ajax.php?action=ajax_save_listing_search_order";
			 					var post_field_val = "search_order";
			 				}
			 				else if (type == "search_result_order") {
			 					$("#search_result_order").each(function(item){
									current_location.push($(this).attr("id"));
									field_name.push($(this).sortable("toArray"));
								});
			 					var action = "ajax.php?action=ajax_save_search_result_order";
			 					var post_field_val = "search_results";
			 				}
			 				else {
								$(".qed_list").each(function(item){
									current_location.push($(this).attr("id"));
									field_name.push($(this).sortable("toArray"));
								});
			 					var action = "ajax.php?action=ajax_save_listing_field_order";
			 					var post_field_val = current_location;
			 				}

							$.post(action, {"search_setup[]": post_field_val, "field_name[]": field_name },
								function(data){
									if(data.error == "1") {
										status_error(data.error_msg);
									}
									else {
										status_msg("{lang_admin_template_editor_field_order_set}");
									}
								},"text");
							return false;
						};

			</script>';

            $page->replace_tag('content', $display);

            $listing_template_navbar = $this->show_listing_navbar();
            $page->replace_tag('listing_template_navbar', $listing_template_navbar);

            $page->replace_tag('application_status_text', '');
            $page->replace_lang_template_tags(true);
            $page->replace_permission_tags();
            $page->auto_replace_tags('', true);
            return $page->return_page();
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function show_listing_navbar()
    {
        global $config;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('edit_listing_template');
        if ($security === true) {
            global $conn, $lang, $misc;

            $display = '';
            // Grab a list of field_names in the Database to Edit
            $sql = 'SELECT listingsformelements_field_name, listingsformelements_field_caption, listingsformelements_id
					FROM ' . $config['table_prefix'] . 'listingsformelements';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $field_names = '<option value="" selected="selected">'.$lang['select_field_to_edit'].'</option>';

            while (!$recordSet->EOF) {
                // Get Caption from users selected language
                if (!isset($_SESSION['users_lang'])) {
                    $caption = $recordSet->fields['listingsformelements_field_caption'];
                } else {
                    $field_id = intval($recordSet->fields['listingsformelements_id']);
                    $sql2 = 'SELECT listingsformelements_field_caption
							FROM ' . $config['lang_table_prefix'] . "listingsformelements
							WHERE listingsformelements_id = $field_id";
                    $recordSet2 = $conn->Execute($sql2);
                    if (!$recordSet2) {
                        $misc->log_error($sql2);
                    }
                    $caption = $recordSet2->fields['listingsformelements_field_caption'];
                }
                if (isset($_GET['edit_field']) && $_GET['edit_field'] == $recordSet->fields['listingsformelements_field_name']) {
                    $selected = ' selected="selected" ';
                } else {
                    $selected = '';
                }
                $field_names .= '<option value="' . $recordSet->fields['listingsformelements_field_name'] . '" ' . $selected . '>' . $caption . ' ('.$recordSet->fields['listingsformelements_field_name'].')</option>';

                $recordSet->MoveNext();
            }
            //	$display .= '<span class="section_header">' . $lang['listing_editor'] . '</span><br /><br />';
            $display .= '<div class="template_editor_navbar">'.BR;
            $display .= '	<form action="' . $config['baseurl'] . '/admin/index.php" id="navbar">'.BR;
            $display .= '		<div class="template_editor_navbar_form">' . $lang['field'];
            $display .= '			<input type="hidden" name="action" value="edit_listing_template" />'.BR;
            $display .= '			<select id="edit_field_list">'.BR;
            $display .= $field_names;
            $display .= '			</select>'.BR;
            //$display .= '			<span class="edit_field_link or_std_button">' . $lang['edit'] . '</span>'.BR;
            //$display .= '			<span class="or_std_button add_field_link" id="add_field_link">' . $lang['add_field'] . '</span>'.BR;
            $display .= '			<button class="or_std_button" id="add_field_link" ><span class="ui-icon ui-icon-plusthick"></span> '.$lang['add_field'].'</button>'.BR;

            $display .= '		</div>'.BR;
            $display .= '			<div style="clear: both;"></div>'.BR;
            $display .= '	</form>'.BR;

            $display .= '</div>'.BR;

            $display .= '<div style="clear: both;"></div><br />'.BR;
            return $display;
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
    }

    public function show_quick_field_edit()
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('edit_listing_template');
        if ($security === true) {
            global $conn, $misc;
            $display = '';

            $sqlDL ='SELECT controlpanel_template_listing_sections
					FROM ' . $config['table_prefix_no_lang'] . 'controlpanel';
            $recordSetDL = $conn->Execute($sqlDL);
            if (!$recordSetDL) {
                $misc->log_error($sql);
            }

            $dla = explode(',', $recordSetDL->fields['controlpanel_template_listing_sections']);
            $dla[]='';
            foreach ($dla as $key => $display_loc) {
                $display_loc=trim($display_loc);
                // Grab a list of field_names in the Database to Edit
                $sql = 'SELECT listingsformelements_id, listingsformelements_field_name,
						listingsformelements_required, listingsformelements_searchable,
						listingsformelements_field_caption, listingsformelements_rank
						FROM ' . $config['table_prefix'] . "listingsformelements
						WHERE listingsformelements_location = '".$display_loc."'
						ORDER BY listingsformelements_rank";
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }

                // if the display location was added after "bottom_right", it is custom
                // definitely not the best way to handle this
                if ($display_loc =='') {
                    $display_loc_class = 'center_box';
                    $display .= '<div class="'.$display_loc_class.'">'.$display_loc.BR;
                } else {
                    if (!in_array($display_loc, ['headline', 'top_left', 'top_right', 'center', 'feature1', 'feature2', 'bottom_left', 'bottom_right'])) {
                        $display_loc_class = 'center_box '.$display_loc;
                    } else {
                        $display_loc_class = $display_loc;
                    }
                    if (strpos($display_loc, 'top_') ==false) {
                        $eclass = 'equal_top';
                    } elseif (strpos($display_loc, 'bottom_') ==false) {
                        $eclass = 'equal_bottom';
                    } else {
                        $eclass = '';
                    }

                    $display .= '<div class="'.$display_loc_class.'">'.$display_loc.BR;
                }

                $display .= '	<ul id="'.$display_loc.'" class="qed_list display_location" title="'.$display_loc.'">'.BR;

                while (!$recordSet->EOF) {
                    $fid = $recordSet->fields['listingsformelements_id'];
                    $f_rank = $recordSet->fields['listingsformelements_rank'];

                    // Get Caption from users selected language
                    if (!isset($_SESSION['users_lang'])) {
                        $caption = $recordSet->fields['listingsformelements_field_caption'];
                    } else {
                        $field_id = $misc->make_db_safe($fid);
                        $sql2 = 'SELECT listingsformelements_field_caption
								FROM ' . $config['lang_table_prefix'] . "listingsformelements
								WHERE listingsformelements_id = $field_id";
                        $recordSet2 = $conn->Execute($sql2);
                        if (!$recordSet2) {
                            $misc->log_error($sql2);
                        }
                        $caption = htmlspecialchars($recordSet2->fields['listingsformelements_field_caption']);
                    }

                    if ($recordSet->fields['listingsformelements_searchable'] ==1) {
                        $searchable = '<span class="src_field">&nbsp;</span>';
                    } else {
                        $searchable ='<span class="fill">&nbsp;</span>';
                    }
                    if ($recordSet->fields['listingsformelements_required'] =='Yes') {
                        $required = '<span class="req_field">&nbsp;</span>';
                    } else {
                        $required ='<span class="fill">&nbsp;</span>';
                    }

                    $field_name = $recordSet->fields['listingsformelements_field_name'];

                    $display .='<li id="'.$field_name.'">'.BR;
                    $display .= '	<div class="block">'.BR;
                    $display .='		<a href="" class="edit_field_link" id="rank_'.$f_rank.'" name="'.$field_name.'">'.$caption.'</a>'.$searchable.$required.BR;
                    $display .='	</div>'.BR;
                    $display .='</li>'.BR;
                    //<span class="sortable-number"></span>'
                    $recordSet->MoveNext();
                    $f_rank++;
                }

                $display .= '	</ul>'.BR;
                $display .= '</div>'.BR;

                if ($key  == 7) {
                    $display .= '<div class="clear"></div>';
                }
            } //end foreach

            return $display;
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
    }

    public function edit_listing_template_spo()
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User is an Admin
        $security = $login->verify_priv('edit_listing_template');
        $display ='';
        $spo ='';

        if ($security) {
            global $conn, $misc;

            // Grab the list of Searchable fields sorted by search_rank
            $sql = 'SELECT listingsformelements_id, listingsformelements_field_name,
						listingsformelements_required, listingsformelements_searchable,
						listingsformelements_field_caption, listingsformelements_search_rank
						FROM ' . $config['table_prefix'] . "listingsformelements
						WHERE listingsformelements_searchable = '1'
		 				ORDER BY listingsformelements_search_rank";
            $recordSet = $conn->Execute($sql);

            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $spo .= '<div class="top_left">	
						<ul id="search_page_order">'.BR;

            while (!$recordSet->EOF) {
                $fid = $recordSet->fields['listingsformelements_id'];
                $f_rank = $recordSet->fields['listingsformelements_search_rank'];

                // Get Caption from users selected language
                if (!isset($_SESSION['users_lang'])) {
                    $caption = $recordSet->fields['listingsformelements_field_caption'];
                } else {
                    $field_id = $misc->make_db_safe($fid);
                    $sql2 = 'SELECT listingsformelements_field_caption
								FROM ' . $config['lang_table_prefix'] . "listingsformelements
								WHERE listingsformelements_id = $field_id";
                    $recordSet2 = $conn->Execute($sql2);
                    if (!$recordSet2) {
                        $misc->log_error($sql2);
                    }
                    $caption = $recordSet2->fields['listingsformelements_field_caption'];
                }

                if ($recordSet->fields['listingsformelements_searchable'] ==1) {
                    $searchable = '<span class="src_field">&nbsp;</span>';
                } else {
                    $searchable ='<span class="fill">&nbsp;</span>';
                }
                if ($recordSet->fields['listingsformelements_required'] =='Yes') {
                    $required = '<span class="req_field">&nbsp;</span>';
                } else {
                    $required ='<span class="fill">&nbsp;</span>';
                }
                $field_name = $recordSet->fields['listingsformelements_field_name'];

                $spo .='<li id="'.$field_name.'">'.BR;
                $spo .= '	<div class="block">'.BR;
                $spo .='		<a href="" class="edit_field_link" name="'.$field_name.'">'.$caption.'</a><span class="sortable-number">'.$f_rank.'</span>'.$searchable.$required.BR;
                $spo .='	</div>'.BR;
                $spo .='</li>'.BR;

                $recordSet->MoveNext();
            }
            $spo .= '	</ul></div><div style="clear: both;"></div> '.BR;

            $spo.='
			<script type="text/javascript">
			$("#search_page_order").sortable({
					tolerance: "pointer",
					revert: true,
					scroll: false,
					opacity: 0.9,
					cancel: "#tabnav",
					update: function(event, ui) {
						save_fields_locations("search_page_order");
						var $lis = $(this).children("li");
						$lis.each(function() {
							var newVal = $(this).index() + 1;
							$( this ).find("span.sortable-number").text(newVal);
						});
					}
			});

			$(".edit_field_link").click(function() {
				var fname =  $(this).attr("name");
				var fcaption = $(this).text();
				if (fname === undefined){
					fname =$("#edit_field_list").val();
					fcaption = $("#edit_field_list option:selected").text();
				}
				edit_listing_fields(fname, fcaption);
				return false;
 			});
 			</script>';
        } else {
            $spo .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return 	$spo;
    }

    public function edit_listing_template_sro()
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User is an Admin
        $security = $login->verify_priv('edit_listing_template');
        $display ='';
        $sro ='';

        if ($security === true) {
            global $conn, $misc;

            // Grab the list of fields set to be on the search results page sorted by search_result_rank
            $sql = 'SELECT listingsformelements_id, listingsformelements_field_name,
						listingsformelements_required, listingsformelements_searchable,
						listingsformelements_field_caption, listingsformelements_search_result_rank
						FROM ' . $config['table_prefix'] . "listingsformelements
						WHERE listingsformelements_display_on_browse = 'Yes'
						ORDER BY listingsformelements_search_result_rank;";
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $sro .= '	<div class="top_left">
							<ul id="search_result_order">'.BR;

            while (!$recordSet->EOF) {
                $fid = $recordSet->fields['listingsformelements_id'];
                $f_rank = $recordSet->fields['listingsformelements_search_result_rank'];
                // Get Caption from users selected language
                if (!isset($_SESSION['users_lang'])) {
                    $caption = $recordSet->fields['listingsformelements_field_caption'];
                } else {
                    $field_id = intval($fid);
                    $sql2 = 'SELECT listingsformelements_field_caption
								FROM ' . $config['lang_table_prefix'] . "listingsformelements
								WHERE listingsformelements_id = $field_id";
                    $recordSet2 = $conn->Execute($sql2);
                    if (!$recordSet2) {
                        $misc->log_error($sql2);
                    }
                    $caption = $recordSet2->fields['listingsformelements_field_caption'];
                }

                if ($recordSet->fields['listingsformelements_searchable'] ==1) {
                    $searchable = '<span class="src_field">&nbsp;</span>';
                } else {
                    $searchable ='<span class="fill">&nbsp;</span>';
                }

                if ($recordSet->fields['listingsformelements_required'] =='Yes') {
                    $required = '<span class="req_field">&nbsp;</span>';
                } else {
                    $required ='<span class="fill">&nbsp;</span>';
                }

                $field_name = $recordSet->fields['listingsformelements_field_name'];

                $sro .='<li id="'.$field_name.'">'.BR;
                $sro .= '	<div class="block">'.BR;
                $sro .='		<a href="" class="edit_field_link" name="'.$field_name.'">'.$caption.'</a><span class="sortable-number">'.$f_rank.'</span>'.$searchable.$required.BR;
                $sro .='	</div>'.BR;
                $sro .='</li>'.BR;

                $recordSet->MoveNext();
            }

            $sro .= '	</ul></div><div style="clear: both;"></div>'.BR;
            $sro.='
			<script type="text/javascript">
			$("#search_result_order").sortable({
					tolerance: "pointer",
					revert: true,
					scroll: false,
					opacity: 0.9,
					cancel: "#tabnav",
					update: function(event, ui) {
						save_fields_locations("search_result_order");
						var $lis = $(this).children("li");
						$lis.each(function() {
							var newVal = $(this).index() + 1;
							$( this ).find("span.sortable-number").text(newVal);
						});
					}
			});

			$(".edit_field_link").click(function() {
				var fname =  $(this).attr("name");
				var fcaption = $(this).text();
				if (fname === undefined){
					fname =$("#edit_field_list").val();
					fcaption = $("#edit_field_list option:selected").text();
				}
				edit_listing_fields(fname, fcaption);
				return false;
 			});
		</script>';
        } else {
            $sro .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return 	$sro;
    }

    public function edit_listing_template_qed()
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User is an Admin
        $security = $login->verify_priv('edit_listing_template');
        $display ='';

        if ($security === true) {
            global $conn, $misc;

            $listing_template_navbar = $this->show_listing_navbar();
            //gets the quick editor pane content for template tag replacement later
            $display .= $this->show_quick_field_edit();
            $display .='<script type="text/javascript">
					$(document).ready(function() { 
						$(".top_right .top_left").equalHeights(); 
					}); 
					
					//UnSet Focus Event for status_msg
					var handler_status_focus = function status_focus(){
					}
					$(".qed_list").sortable({
						connectWith: "ul",
						tolerance: "pointer",
						revert: true,
						scroll: false,
						opacity: 0.9,
						cancel: "#tabnav",
						update: function(event, ui) {
							save_fields_locations();
						 }
					});

					$("#edit_field_list").change(function(){
						fname = $("#edit_field_list").val();
						if(fname != ""){
							fcaption = $("#edit_field_list option:selected").text();
							edit_listing_fields(fname, fcaption);
							$("#edit_field_list").val("");
							return false;
						}
					});
					$(".edit_field_link").click(function() {
						var fname =  $(this).attr("name");
						var fcaption = $(this).text();
						edit_listing_fields(fname, fcaption);
						return false;
 					});
		</script>';
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function ajax_add_listing_field()
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User is an Admin
        $security = $login->verify_priv('edit_listing_template');
        if ($security) {
            $display = $this->add_listing_template_field();
        } else {
            $display = '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function add_listing_template_field()
    {
        global $config, $lang;

        $display = '';
        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User has cred
        $security = $login->verify_priv('edit_listing_template');

        if ($security) {
            global $conn, $misc;

            $display .= '<div class="edit_field_div">
							<form action="#" method="post" id="update_field">
								<div id="mini-tabs">
									<ul>
										<li><a href="#gen_opt">' . $lang['general_options'] . '</a></li>
	    								<li><a href="#lp_opt">' . $lang['listing_page_options'] . '</a></li>
										<li><a href="#sp_opt">' . $lang['search_options'] . '</a></li>
	   									<li><a href="#sr_opt">' . $lang['search_result_options'] . '</a></li>
									</ul>
	
									<!-- tab "panes" -->
    								<div id="gen_opt">

									<div class="form_div">
										<div class="field_caption">' . $lang['admin_template_editor_field_name'] . ':</div>
										<div class="field_element">
											<input type="text" name="edit_field" class="required" value="" /> <span class="form_required_field"></span>
											<script>
												$(document).ready(function(){
													$("input[name=edit_field]").on("input",function() {
														var oldVal = $(this).val();
														var newVal = oldVal.replace(RegExp("[^A-Za-z0-9_]","g"),"");
														$(this).val(newVal);
													});
												});		
											</script>
										</div>
									</div>
									<div class="clear"></div>

									<div class="form_div">
										<div class="field_caption">' . $lang['admin_template_editor_field_caption'] . ':</div>
										<div class="field_element" >
											<input type="text" name="field_caption" class="required" value="" /> <span class="form_required_field"></span>
										</div>
									</div>
									<div class="clear"></div>

									<div class="form_div">
										<div class="field_caption">' . $lang['admin_template_editor_field_required'] . ':</div>
										<div class="field_element">
										<select name="required" size="1">
											<option value="No" selected="selected">' . $lang['no'] . '</option>
											<option value="Yes" >' . $lang['yes'] . '</option>
										</select>
										</div>
									</div>
									<div class="clear"></div>

									<div class="form_div">
										<div class="field_caption">' . $lang['admin_template_editor_field_type'] . ':</div>
										<div class="field_element">
										<select name="field_type" class="required">
											<option value="text" selected="selected">' . $lang['text'] . '</option>
											<option value="textarea" >' . $lang['textarea'] . '</option>
											<option value="select" >' . $lang['select'] . '</option>
											<option value="select-multiple">' . $lang['select-multiple'] . '</option>
											<option value="option" >' . $lang['option'] . '</option>
											<option value="checkbox" >' . $lang['checkbox'] . '</option>
											<option value="divider">' . $lang['divider'] . '</option>
											<option value="price">' . $lang['price'] . '</option>
											<option value="url">' . $lang['url'] . '</option>
											<option value="email">' . $lang['email'] . '</option>
											<option value="number">' . $lang['number'] . '</option>
											<option value="decimal">' . $lang['decimal'] . '</option>
											<option value="date">' . $lang['date'] . '</option>
											<option value="lat">' . $lang['lat'] . '</option>
											<option value="long">' . $lang['long'] . '</option>
										</select> <span class="form_required_field"></span>
										</div>
									</div>
									<div class="clear"></div>

									<div class="form_div">
										<div class="field_caption">' . $lang['admin_template_editor_field_elements'] . ':<br />
														<span class="small">(' . $lang['admin_template_editor_choices_separated'] . ')</span></div>
										<div class="field_element"><textarea name="field_elements"  cols="80" rows="5"></textarea></div>
									</div>
									<div class="clear"></div>

									<div class="form_div">
										<div class="field_caption">' . $lang['admin_template_editor_field_default_text'] . ':</div>
										<div class="field_element"><input type="text" name="default_text" value="" /></div>
									</div>
									<div class="clear"></div>

									<div class="form_div">
										<div class="field_caption">' . $lang['admin_template_editor_field_length'] . ':</div>
										<div class="field_element"><input type="text" name="field_length" value="" /></div>
									</div>
									<div class="clear"></div>

									<div class="form_div">
										<div class="field_caption">' . $lang['admin_template_editor_tool_tip'] . ':</div>
										<div class="field_element"><textarea name="tool_tip" cols="80" rows="5"></textarea></div>
									</div>
									<div class="clear"></div>

									<div class="form_div">
										<div class="field_caption">' . $lang['admin_template_editor_field_display_priv'] . ':</div>
										<div class="field_element">
											<select name="display_priv" size="1">
											<option value="0" selected="selected">' . $lang['display_priv_0'] . '</option>
											<option value="1" >' . $lang['display_priv_1'] . '</option>
											<option value="2" >' . $lang['display_priv_2'] . '</option>
											<option value="3" >' . $lang['display_priv_3'] . '</option>
										</select>
										</div>
									</div>
									<div class="clear"></div>';

            // Property Class Selection
            $display .= '			<div class="form_div">
										<div class="field_caption">' . $lang['admin_template_editor_field_property_class'] . ':</div>
										<div class="field_element">
											<select name="property_class[]" class="required" id="propclass" multiple="multiple" size="5"> ';

            // get list of all property clases
            $sql = 'SELECT class_name, class_id 
					FROM ' . $config['table_prefix'] . 'class 
					ORDER BY class_rank';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            while (!$recordSet->EOF) {
                $class_id = $recordSet->fields['class_id'];
                $class_name = $recordSet->fields['class_name'];
                $display .= '<option value="' . $class_id . '" >' . $class_name . '</option>';
                $recordSet->MoveNext();
            }
            $display .= '					</select> <span class="form_required_field"></span>
										</div>
									</div>
									<div class="clear"></div>';

            // end first pane
            $display .= '			</div>';

            // Listing Page Options
            $display .= '			<!-- tab contents -->
									<div id="lp_opt" class="edit_field_pane">
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_display_location'] . ':</div>
											<div class="field_element">
												<select name="location" size="1">
													<option value="" selected="selected"></option>
													<option value="">-- '.$lang['do_not_display'].' --</option>';

            $sections = explode(',', $config['template_listing_sections']);
            foreach ($sections as $section) {
                $section = trim($section);
                $display .= '						<option value="' . $section . '">' . $section . '</option>';
            }
            $display .= '						</select>
											</div>
										</div>
										<div class="clear"></div>

										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_rank'] . ':</div>
											<div class="field_element"><input type="text" name="rank" value="0" /></div>
										</div>
										<div class="clear"></div>

									</div>'; // END Listing Page Options

            // Search Page Options
            $display .= '			<!-- tab contents -->
									<div id="sp_opt" class="edit_field_pane">

										<div class="form_div">
											<div class="field_caption">' . $lang['allow_searching'] . '</div>
											<div class="field_element"><input type="checkbox" name="searchable" value = "1" /></div>
										</div>
										<div class="clear"></div>

										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_rank_search'] . ':</div>
											<div class="field_element"><input type="text" name="search_rank" value="0" /></div>
										</div>
										<div class="clear"></div>

										<div class="form_div">
											<div class="field_caption">' . $lang['search_label'] . '</div>
											<div class="field_element"><input type="text" name="search_label" value="" /></div>
										</div>
										<div class="clear"></div>

										<div class="form_div">
											<div class="field_caption">' . $lang['search_type'] . '</div>
											<div class="field_element">
											<select name="search_type">
												<option></option>
												<option value="ptext">' . $lang['ptext_description'] . '</option>
												<option value="optionlist">' . $lang['optionlist_description'] . '</option>
												<option value="optionlist_or">' . $lang['optionlist_or_description'] . '</option>
												<option value="fcheckbox">' . $lang['fcheckbox_description'] . '</option>
												<option value="fcheckbox_or">' . $lang['fcheckbox_or_description'] . '</option>
												<option value="fpulldown">' . $lang['fpulldown_description'] . '</option>
												<option value="select">' . $lang['select_description'] . '</option>
												<option value="select_or">' . $lang['select_or_description'] . '</option>
												<option value="pulldown">' . $lang['pulldown_description'] . '</option>
												<option value="checkbox">' . $lang['checkbox_description'] . '</option>
												<option value="checkbox_or">' . $lang['checkbox_or_description'] . '</option>
												<option value="option">' . $lang['option_description'] . '</option>
												<option value="minmax">' . $lang['minmax_description'] . '</option>
												<option value="daterange">' . $lang['daterange_description'] . '</option>
												<option value="singledate">' . $lang['singledate_description'] . '</option>
												<option value="null_checkbox">' . $lang['null_checkbox_description'] . '</option>
												<option value="notnull_checkbox">' . $lang['notnull_checkbox_description'] . '</option>
												</select>
											</div>
										</div>
										<div class="clear"></div>

										<div class="form_div">
											<div class="field_caption"><span class="small">++ </span>' . $lang['step_by'] . '</div>
											<div class="field_element"><input type="text" name="search_step" value = "0" />
												<br /><span class="small">' . $lang['used_for_range_selections_only'] . '</span>
											</div>
										</div>
										<div class="clear"></div>

									</div>'; // END Search Page Options

            // Search Result Options
            $display .= '			<!-- tab contents -->
									<div id="sr_opt" class="edit_field_pane">

										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_display_browse'] . ':</div>
											<div class="field_element">
											<select name="display_on_browse" size="1">
												<option value="No" selected="selected">' . $lang['no'] . '</option>
												<option value="Yes" >' . $lang['yes'] . '</option>
											</select>
											</div>
										</div>
										<div class="clear"></div>

										<div class="form_div">
											<div class="field_caption" >' . $lang['admin_template_editor_field_rank_search_result'] . ':</div>
											<div class="field_element"><input type="text" name="search_result_rank" value="0" /></div>
										</div>
										<div class="clear"></div>

									</div>'; // END Search Result Options
            //end panes
            $display .= '		</div>
								<div class="space_10"></div>';

            // Add field Button
            $display .= '		<div class="form_div">
									<div>
										<div class="field_caption" ></div>
											<a href="#" class="or_std_button" id="update_button">' . $lang['add_field'] . '</a>
										<div class="field_element"></div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="space_5"></div>
							</div> 
							</form>'.BR;

            $display .= '</div>';
        } else {
            $display = '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }

        return $display;
    }

    public function ajax_insert_listing_field()
    {
        global $lang, $config;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('edit_listing_template');

        if ($security) {
            global $misc, $api;

            if (isset($_POST['edit_field']) && !isset($_POST['lang_change'])) {
                if (empty($_POST['property_class'])) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1','error_msg' =>  $lang['no_pclass_selected']]);
                }
                if (!isset($_POST['searchable'])) {
                    $_POST['searchable']=false;
                } else {
                    $_POST['searchable']=true;
                }
                if (!isset($_POST['field_length'])) {
                    $_POST['field_length']='';
                }
                if (!isset($_POST['tool_tip'])) {
                    $_POST['tool_tip']='';
                }
                if ($_POST['required']=='Yes') {
                    $_POST['required']=true;
                } else {
                    $_POST['required']=false;
                }
                if ($_POST['display_on_browse']=='Yes') {
                    $_POST['display_on_browse']=true;
                } else {
                    $_POST['display_on_browse']=false;
                }
                $create_result = $api->load_local_api(
                    'fields__create',
                    [
                                           'resource'=>'listing',
                                           'class'=>$_POST['property_class'],
                                           'field_type'=>$_POST['field_type'],
                                           'field_name'=>$_POST['edit_field'],
                                           'field_caption'=>$_POST['field_caption'],
                                           'default_text'=>$_POST['default_text'],
                                           'field_elements'=>explode('||', $_POST['field_elements']),
                                           'rank'=>$_POST['rank'],
                                           'search_rank'=>$_POST['search_rank'],
                                           'search_result_rank'=>$_POST['search_result_rank'],
                                           'required'=>$_POST['required'],
                                           'location'=>$_POST['location'],
                                           'display_on_browse'=>$_POST['display_on_browse'],
                                           'search_step'=>$_POST['search_step'],
                                           'display_priv'=>$_POST['display_priv'],
                                           'field_length'=>intval($_POST['field_length']),
                                           'tool_tip'=>$_POST['tool_tip'],
                                           'search_label'=>$_POST['search_label'],
                                           'search_type'=>$_POST['search_type'],
                                           'searchable'=>$_POST['searchable'],
                                     ]
                );
                if ($create_result['error'] == false) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '0','field_name' => $_POST['edit_field']]);
                } else {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1','error_msg' => $create_result['error_msg']]);
                }
            }
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1','error_msg' =>  $lang['access_denied']]);
        }
    }

    public function edit_listing_field($edit_listing_field_name)
    {
        global $config ;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('edit_listing_template');

        $display = '';

        if ($security === true) {
            global $conn, $lang, $misc, $jscript;

            $edit_listing_field_name = $misc->make_db_safe($edit_listing_field_name);
            $sql = 'SELECT * FROM ' . $config['table_prefix'] . "listingsformelements
					WHERE listingsformelements_field_name = $edit_listing_field_name";
            $recordSet = $conn->Execute($sql);
            if ($recordSet === false) {
                $misc->log_error($sql);
            }
            $id = $recordSet->fields['listingsformelements_id'];
            $field_type = $recordSet->fields['listingsformelements_field_type'];
            $field_name = $recordSet->fields['listingsformelements_field_name'];

            // Multi Lingual Support
            if (!isset($_SESSION['users_lang'])) {
                // Hold empty string for translation fields, as we are workgin with teh default lang
                $default_lang_field_caption = '';
                $default_lang_default_text = '';
                $default_lang_field_elements = '';
                $default_lang_search_label = '';

                $field_caption = $recordSet->fields['listingsformelements_field_caption'];
                $default_text = $recordSet->fields['listingsformelements_default_text'];
                $field_elements = $recordSet->fields['listingsformelements_field_elements'];
                $search_label = $recordSet->fields['listingsformelements_search_label'];
            } else {
                // Store default lang to show for tanslator
                $default_lang_field_caption = $recordSet->fields['listingsformelements_field_caption'];
                $default_lang_default_text = $recordSet->fields['listingsformelements_default_text'];
                $default_lang_field_elements = $recordSet->fields['listingsformelements_field_elements'];
                $default_lang_search_label = $recordSet->fields['listingsformelements_search_label'];
                $default_lang_tool_tip = $recordSet->fields['listingsformelements_tool_tip'];
                $field_id = intval($recordSet->fields['listingsformelements_id']);
                $lang_sql = 'SELECT listingsformelements_field_caption,listingsformelements_default_text,listingsformelements_field_elements,listingsformelements_search_label
							FROM ' . $config['lang_table_prefix'] . "listingsformelements
							WHERE listingsformelements_id = $field_id";
                $lang_recordSet = $conn->Execute($lang_sql);
                if (!$lang_recordSet) {
                    $misc->log_error($lang_sql);
                }
                $field_caption = $lang_recordSet->fields['listingsformelements_field_caption'];
                $default_text = $lang_recordSet->fields['listingsformelements_default_text'];
                $field_elements = $lang_recordSet->fields['listingsformelements_field_elements'];
                $search_label = $lang_recordSet->fields['listingsformelements_search_label'];
            }

            $rank = $recordSet->fields['listingsformelements_rank'];
            $search_rank = $recordSet->fields['listingsformelements_search_rank'];
            $search_result_rank = $recordSet->fields['listingsformelements_search_result_rank'];
            $required = $recordSet->fields['listingsformelements_required'];
            $location = $recordSet->fields['listingsformelements_location'];
            $display_on_browse = $recordSet->fields['listingsformelements_display_on_browse'];
            $display_priv = $recordSet->fields['listingsformelements_display_priv'];
            $search_step = $recordSet->fields['listingsformelements_search_step'];
            $searchable = $recordSet->fields['listingsformelements_searchable'];
            $search_type = $recordSet->fields['listingsformelements_search_type'];
            $field_length = $recordSet->fields['listingsformelements_field_length'];
            $tool_tip = $recordSet->fields['listingsformelements_tool_tip'];

            $display .= '<div class="edit_field_div">
							<form action="' . $config['baseurl'] . '/admin/index.php?action=edit_listing_template" method="post" id="update_field">
								<div id="mini-tabs">
									<ul>
										<li><a href="#gen_opt">' . $lang['general_options'] . '</a></li>
		    							<li><a href="#lp_opt">' . $lang['listing_page_options'] . '</a></li>
										<li><a href="#sp_opt">' . $lang['search_options'] . '</a></li>
		   								<li><a href="#sr_opt">' . $lang['search_result_options'] . '</a></li>
									</ul>

								
									<!-- 1st pane General Options -->
									<div id="gen_opt">
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_name'] . ':</div>
											<div class="field_element">
												<input type="hidden" name="update_id" value="' . $id . '" />
												<input type="hidden" name="old_field_name" value="' . $field_name . '" />
												<input type="text"  class="required" name="edit_field" value="' . $field_name . '" /> <span class="form_required_field"></span>
												
												<script>
													$(document).ready(function(){
														$("input[name=edit_field]").on("input",function() {
															var oldVal = $(this).val();
															var newVal = oldVal.replace(RegExp("[^A-Za-z0-9_]","g"),"");
															$(this).val(newVal);
														});
													});		
												</script>
											</div>
										</div>
										<div class="clear"></div>
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_caption'] . ':</div>
											<div class="field_element">
												<input type="text" class="required" name="field_caption" value = "' . $field_caption . '" /> <span class="form_required_field"></span>';

            if (isset($_SESSION['users_lang'])) {
                // Show Fields value in default language.
                $display .= '' . $lang['translate'] . '' . ': ' . $default_lang_field_caption;
            }
            $display .= '					</div>
										</div>
										<div class="clear"></div>

										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_required'] . ':</div>
											<div class="field_element">
											<select name="required" size="1">
												<option value="' . $required . '" selected="selected">' . $lang[strtolower($required)] . '</option>
												<option value="No">-----</option>
												<option value="No">' . $lang['no'] . '</option>
												<option value="Yes" >' . $lang['yes'] . '</option>
											</select>
											</div>
										</div>
										<div class="clear"></div>';

            $display .= '				<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_type'] . ':</div>
											<div class="field_element">
											<select name="field_type" size="1">
											<option value="' . $field_type . '" selected="selected">' . $lang[$field_type] . '</option>
											<option value="">-----</option>
											<option value="text">' . $lang['text'] . '</option>
											<option value="textarea" >' . $lang['textarea'] . '</option>
											<option value="select" >' . $lang['select'] . '</option>
											<option value="select-multiple">' . $lang['select-multiple'] . '</option>
											<option value="option" >' . $lang['option'] . '</option>
											<option value="checkbox" >' . $lang['checkbox'] . '</option>
											<option value="divider">' . $lang['divider'] . '</option>
											<option value="price">' . $lang['price'] . '</option>
											<option value="url">' . $lang['url'] . '</option>
											<option value="email">' . $lang['email'] . '</option>
											<option value="number">' . $lang['number'] . '</option>
											<option value="decimal">' . $lang['decimal'] . '</option>
											<option value="date">' . $lang['date'] . '</option>
											<option value="lat">' . $lang['lat'] . '</option>
											<option value="long">' . $lang['long'] . '</option>
											</select>
											</div>
										</div>
										<div class="clear"></div>
										
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_elements'] . ':<br />
												<span class="small">(' . $lang['admin_template_editor_choices_separated'] . ')</span>
											</div>
											<div class="field_element">
												<textarea name="field_elements" cols="80" rows="5">' . $field_elements . '</textarea>';
            if (isset($_SESSION['users_lang'])) {
                // Show Fields value in default language.
                $display .= '					<br />' . '' . $lang['translate'] . '' . ': ' . $default_lang_field_elements;
            }
            $display .= '					</div>
										</div>
										<div class="clear"></div>


										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_default_text'] . ':</div>
											<div class="field_element"><input type="text" name="default_text" value = "' . $default_text . '" />';
            if (isset($_SESSION['users_lang'])) {
                // Show Fields value in default language.
                $display .= '' . $lang['translate'] . '' . ': ' . $default_lang_default_text;
            }
            $display .= '					</div>
										</div>
										<div class="clear"></div>

										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_tool_tip'] . ':</div>
											<div class="field_element">
												<textarea name="tool_tip" cols="80" rows="5">' . $tool_tip . '</textarea>';
            if (isset($_SESSION['users_lang'])) {
                // Show Fields value in default language.
                $display .= '					<br />' . '' . $lang['translate'] . '' . ': ' . $default_lang_tool_tip;
            }
            $display .= '					</div>
										</div>
										<div class="clear"></div>

										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_length'] . ':</div>
											<div class="field_element"><input type="text" name="field_length" value = "' . $field_length . '" /></div>
										</div>
										<div class="clear"></div>

										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_display_priv'] . ':</div>
											<div class="field_element">
												<select name="display_priv" size="1">
													<option value="' . $display_priv . '" selected="selected">' . $lang['display_priv_' . $display_priv] . '</option>
													<option value="0">-----</option>
													<option value="0">' . $lang['display_priv_0'] . '</option>
													<option value="1" >' . $lang['display_priv_1'] . '</option>
													<option value="2" >' . $lang['display_priv_2'] . '</option>
													<option value="3" >' . $lang['display_priv_3'] . '</option>
												</select>
											</div>
										</div>
										<div class="clear"></div> 
										
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_property_class'] . ':</div>
											<div class="field_element">
												<select name="property_class[]" multiple="multiple" size="5" class="required">';
            // get list of all property clases
            $sql = 'SELECT class_name, class_id
					FROM ' . $config['table_prefix'] . 'class
					ORDER BY class_rank';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            while (!$recordSet->EOF) {
                $class_id = $recordSet->fields['class_id'];
                $class_name = $recordSet->fields['class_name'];
                // check if this field is part of this class
                $sql = 'SELECT count(class_id) 
						AS exist 
						FROM ' . $config['table_prefix_no_lang'] . 'classformelements
						WHERE listingsformelements_id = ' . $id . '
						AND class_id =' . $class_id;
                $recordSet2 = $conn->Execute($sql);
                if (!$recordSet2) {
                    $misc->log_error($sql);
                }
                $select = $recordSet2->fields['exist'];
                if ($select > 0) {
                    $display .= '					<option value="' . $class_id . '" selected="selected">' . $class_name . '</option>';
                } else {
                    $display .= '					<option value="' . $class_id . '" >' . $class_name . '</option>';
                }
                $recordSet->MoveNext();
            }
            $display .= '						</select> <span class="form_required_field"></span>
											</div>
										</div>
										<div class="clear"></div>
									</div>';

            // 2nd Pane LISTING PAGE OPTIONS
            $display .= '			<div id="lp_opt" class="edit_field_pane">
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_display_location'] . ':</div>
											<div class="field_element">
												<select name="location" size="1">
													<option value="' . $location . '" selected="selected">' . $location . '</option>
													<option value="">-- '.$lang['do_not_display'].' --</option>';
            $sections = explode(',', $config['template_listing_sections']);
            foreach ($sections as $section) {
                $section = trim($section);
                $display .= '						<option value="' . $section . '">' . $section . '</option>';
            }
            $display .= '						</select>
											</div>
										</div>
										<div class="clear"></div>

										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_rank'] . ':</div>
											<div class="field_element" ><input type="text" name="rank" value = "' . $rank . '" /></div>
										</div>
										<div class="clear"></div>

									</div>';

            // 3nd Pane Search Page Options
            $display .= '			<div id="sp_opt" class="edit_field_pane">
										<div class="form_div">
											<div class="field_caption">' . $lang['allow_searching'] . '</div>
											<div class="field_element">
												<input type="checkbox" name="searchable" value="1" ';
            if ($searchable) {
                $display .= 'checked="checked"';
            }
            $display .= '						/>			
											</div>
										</div>
										<div class="clear"></div>
	
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_rank_search'] . ':</div>
											<div class="field_element" ><input type="text" name="search_rank" value = "' . $search_rank . '"  /></div>
										</div>
										<div class="clear"></div>
	
										<div class="form_div">
											<div class="field_caption">' . $lang['search_label'] . '</div>
											<div class="field_element">
												<input type="text" name="search_label" value="' . htmlspecialchars($search_label, ENT_COMPAT, $config['charset']) . '" />';
            if (isset($_SESSION['users_lang'])) {
                // Show Fields value in default language.
                $display .= '' . $lang['translate'] . '' . ': ' . $default_lang_search_label;
            }
            $display .= '					</div>
										</div>
										<div class="clear"></div>
	
										<div class="form_div">
											<div class="field_caption">' . $lang['search_type'] . '</div>
											<div class="field_element">
												<select name="search_type">';
            if ($search_type != '') {
                $display .= '						<option value="' . $search_type . '">' . $lang[$search_type . '_description'] . '</option>';
            }
            $display .= '							<option></option>
													<option value="ptext">' . $lang['ptext_description'] . '</option>
													<option value="optionlist">' . $lang['optionlist_description'] . '</option>
													<option value="optionlist_or">' . $lang['optionlist_or_description'] . '</option>
													<option value="fcheckbox">' . $lang['fcheckbox_description'] . '</option>
													<option value="fcheckbox_or">' . $lang['fcheckbox_or_description'] . '</option>
													<option value="fpulldown">' . $lang['fpulldown_description'] . '</option>
													<option value="select">' . $lang['select_description'] . '</option>
													<option value="select_or">' . $lang['select_or_description'] . '</option>
													<option value="pulldown">' . $lang['pulldown_description'] . '</option>
													<option value="checkbox">' . $lang['checkbox_description'] . '</option>
													<option value="checkbox_or">' . $lang['checkbox_or_description'] . '</option>
													<option value="option">' . $lang['option_description'] . '</option>
													<option value="minmax">' . $lang['minmax_description'] . '</option>
													<option value="daterange">' . $lang['daterange_description'] . '</option>
													<option value="singledate">' . $lang['singledate_description'] . '</option>
													<option value="null_checkbox">' . $lang['null_checkbox_description'] . '</option>
													<option value="notnull_checkbox">' . $lang['notnull_checkbox_description'] . '</option>
												</select>
											</div>
										</div>
										<div class="clear"></div>
		
										<div class="form_div">
											<div class="field_caption"><span class="small">++ </span>' . $lang['step_by'] . '</div>
											<div class="field_element"><input type="text" name="search_step" value = "' . $search_step . '" />
												<br /><span class="small">' . $lang['used_for_range_selections_only'] . '</span>
											</div>
										</div>
										<div class="clear"></div>
									</div>';

            // 4th Pane SEARCH RESULT OPTIONS
            $display .= '		<div id="sr_opt" class="edit_field_pane">
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_display_browse'] . ':</div>
											<div class="field_element">
											<select name="display_on_browse" size="1">
												<option value="' . $display_on_browse . '" selected="selected">' . $lang[strtolower($display_on_browse)] . '</option>
												<option value="No">-----</option>
												<option value="No">' . $lang['no'] . '</option>
												<option value="Yes" >' . $lang['yes'] . '</option>
												</select>
											</div>
										</div>
										<div class="clear"></div>
	
										<div class="form_div">
											<div class="field_caption">' . $lang['admin_template_editor_field_rank_search_result'] . ':</div>
											<div class="field_element" ><input type="text" name="search_result_rank" value = "' . $search_result_rank . '" /></div>
										</div>
	
										</div>
										<br class="clear">
									</div>
	
									<div class="form_div">
										<div style="text-align: center;">
											<a href="#" class="or_std_button medf" id="update_button">' . $lang['update_button'] . '</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp
											<a href="' . $config['baseurl'] . '/admin/index.php?action=edit_listing_template&amp;delete_field=' . urlencode($field_name) . '" class="or_std_button red medf" onclick="return confirmDelete()">' . $lang['delete'] . '</a>
										</div>
										
									</div>
									<div class="clear"></div>
									
								</div>
								
							</form>
	
						</div>';
            return $display;
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
    }

    public function ajax_update_listing_field()
    {
        global $lang, $config;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $display = '';
        $security = $login->verify_priv('edit_listing_template');

        if ($security) {
            global $conn, $misc, $api;

            if (isset($_POST['update_id']) && !isset($_POST['lang_change'])) {
                $id = intval($_POST['update_id']);
                $_POST['old_field_name'] = str_replace(' ', '_', $_POST['old_field_name']);
                $_POST['edit_field'] = str_replace(' ', '_', $_POST['edit_field']);
                $field_name = $misc->make_db_safe($_POST['edit_field']);
                $old_field_name = $misc->make_db_safe($_POST['old_field_name']);
                $required = $misc->make_db_safe($_POST['required']);
                $update_field_name = false;
                if ($old_field_name != $field_name) {
                    $update_field_name = true;
                }
                $field_type = $misc->make_db_safe($_POST['field_type']);
                $field_caption = $misc->make_db_safe($_POST['field_caption']);
                $default_text = $misc->make_db_safe($_POST['default_text']);
                $field_elements = $misc->make_db_safe($_POST['field_elements']);
                $rank = intval($_POST['rank']);
                $search_rank = intval($_POST['search_rank']);
                $search_result_rank = intval($_POST['search_result_rank']);
                $location = $misc->make_db_safe($_POST['location']);
                $display_on_browse = $misc->make_db_safe($_POST['display_on_browse']);
                $display_priv = intval($_POST['display_priv']);
                $search_step = intval($_POST['search_step']);
                $field_length = intval($_POST['field_length']);
                $tool_tip = $misc->make_db_safe($_POST['tool_tip']);
                if (isset($_POST['searchable'])) {
                    $searchable = intval($_POST['searchable']);
                } else {
                    $searchable = intval(0);
                }
                if ($searchable == '1' && $_POST['search_type'] == '') {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1','error_msg' =>  $lang['no_search_type']]);
                } elseif (count($_POST['property_class']) == 0) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1','error_msg' =>  $lang['no_property_class_selected']]);
                } else {
                    $search_label = $misc->make_db_safe($_POST['search_label']);
                    $search_type = $misc->make_db_safe($_POST['search_type']);
                    $sql = 'UPDATE ' . $config['table_prefix'] . "listingsformelements
							SET listingsformelements_field_type = $field_type, listingsformelements_field_name = $field_name, listingsformelements_rank = $rank, listingsformelements_search_rank = $search_rank, listingsformelements_search_result_rank = $search_result_rank, listingsformelements_required = $required, listingsformelements_location = $location, listingsformelements_display_on_browse = $display_on_browse, listingsformelements_search_step = $search_step, listingsformelements_searchable = $searchable, listingsformelements_search_type = $search_type, listingsformelements_display_priv = $display_priv, listingsformelements_field_length = $field_length, listingsformelements_tool_tip = $tool_tip
							WHERE listingsformelements_id = $id";
                    $recordSet = $conn->Execute($sql);
                    if ($recordSet === false) {
                        $misc->log_error($sql);
                    }
                    // Update Current language
                    if (!isset($_SESSION['users_lang'])) {
                        $lang_sql = 'UPDATE  ' . $config['table_prefix'] . "listingsformelements SET listingsformelements_field_caption = $field_caption, listingsformelements_default_text = $default_text,listingsformelements_field_elements = $field_elements,listingsformelements_search_label = $search_label
									WHERE listingsformelements_id = $id";
                        $lang_recordSet = $conn->Execute($lang_sql);
                        if (!$lang_recordSet) {
                            $misc->log_error($lang_sql);
                        }
                    } else {
                        $lang_sql = 'DELETE FROM  ' . $config['lang_table_prefix'] . "listingsformelements WHERE listingsformelements_id = $id";
                        $lang_recordSet = $conn->Execute($lang_sql);
                        if (!$lang_recordSet) {
                            $misc->log_error($lang_sql);
                        }
                        $lang_sql = 'INSERT INTO ' . $config['lang_table_prefix'] . "listingsformelements (listingsformelements_id, listingsformelements_field_caption,listingsformelements_default_text,listingsformelements_field_elements,listingsformelements_search_label)
									VALUES ($id, $field_caption,$default_text,$field_elements,$search_label)";
                        $lang_recordSet = $conn->Execute($lang_sql);
                        if (!$lang_recordSet) {
                            $misc->log_error($lang_sql);
                        }
                    }
                    // Check if field name changed, if it as update all listingsdbelement tables
                    if ($update_field_name) {
                        $lang_sql = 'UPDATE  ' . $config['table_prefix'] . "listingsdbelements SET listingsdbelements_field_name = $field_name
									WHERE listingsdbelements_field_name = $old_field_name";
                        $lang_recordSet = $conn->Execute($lang_sql);
                        if (!$lang_recordSet) {
                            $misc->log_error($lang_sql);
                        }
                    }
                    // Delete from classform elements.
                    $sql = 'DELETE FROM ' . $config['table_prefix_no_lang'] . 'classformelements WHERE listingsformelements_id = ' . $id;
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $misc->log_error($sql);
                    }
                    // Insert new selections into class formelements
                    $class_sql = '';
                    foreach ($_POST['property_class'] as $class_id) {
                        $class_id = intval($class_id);
                        if ($class_id>0) {
                            //Add to Property Class
                            $result = $api->load_local_api('fields__assign_class', ['class'=>$class_id,'field_id'=>$id]);
                            if (!empty($class_sql)) {
                                $class_sql .= ' OR listingsdb_pclass_id = ' . $class_id;
                            } else {
                                $class_sql .= ' listingsdb_pclass_id = ' . $class_id;
                            }
                        }
                    }
                    // Remove fields from any listings that are not in this class.
                    $pclass_list = '';
                    $sql = 'SELECT listingsdb_id FROM ' . $config['table_prefix'] . 'listingsdb WHERE ' . $class_sql;
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $misc->log_error($sql);
                    }
                    while (!$recordSet->EOF) {
                        if (empty($pclass_list)) {
                            $pclass_list .= $recordSet->fields['listingsdb_id'];
                        } else {
                            $pclass_list .= ',' . $recordSet->fields['listingsdb_id'];
                        }
                        $recordSet->Movenext();
                    }
                    if ($pclass_list == '') {
                        $pclass_list = 0;
                    }
                    $sql = 'DELETE FROM ' . $config['table_prefix'] . 'listingsdbelements
							WHERE listingsdbelements_field_name = ' . $field_name . '
							AND listingsdb_id NOT IN (' . $pclass_list . ')';
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $misc->log_error($sql);
                    }
                    header('Content-type: application/json');
                    return json_encode(['error' => '0','field_id' =>  $id]);
                    //$display .= '<center>' . $lang['field_has_been_updated'] . '</center><br />';
                }
            }
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1','error_msg' =>  $lang['access_denied']]);
        }
        return $display;
    }

    public function ajax_get_listing_field_info()
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User is an Admin
        $security = $login->verify_priv('edit_listing_template');

        $display = '';

        if ($security === true) {
            if (isset($_GET['edit_field'])) {
                $display .= $this->edit_listing_field($_GET['edit_field']);
            }
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function ajax_save_listing_field_order()
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User is an Admin
        $security = $login->verify_priv('edit_listing_template');

        echo '<pre>'.print_r($_POST['field_name'], true).'</pre>';
        echo '<pre>'.print_r($_POST['search_setup'], true).'</pre>';

        $display ='';
        if ($security === true) {
            global $conn, $misc;

            if (isset($_POST['search_setup'])) {
                $count = 0;
                $num_locations = count($_POST['search_setup']);

                while ($count < $num_locations) {
                    //explode the comma delimited field list for this display location
                    $field_name_arr[$count] = explode(',', $_POST['field_name'][$count]);

                    $field_location = $_POST['search_setup'][$count];
                    $display .= $field_location .' - '.$lang['generic_saved_text'].' <br />'.BR;
                    $field_rank =0;

                    foreach ($field_name_arr[$count] as $field_name) {
                        //empty locations are skipped
                        if (!empty($field_name)) {
                            $num_of_fields =count($field_name_arr[$count]);
                            $field_rank = $field_rank+1;

                            $sql = 'UPDATE ' . $config['table_prefix'] . "listingsformelements
							SET listingsformelements_location = '".$field_location."',
							 listingsformelements_rank = '".$field_rank."'
							WHERE listingsformelements_field_name = '".$field_name."'";
                            $recordSet = $conn->Execute($sql);
                            //echo $sql.'<br>';
                            if (!$recordSet) {
                                $misc->log_error($sql);
                            }
                        }
                    }
                    $count++;
                } // while
                $display .= '<center><strong>' . $lang['admin_template_editor_field_order_set'] . ' ('.$num_locations.')</strong></center>';
            }
        } else {
            $display = '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function ajax_save_listing_search_order($order_form)
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User is an Admin
        $security = $login->verify_priv('edit_listing_template');

        //echo print_r($_POST['search_setup'],TRUE).' zzz<br>';

        $display ='';
        if ($security === true) {
            global $conn, $misc;
            if (isset($_POST['search_setup'])) {
                if ($order_form =='spo') {
                    $rank_field = 'listingsformelements_search_rank';
                } elseif ($order_form =='sro') {
                    $rank_field = 'listingsformelements_search_result_rank';
                }

                //explode the comma delimited field list for this display location
                $field_name_arr = explode(',', $_POST['field_name'][0]);
                $search_rank =0;

                foreach ($field_name_arr as $field_name) {
                    //empty locations are skipped
                    if (!empty($field_name)) {
                        $num_of_fields =count($field_name_arr);
                        $search_rank = $search_rank+1;

                        $sql = 'UPDATE ' . $config['table_prefix'] . 'listingsformelements
						SET  '.$rank_field." = '".$search_rank."'
						WHERE listingsformelements_field_name = '".$field_name."'";
                        $recordSet = $conn->Execute($sql);
                        if (!$recordSet) {
                            $misc->log_error($sql);
                        }
                    }
                }
                $display .= '<center><strong>' . $lang['admin_template_editor_field_order_set'] . ' ('.$num_locations.')</strong></center>';
            }
        } else {
            $display = '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function delete_listing_field()
    {
        global $config, $lang;

        include_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $security = $login->verify_priv('edit_listing_template');

        if ($security) {
            global $conn, $misc, $api;

            if (isset($_GET['delete_field']) && !isset($_POST['lang_change'])) {
                $field_name = $misc->make_db_safe($_GET['delete_field']);
                $sql = 'SELECT listingsformelements_id 
						FROM ' . $config['table_prefix'] . "listingsformelements
						WHERE listingsformelements_field_name = $field_name";
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                // Delete All Translationf for this field.
                $configured_langs = explode(',', $config['configured_langs']);
                while (!$recordSet->EOF) {
                    $listingsformelements_id = intval($recordSet->fields['listingsformelements_id']);
                    foreach ($configured_langs as $configured_lang) {
                        $sql = 'DELETE FROM ' . $config['table_prefix_no_lang'] . $configured_lang . "_listingsformelements
								WHERE listingsformelements_id = $listingsformelements_id";
                        $recordSet = $conn->Execute($sql);
                        if (!$recordSet) {
                            $misc->log_error($sql);
                        }
                    }
                    // Remove field from property class.
                    $sql = 'DELETE FROM ' . $config['table_prefix_no_lang'] . 'classformelements
							WHERE listingsformelements_id = ' . $listingsformelements_id;
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $misc->log_error($sql);
                    }
                }
                // Cleanup any listingdbelemts entries from this field.
                foreach ($configured_langs as $configured_lang) {
                    $sql = 'DELETE FROM ' . $config['table_prefix_no_lang'] . $configured_lang . "_listingsdbelements
							WHERE listingsdbelements_field_name = $field_name";
                    $recordSet = $conn->Execute($sql);
                    if (!$recordSet) {
                        $misc->log_error($sql);
                    }
                }
                $api->load_local_api('log__log_create_entry', ['log_type'=>'CRIT','log_api_command'=>'function->delete_listing_field','log_message'=>'Deleted Listing Field '.$field_name]);
            }
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
    }


    /*** DEPRECATED BELOW *****/
/*
    function ajax_edit_search_setup() {
        global $config, $lang;

        $display = '';
        require_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        // Verify User is an Admin
        $security = $login->verify_priv('edit_listing_template');

        if ($security === true)	{
            $display .= $this->edit_listing_template_search();
        }
        else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    function save_search_setup(){
        global $config, $lang;

        require_once $config['basepath'] . '/include/login.inc.php';
        $login = new login();
        $display = '';
        $security = $login->verify_priv('edit_listing_template');

        if ($security === true) {
            global $conn, $misc;
            if ((isset($_POST['field_name'])) && (!isset($_POST['lang_change']))) {
                $count = 0;
                $num_fields = count($_POST['field_name']);
                while ($count < $num_fields) {
                    $field_name = $misc->make_db_safe($_POST['field_name'][$count]);
                    $id = intval($_POST['id'][$count]);
                    if (isset($_POST['searchable_' . $_POST['id'][$count]])) {
                        $searchable = $misc->make_db_safe($_POST['searchable_' . $_POST['id'][$count]]);
                    }
                    else {
                        $searchable = $misc->make_db_safe(0);
                    }
                    if ($searchable == "'1'" && $_POST['search_type'][$count] == '') {
                        $display .= '<span class="error_message">' . $lang['no_search_type'] . '</span><br />';
                    }
                    else {
                        $search_type = $misc->make_db_safe($_POST['search_type'][$count]);
                        $search_label = $misc->make_db_safe($_POST['search_label'][$count]);
                        $search_step = $misc->make_db_safe($_POST['search_step'][$count]);
                        $search_rank = $misc->make_db_safe($_POST['search_rank'][$count]);
                        $sql = "UPDATE " . $config['table_prefix'] . "listingsformelements
                                SET listingsformelements_searchable = $searchable,listingsformelements_search_type = $search_type,listingsformelements_search_step = $search_step, listingsformelements_search_rank = $search_rank
                                WHERE listingsformelements_id = $id";
                        $recordSet = $conn->Execute($sql);
                        if (!$recordSet) {
                            $misc->log_error($sql);
                        }
                        // Save Search Label in corerect language
                        if (!isset($_SESSION["users_lang"])) {
                            $lang_table_prefix = $config['table_prefix'];
                        }
                        else {
                            $lang_table_prefix = $config['lang_table_prefix'];
                        }
                        $sql = "SELECT listingsformelements_id FROM " . $lang_table_prefix . "listingsformelements
                                WHERE listingsformelements_id = $id";
                        $recordSet = $conn->Execute($sql);
                        if (!$recordSet) {
                            $misc->log_error($sql);
                        }
                        if ($recordSet->RecordCount() > 0) {
                            $sql = "UPDATE " . $lang_table_prefix . "listingsformelements SET listingsformelements_search_label = $search_label
                                    WHERE listingsformelements_id = $id";
                            $recordSet = $conn->Execute($sql);
                            if (!$recordSet) {
                                $misc->log_error($sql);
                            }
                        }
                        else {
                            $sql = "INSERT INTO " . $lang_table_prefix . "listingsformelements (listingsformelements_search_label, listingsformelements_id)
                                    VALUES ($search_label,$id)";
                            $recordSet = $conn->Execute($sql);
                            if (!$recordSet) {
                                $misc->log_error($sql);
                            }
                        }
                    }
                    $count++;
                } // while
                $display .= '<center><strong>' . $lang['admin_template_editor_field_order_set'] . '</strong></center>';

                return $display;
            }
        }
    }
*/
}
