<?php



// ENGLISH LANGUAGE FILE

//Step 1: Check File Permissions:
$this->lang['install_Page_Title'] = "Open-Realty&reg; Install";
$this->lang['install_version_warn'] = "Your version of php is not able to run the current version of Open-Realty&reg; Installation has been cancelled";
$this->lang['install_sqlversion_warn'] = "Your version of mysql is not able to run the current version of Open-Realty&reg; Installation has been cancelled";
$this->lang['install_php_version'] = "Your Current PHP version is ";
$this->lang['install_sql_version'] = "Your Current MySql version is ";
$this->lang['install_php_required'] = "The current version of Open-Realty&reg; requires a minimum PHP version of ";
$this->lang['install_sql_required'] = "The current version of Open-Realty&reg; requires a minimum MySql version of ";
$this->lang['install_welcome'] = "Welcome to the Open-Realty&reg; install tool.";
$this->lang['install_intro'] = "This tool will guide you through setting up your Open-Realty&reg; install. Before you begin you must have created a blank database on your system. You must also have file permissions set so the following files and directories are writeable by the web server.";
$this->lang['install_step_one_header'] = "Step 1: Check File Permissions:";
$this->lang['install_Permission_on'] = "Permission on";
$this->lang['install_are_correct'] = "are correct";
$this->lang['install_are_incorrect'] = "are incorrect";
$this->lang['install_all_correct'] = "All Permissions are correct.";
$this->lang['install_continue'] = "Click To Continue Installation";
$this->lang['install_please_fix'] = "Please have your host enable the above requirements.";

//Step 1: Determine Install Type
$this->lang['install_select_type'] = 'Select Installation Type:';
$this->lang['install_new'] = 'New Install Of Open-Realty&reg;';
$this->lang['move'] = 'Update Path and URL information only';
$this->lang['upgrade_200'] = 'Upgrade from Open-Realty&reg; 2.x.x (2.0.0 Beta 1) or newer)';

//Step 2: Setup Database Connection:
$this->lang['install_setup__database_settings'] = "Setup Database Connection:";
$this->lang['install_Database_Type'] = "Database Type:";
$this->lang['install_mySQL'] = "mySQL";
$this->lang['install_PostgreSQL'] = "PostgreSQL";
$this->lang['install_Database_Server'] = "Database Server:";
$this->lang['install_Database_Name'] = "Database Name:";
$this->lang['install_Database_User'] = "Database User:";
$this->lang['install_Database_Password'] = "Database Password:";
$this->lang['install_Table Prefix'] = "Table Prefix:";
$this->lang['install_Base_URL'] = "Base URL:";
$this->lang['install_Base_Path'] = "Base Path:";
$this->lang['install_Language'] = "Language:";
$this->lang['install_English'] = "English";
$this->lang['install_Spanish'] = "Spanish";
$this->lang['install_Italian'] = "Italian";
$this->lang['install_French'] = "French";
$this->lang['install_Portuguese'] = "Portuguese";
$this->lang['install_Russian'] = "Russian";
$this->lang['install_Turkish'] = "Turkish";
$this->lang['install_German'] = "German";
$this->lang['install_Dutch'] = "Dutch";
$this->lang['install_Lithuanian'] = "Lithuanian";
$this->lang['install_Arabic'] = "Arabic";
$this->lang['install_Polish'] = "Polish";
$this->lang['install_Czech'] = "Czech";
$this->lang['install_Indonesian'] = "Indonesian";
$this->lang['install_Bulgarian'] = "Bulgarian";
$this->lang['install_connection_fail'] = "We are unable to connect to your database. Please Check your settings and try again.";

//Step Three
$this->lang['install_get_old_version'] = 'Determining old Open-Realty&reg; version';
$this->lang['install_get_old_version_error'] = 'Error determining old Open-Realty&reg; version. Upgrade can not continue.';
$this->lang['install_cleared_cache'] = "Cleared Cache";
$this->lang['install_connection_ok'] = "We are able to connect to the database.";
$this->lang['install_save_settings'] = "We are now going to save your settings to your common.php file";
$this->lang['install_settings_saved'] = "Database Settings Saved.";
$this->lang['install_continue_db_setup'] = "Continue to setup the database.";
$this->lang['install_populate_db'] = "We are now going to populate the database.";

//finalize installation
$this->lang['install_installation_complete'] = "Installation is complete.";
$this->lang['install_configure_installation'] = "Click here to configure your installation";

//2.2.0 additions.
$this->lang['install_devel_mode'] = "Developer Mode Install - This will allow the install to continue even with errors. THIS IS NOT RECOMMENDED.";
$this->lang['yes'] = "Yes";
$this->lang['no'] = "No";

//3.0.4 additions
$this->lang['curl_not_enabled'] = 'PHP Curl Extenstion is not installed';
$this->lang['warnings_php_zip'] = 'Your PHP Install does not have the PHP ZIP Functions Installed';
$this->lang['warnings_nothing'] = 'Nothing wrong was detected for the settings tested.';
$this->lang['warnings_magic_quotes_gpc'] = '- You have "magic_quotes_gpc" actually set to "ON" at your server while you should have it set to "OFF". Contact your host support and ask to turn it off.';
$this->lang['warnings_mb_convert_encoding'] = '- MBString is not enabled at your server and you have it set to "Yes". Modify this setting to "No" (at "Site Config", "Editor/HTML" tab) or contact your host support and ask to enable it.';
$this->lang['warnings_mod_rewrite'] = '- Your server have "mod_rewrite" DISABLED and you have enabled URL TYPE as "Search Engine Friendly" (at "Site Config", "SEO" tab). Contact your host support and ask to turn it on.';
$this->lang['warnings_htaccess'] = '- You don\'t have a ".htaccess" file but you have enabled URL TYPE as "Search Engine Friendly" (at "Site Config", "SEO" tab). Revert back to "Standard URL" and read Open-Realty® Documentation (<a href="http://docs.google.com/View?id=dhk2ckgx_4dw62x5fh#_SEO_Search_Engine_Optimizatio" title="Search engine optimization settings">here</a>).';
$this->lang['warnings_admin_password'] = '- For security reasons you should modify the password for the "admin" user name account. Actually it is still set as the default one set during installation: "password".';
$this->lang['warnings_openssl'] = '- Open-Realty® v.3.0.0 and later, requires "openssl" to be loaded by PHP at your server. Actually you have it DISABLED - contact your Host Support and ask to enable it.';
$this->lang['warnings_safe_mode'] = 'PHP Safe Mode is enabled, this must be turned off.';
$this->lang['warnings_php_gd'] = 'Your PHP Install does not have the GD Libraries Installed';
$this->lang['file_path_contains_a_space'] = 'Your File System Path contains a space, this is not allowed.';
$this->lang['warnings_php_freetype'] = 'Your PHP Install does not have the TTF FreeType Libraries Installed';
