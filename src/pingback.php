<?php

    error_reporting(E_ALL ^ E_NOTICE);
    //
    // This Fixes XHTML Validation issues, with PHP
    @ini_set('arg_separator.output', '&amp;');
    @ini_set('pcre.backtrack_limit', '10000000');
    @ini_set('url_rewriter.tags', 'a=href,area=href,frame=src,input=src');
    @ini_set('precision', 14);
    //Start a session
if (session_id()=='') {
    session_start();
}
    header('Cache-control: private'); //IE6 Form Refresh Fix

    global $config, $conn, $css_file;
    $css_file = '';
    require_once dirname(__FILE__) . '/include/common.php';

    //Set XML Header
    header('Content-Type: application/xml');
    //Load XMLRPC
    require_once $config['basepath'] . '/vendor/phpxmlrpc/phpxmlrpc/lib/xmlrpc.inc';
    require_once $config['basepath'] . '/vendor/phpxmlrpc/phpxmlrpc/lib/xmlrpcs.inc';

    //Tutorial at quietearth.us was very helpful in building the pingback parser.
function process_ping($m)
{
    global $config, $conn, $misc, $css_file;

    include_once dirname(__FILE__) . '/include/core.inc.php';
    $page = new page_user();

    $x1 = $m->getParam(0);
    //echo 'x1: <pre>'.print_r($x1).'</pre>';
    $x2 = $m->getParam(1);
    //echo 'x2: <pre>'.print_r($x2).'</pre>';
    $source_uri = $x1->scalarval(); # their article
    $dest_uri = $x2->scalarval(); # your article
    $agent='Open-Realty Pingback Service ('.$config['baseurl'].')';
    //echo $source;
    # INSERT CODE
    # here we can check for valid urls in source and dest, security
    # lookup the dest article in our database etc..
    if ($config['allow_pingbacks'] == 0) { # Access denied
        return new xmlrpcresp(0, 49, 'Access denied');
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $source_uri);
    curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $source = curl_exec($ch);
    curl_close($ch);

    if ($source == false) { # source uri does not exist
        return new xmlrpcresp(0, 16, 'Source uri does not exist');
    }
    //$source = html_entity_decode($source);
    if (strpos($source, $dest_uri) === false) { # source uri does not have a link to target uri
        //echo '<br />Source: <pre>'.$source.'</pre><br />';
        //echo '<br />Dest URI: <pre>'.$dest_uri.'</pre><br />';
        return new xmlrpcresp(0, 17, 'Source uri does not have link to target uri');
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $dest_uri);
    curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $target = curl_exec($ch);
    curl_close($ch);

    if ($target==false) { # target uri does not exist
        return new xmlrpcresp(0, 32, 'Target uri does not exist');
    }
    //Check that target is a local URI and is a published article.
    $base_url_nowww = str_replace('www.', '', $config['baseurl']);
    if (strpos($dest_uri, $config['baseurl'])===false && strpos($dest_uri, $base_url_nowww)===false) { # target uri cannot be used as target
        return new xmlrpcresp(0, 33, 'Target uri cannot be used as target (baseurl)');
    }
    $dest_url_path =str_replace($base_url_nowww, '', $dest_uri);
    $dest_url_path = str_replace($config['baseurl'], '', $dest_url_path);
    //Check that it is a published artile
    $article_id = 0;
    unset($_GET['ArticleID']);
    $page->magicURIParser(false, $dest_url_path);
    if (isset($_GET['ArticleID'])) {
        $article_id=intval($_GET['ArticleID']);
    }
    if ($article_id>0) {
        //Make sure article is published.
        $sql = 'SELECT blogmain_id FROM '.$config['table_prefix'].'blogmain WHERE blogmain_id = '.intval($article_id).' AND blogmain_published = 1;';
        //echo $sql;
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
        //echo 'RC:'.$recordSet->RecordCount();
        if ($recordSet->RecordCount() == 0) { # Pingback already registered
            return new xmlrpcresp(0, 33, 'Target uri cannot be used as target (nonpublished)');
        }
    } else {
        return new xmlrpcresp(0, 33, 'Target uri cannot be used as target (invalid article id)');
    }
    //Al
    //Make sure this is not a duplicate ping
    /*  CREATE TABLE  `blogtest`.`default_blogpingbacks` (
    `blogpingback_id` int(11) NOT NULL auto_increment,
    `blogmain_id` int(11) NOT NULL,
    `blogpingback_timestamp` int(11) NOT NULL,
    `blogpingback_source` varchar(2000) NOT NULL,
    `blogpingback_dest` varchar(2000) NOT NULL,
    `blogcomments_moderated` tinyint(1) default NULL,
    PRIMARY KEY  (`blogpingback_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=latin1
    */
    $sql = 'SELECT blogpingback_id FROM '.$config['table_prefix_no_lang'].'blogpingbacks 
			WHERE blogpingback_source = '.$misc->make_db_safe($source_uri).' 
			AND blogmain_id = '.$article_id.';';
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        $misc->log_error($sql);
    }

    if ($recordSet->RecordCount() > 0) { # Pingback already registered
        return new xmlrpcresp(0, 48, 'Pingback already registered');
    }

    /*
     if (..) { # Could not communicate with upstream server or got error
        return new xmlrpcresp(0, 50, "Problem with upstream server");
        }

        */
    if ($config['blog_requires_moderation']==1) {
        $moderated=0;
    } else {
        $moderated=1;
    }
    $sql = 'INSERT INTO '.$config['table_prefix_no_lang'].'blogpingbacks (blogpingback_source,blogmain_id,blogpingback_timestamp,blogcomments_moderated)
	VALUES ('.$misc->make_db_safe($source_uri).','.$article_id.',
	'.time().','.$moderated.'
	);';
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        return new xmlrpcresp(0, 50, 'Unkown error');
    }

    return new xmlrpcresp(new xmlrpcval('Pingback registered.', 'string'));
}

    $a = [ 'pingback.ping' => [ 'function' => 'process_ping' ]];

    $s = new xmlrpc_server($a, false);
    #$s->setdebug(3);
    $s->service();
